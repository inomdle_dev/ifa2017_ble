/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 */

/** @file
 *
 * @defgroup ble_sdk_apple_notification_main main.c
 * @{
 * @ingroup ble_sdk_app_apple_notification
 * @brief Apple Notification Client Sample Application main file. Disclaimer: 
 * This client implementation of the Apple Notification Center Service can and 
 * will be changed at any time by Nordic Semiconductor ASA.
 *
 * Server implementations such as the ones found in iOS can be changed at any 
 * time by Apple and may cause this client implementation to stop working.
 *
 * This file contains the source code for a sample application using the Apple 
 * Notification Center Service Client.
 */

#include "peripheral_define.h"


#if BUTTONS_NUMBER < 2
#error "Not enough resources on board"
#endif

#if (NRF_SD_BLE_API_VERSION == 3)
#define NRF_BLE_MAX_MTU_SIZE           GATT_MTU_SIZE_DEFAULT                       /**< MTU size used in the softdevice enabling and to reply to a BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST event. */
#endif

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define CENTRAL_LINK_COUNT             0                                           /**< The number of central links used by the application. When changing this number remember to adjust the RAM settings. */
#define PERIPHERAL_LINK_COUNT          1                                           /**< The number of peripheral links used by the application. When changing this number remember to adjust the RAM settings. */
#define VENDOR_SPECIFIC_UUID_COUNT     10                                          /**< The number of vendor specific UUIDs used by this example. */

#define ATTR_DATA_SIZE                 BLE_ANCS_ATTR_DATA_MAX                      /**< Allocated size for attribute data. */

#define DISPLAY_MESSAGE_BUTTON_ID      1                                           /**< Button used to request notification attributes. */

#define DEVICE_NAME                    "sgnl_bleU"                                      /**< Name of the device. Will be included in the advertising data. */
#define APP_ADV_FAST_INTERVAL          40                                          /**< The advertising interval (in units of 0.625 ms). The default value corresponds to 25 ms. */
#define APP_ADV_SLOW_INTERVAL          3200                                        /**< Slow advertising interval (in units of 0.625 ms). The default value corresponds to 2 seconds. */
#define APP_ADV_FAST_TIMEOUT           180   //3 min                                         /**< The advertising time-out in units of seconds. */
#define APP_ADV_SLOW_TIMEOUT           3420  //57 min                                       /**< The advertising time-out in units of seconds. */
#define ADV_INTERVAL_FAST_PERIOD       30                                          /**< The duration of the fast advertising period (in seconds). */

#define APP_TIMER_PRESCALER            0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE        7//5                                           /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL              MSEC_TO_UNITS(200, UNIT_1_25_MS)            /**< Minimum acceptable connection interval (0.5 seconds). */
#define MAX_CONN_INTERVAL              MSEC_TO_UNITS(300, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (1 second). */
#define SLAVE_LATENCY                  0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT               MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory time-out (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time from initiating an event (connect or start of notification) to the first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /**< Time between each call to sd_ble_gap_conn_param_update after the first (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT   3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define MESSAGE_BUFFER_SIZE            18                                          /**< Size of buffer holding optional messages in notifications. */

#define SECURITY_REQUEST_DELAY         APP_TIMER_TICKS(1500, APP_TIMER_PRESCALER)  /**< Delay after connection until security request is sent, if necessary (ticks). */

#define SEC_PARAM_BOND                 1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                 0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                 0                                           /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS             0                                           /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES      BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                  0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE         7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE         16                                          /**< Maximum encryption key size. */

#define DEAD_BEEF                      0xDEADBEEF                                  /**< Value used as error code on stack dump. Can be used to identify stack location on stack unwind. */

#define SCHED_MAX_EVENT_DATA_SIZE      MAX(APP_TIMER_SCHED_EVT_SIZE, \
                                           BLE_STACK_HANDLER_SCHED_EVT_SIZE)       /**< Maximum size of scheduler events. */
#ifdef SVCALL_AS_NORMAL_FUNCTION
#define SCHED_QUEUE_SIZE               20                                          /**< Maximum number of events in the scheduler queue. More is needed in case of Serialization. */
#else
#define SCHED_QUEUE_SIZE               10                                          /**< Maximum number of events in the scheduler queue. */
#endif

static pm_peer_id_t   m_whitelist_peers[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];  /**< List of peers currently in the whitelist. */
static uint32_t       m_whitelist_peer_cnt;                                 /**< Number of peers currently in the whitelist. */
static bool           m_is_wl_changed;                                      /**< Indicates if the whitelist has been changed since last time it has been updated in the Peer Manager. */

#if defined(BOARD_SGNL)

bitfield_t g_str_bit;
call_bitfield_t g_str_call_bit;

timer_struct_t g_str_timer;
time_info_struct_t g_str_time;

init_status_struct_t g_str_app_status;

#endif //defined(BOARD_SGNL)

#ifdef ENABLE_BATT
batt_struct_t g_str_batt;
#endif //ENABLE_BATT

#ifdef ENABLE_FUEL_GAUGE
fg_batt_status_struct_t g_str_fuel_gauge_batt;
fg_status_sturct_t g_str_fuel_gauge;
#endif //ENABLE_FUEL_GAUGE

#ifdef ENABLE_ANCS_PARSER
ancs_parse_struct_t g_str_ancs;
#endif

menu_bitfield_t g_str_menu;

#ifdef ENABLE_WDT
nrf_drv_wdt_channel_id m_channel_id;
#endif

static ble_db_discovery_t m_ble_db_discovery;                          /**< Structure used to identify the DB Discovery module. */
static pm_peer_id_t       m_peer_id;                                   /**< Device reference handle to the current bonded central. */
static uint16_t           m_cur_conn_handle = BLE_CONN_HANDLE_INVALID; /**< Handle of the current connection. */

APP_TIMER_DEF(m_sec_req_timer_id);                                     /**< Security request timer. The timer lets us start pairing request if one does not arrive from the Central. */

/**@brief Callback function for handling asserts in the SoftDevice.
 *
 * @details This function is called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. 
 *          You must analyze how your product should react to asserts.
 * @warning On assert from the SoftDevice, the system can recover only on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Fetch the list of peer manager peer IDs.
 *
 * @param[inout] p_peers   The buffer where to store the list of peer IDs.
 * @param[inout] p_size    In: The size of the @p p_peers buffer.
 *                         Out: The number of peers copied in the buffer.
 */
static void peer_list_get(pm_peer_id_t * p_peers, uint32_t * p_size)
{
    pm_peer_id_t peer_id;
    uint32_t     peers_to_copy;

    peers_to_copy = (*p_size < BLE_GAP_WHITELIST_ADDR_MAX_COUNT) ?
                     *p_size : BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

    peer_id = pm_next_peer_id_get(PM_PEER_ID_INVALID);
    *p_size = 0;

    while ((peer_id != PM_PEER_ID_INVALID) && (peers_to_copy--))
    {
        p_peers[(*p_size)++] = peer_id;
        peer_id = pm_next_peer_id_get(peer_id);
    }
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    ret_code_t ret;

    memset(m_whitelist_peers, PM_PEER_ID_INVALID, sizeof(m_whitelist_peers));
    m_whitelist_peer_cnt = (sizeof(m_whitelist_peers) / sizeof(pm_peer_id_t));

    peer_list_get(m_whitelist_peers, &m_whitelist_peer_cnt);

    ret = pm_whitelist_set(m_whitelist_peers, m_whitelist_peer_cnt);
    APP_ERROR_CHECK(ret);

    // Setup the device identies list.
    // Some SoftDevices do not support this feature.
    ret = pm_device_identities_list_set(m_whitelist_peers, m_whitelist_peer_cnt);
    if (ret != NRF_ERROR_NOT_SUPPORTED)
    {
        APP_ERROR_CHECK(ret);
    }

    m_is_wl_changed = false;

    ret = ble_advertising_start(BLE_ADV_MODE_FAST);
	
	g_str_bit.b1_adv_status = 1;
	m_nus.is_notification_enabled = false;

    APP_ERROR_CHECK(ret);
}


/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    ret_code_t ret;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            NRF_LOG_DEBUG("Connected to previously bonded device\r\n");
            m_peer_id = p_evt->peer_id;
        } break; // PM_EVT_BONDED_PEER_CONNECTED

        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
            NRF_LOG_INFO("Connection secured. Role: %d. conn_handle: %d, Procedure: %d\r\n",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);

            m_peer_id = p_evt->peer_id;

			g_str_app_status.ui8_ble_bonded = 1;
			app_status_write();
            // Note: You should check on what kind of white list policy your application should use.
            if (p_evt->params.conn_sec_succeeded.procedure == PM_LINK_SECURED_PROCEDURE_BONDING)
            {
                NRF_LOG_DEBUG("New Bond, add the peer to the whitelist if possible\r\n");
                NRF_LOG_DEBUG("\tm_whitelist_peer_cnt %d, MAX_PEERS_WLIST %d\r\n",
                               m_whitelist_peer_cnt + 1,
                               BLE_GAP_WHITELIST_ADDR_MAX_COUNT);

                if (m_whitelist_peer_cnt < BLE_GAP_WHITELIST_ADDR_MAX_COUNT)
                {
                    // Bonded to a new peer, add it to the whitelist.
                    m_whitelist_peers[m_whitelist_peer_cnt++] = m_peer_id;
                    m_is_wl_changed = true;
                }
            }
            ret  = ble_db_discovery_start(&m_ble_db_discovery, p_evt->conn_handle);
            APP_ERROR_CHECK(ret);
        } break;

        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
        } break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;

        case PM_EVT_STORAGE_FULL:
        {
            // Run garbage collection on the flash.
            ret = fds_gc();
            if (ret == FDS_ERR_BUSY || ret == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(ret);
            }
        } break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        {
            advertising_start();
        } break;

        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
        {
            // The local database has likely changed, send service changed indications.
            pm_local_database_has_changed();
        } break;

        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } break;

        case PM_EVT_PEER_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } break;

        case PM_EVT_PEERS_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        } break;

        case PM_EVT_ERROR_UNEXPECTED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } break;

        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}


/**@brief Function for handling the security request timer time-out.
 *
 * @details This function is called each time the security request timer expires.
 *
 * @param[in] p_context  Pointer used for passing context information from the
 *                       app_start_timer() call to the time-out handler.
 */
static void sec_req_timeout_handler(void * p_context)
{
    ret_code_t           ret;
    pm_conn_sec_status_t status;

    if (m_cur_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        ret = pm_conn_sec_status_get(m_cur_conn_handle, &status);
        APP_ERROR_CHECK(ret);

        // If the link is still not secured by the peer, initiate security procedure.
        if (!status.encrypted)
        {
            ret = pm_conn_secure(m_cur_conn_handle, false);
            if (ret != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(ret);
            }
        }
    }
}


/**@brief Function for initializing the timer module.
 */
static void timers_init(void)
{
    ret_code_t ret;

    // Initialize timer module, making it use the scheduler.
    APP_TIMER_APPSH_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, true);

    // Create security request timer.
    ret = app_timer_create(&m_sec_req_timer_id,
                           APP_TIMER_MODE_SINGLE_SHOT,
                           sec_req_timeout_handler);
	
	APP_ERROR_CHECK(ret);
	
}


/**@brief Function for initializing the GAP.
 *
 * @details Use this function to set up all necessary GAP (Generic Access Profile)
 *          parameters of the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    ret_code_t              ret;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    ret = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(ret);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    ret = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(ret);
}




/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             ret;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = true;
    cp_init.evt_handler                    = NULL;
    cp_init.error_handler                  = conn_params_error_handler;

    ret = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(ret);
}


/**@brief Function for handling Database Discovery events.
 *
 * @details This function is a callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective service instances.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
#ifdef ENABLE_ANCS
	if( p_evt->params.discovered_db.srv_uuid.uuid == ANCS_UUID_SERVICE )
	{
    	ble_ancs_c_on_db_disc_evt(&m_ancs_c, p_evt);
	}
#endif
	
#ifdef ENABLE_CTS
	if( p_evt->params.discovered_db.srv_uuid.uuid == BLE_UUID_CURRENT_TIME_SERVICE)
	{
		ble_cts_c_on_db_disc_evt(&m_cts, p_evt);
	}
#endif
}


/**@brief Function for the Peer Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Peer Manager.
 */
static void peer_manager_init(bool erase_bonds)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           ret;

    ret = pm_init();
    APP_ERROR_CHECK(ret);

    if (erase_bonds)
    {
        ret = pm_peers_delete();
        APP_ERROR_CHECK(ret);
    }

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;
    
    ret = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(ret);

    ret = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(ret);
}

#if 0
/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t ret = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(ret);

    // Prepare wakeup buttons.
    ret = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(ret);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    ret = sd_power_system_off();
    APP_ERROR_CHECK(ret);
}
#endif

/**@brief Function for handling advertising events.
 *
 * @details This function is called for advertising events that are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t ret;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            NRF_LOG_INFO("Fast advertising\r\n");
            ret = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(ret);
            break;

        case BLE_ADV_EVT_SLOW:
            NRF_LOG_INFO("Slow advertising\r\n");
            ret = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
            APP_ERROR_CHECK(ret);
            break;

        case BLE_ADV_EVT_FAST_WHITELIST:
            NRF_LOG_INFO("Fast advertising with Whitelist\r\n");
            ret = bsp_indication_set(BSP_INDICATE_ADVERTISING_WHITELIST);
            APP_ERROR_CHECK(ret);
            break;

        case BLE_ADV_EVT_IDLE:
            //sleep_mode_enter();
			advertising_start();
            break;
			
		case BLE_ADV_EVT_SLOW_WHITELIST:
			break;

        case BLE_ADV_EVT_WHITELIST_REQUEST:
        {
            ble_gap_addr_t whitelist_addrs[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
            ble_gap_irk_t  whitelist_irks[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
            uint32_t       addr_cnt = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
            uint32_t       irk_cnt  = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

            ret = pm_whitelist_get(whitelist_addrs, &addr_cnt, whitelist_irks, &irk_cnt);
            APP_ERROR_CHECK(ret);
            NRF_LOG_DEBUG("pm_whitelist_get returns %d addr in whitelist and %d irk whitelist\r\n",
                           addr_cnt,
                           irk_cnt);

            // Apply the whitelist.
            ret = ble_advertising_whitelist_reply(whitelist_addrs,
                                                  addr_cnt,
                                                  whitelist_irks,
                                                  irk_cnt);
            APP_ERROR_CHECK(ret);

        }
        break;

        default:
            break;
    }
}


/**@brief Function for handling the application's BLE stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    ret_code_t ret = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected.\r\n");
            ret = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(ret);

            m_cur_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            ret               = app_timer_start(m_sec_req_timer_id, SECURITY_REQUEST_DELAY, NULL);
            APP_ERROR_CHECK(ret);
			
			g_str_bit.b1_conn_status = 1;
			g_str_bit.b1_adv_status = 0;
			
			g_str_bit.b1_ble_conn_noti = 1;
			g_str_timer.ui16_ble_con_cnt = 0;
			
            break; // BLE_GAP_EVT_CONNECTED

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.\r\n");
            m_cur_conn_handle = BLE_CONN_HANDLE_INVALID;
            ret               = app_timer_stop(m_sec_req_timer_id);
            APP_ERROR_CHECK(ret);

            if (p_ble_evt->evt.gap_evt.conn_handle == m_ancs_c.conn_handle)
            {
                m_ancs_c.conn_handle = BLE_CONN_HANDLE_INVALID;
            }
            if (m_is_wl_changed)
            {
                // The whitelist has been modified, update it in the Peer Manager.
                ret = pm_whitelist_set(m_whitelist_peers, m_whitelist_peer_cnt);
                APP_ERROR_CHECK(ret);

                ret = pm_device_identities_list_set(m_whitelist_peers, m_whitelist_peer_cnt);
                if (ret != NRF_ERROR_NOT_SUPPORTED)
                {
                    APP_ERROR_CHECK(ret);
                }

                m_is_wl_changed = false;
            }
			
			g_str_bit.b1_conn_status = 0;
			g_str_bit.b1_adv_status = 1;
			
			g_str_bit.b1_ble_conn_noti = 1;
			g_str_timer.ui16_ble_con_cnt = 0;
#ifdef ENABLE_CTS
			g_str_bit.b1_cts_ready = 1;
#endif
			m_nus.is_notification_enabled = false;
            break; // BLE_GAP_EVT_DISCONNECTED

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.\r\n");
            ret = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                        BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(ret);
            break; // BLE_GATTC_EVT_TIMEOUT

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.\r\n");
            ret = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                        BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(ret);
            break; // BLE_GATTS_EVT_TIMEOUT

		case BLE_EVT_USER_MEM_REQUEST:
			{
				uint8_t data[3];
				uint8_t ui8_index = 0;
				data[++ui8_index] = REPORT_ERR_MSG;
				data[++ui8_index] = REP_ERR_DATA_LEN;
				data[0] = ++ui8_index;
				
				nus_tx_push(data,data[0]);
			}
            ret = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(ret);
            break; // BLE_EVT_USER_MEM_REQUEST

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    ret = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(ret);
                }
            }
        } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST
#if (NRF_SD_BLE_API_VERSION == 3)
        case BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST:
            ret = sd_ble_gatts_exchange_mtu_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                  NRF_BLE_MAX_MTU_SIZE);
            APP_ERROR_CHECK(ret);
            break; // BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST
#endif
			
#if defined(BOARD_SGNL)
		case BLE_GAP_EVT_TIMEOUT :
			{
				if( p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISING )
				{
				}
			}
			break;

		case BLE_EVT_TX_COMPLETE :
			if( g_str_bit.b1_pedo_transmit )
			{
				g_str_bit.b1_pedo_transmit = 0;
			}
			g_str_bit.b1_ble_send_wait = 0;
			break;
#endif

		default:
            // No implementation needed.
            break;
    }
    APP_ERROR_CHECK(ret);
	
#if defined(BOARD_SGNL)
	if( !g_str_bit.b1_nus_enable )
	{
		if( m_nus.is_notification_enabled )
		{
			uint8_t byte[3] = "";
			uint8_t nus_index = 0;

			byte[++nus_index] = REPORT_APP_CONFIG;
			byte[++nus_index] = REP_CONFIG_BLE_READY;
			byte[0] = ++nus_index;
			g_str_bit.b1_nus_enable = 1;
			nus_tx_push(byte,byte[0]);
		}
	}
	else
	{
		if( !m_nus.is_notification_enabled )
		{
			g_str_bit.b1_nus_enable = 0;
		}
	}
#endif //if defined(BOARD_SGNL)
}

#if 0

/**@brief Function for handling events from the BSP module.
 *
 * @param[in] event  Event generated by button press.
 */
static void bsp_event_handler(bsp_event_t event)
{
    button_event(event);
}
#endif

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    /** The Connection state module has to be fed BLE events in order to function correctly
     * Remember to call ble_conn_state_on_ble_evt before calling any ble_conns_state_* functions. */
    ble_conn_state_on_ble_evt(p_ble_evt);
    pm_on_ble_evt(p_ble_evt);
    ble_db_discovery_on_ble_evt(&m_ble_db_discovery, p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_ancs_c_on_ble_evt(&m_ancs_c, p_ble_evt);
#ifdef ENABLE_CTS
	ble_cts_c_on_ble_evt(&m_cts, p_ble_evt);
#endif
#ifdef ENABLE_NUS
	ble_nus_on_ble_evt(&m_nus, p_ble_evt);
#endif //ENABLE_NUS
    //bsp_btn_ble_on_ble_evt(p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
}


/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the system event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    // Dispatch the system event to the fstorage module, where it will be
    // dispatched to the Flash Data Storage (FDS) module.
    fs_sys_event_handler(sys_evt);

    // Dispatch to the Advertising module last, since it will check if there are any
    // pending flash operations in fstorage. Let fstorage process system events first,
    // so that it can report correctly to the Advertising module.
    ble_advertising_on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t ret;

    nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
    
    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
    
    ble_enable_params_t ble_enable_params;
    ret = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                               PERIPHERAL_LINK_COUNT,
                                               &ble_enable_params);
    APP_ERROR_CHECK(ret);

    ble_enable_params.common_enable_params.vs_uuid_count = VENDOR_SPECIFIC_UUID_COUNT;

    // Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT, PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
#if (NRF_SD_BLE_API_VERSION == 3)
    ble_enable_params.gatt_enable_params.att_mtu = NRF_BLE_MAX_MTU_SIZE;
#endif
    ret = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(ret);

    // Register with the SoftDevice handler module for BLE events.
    ret = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(ret);

    // Register with the SoftDevice handler module for System events.
    ret = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(ret);
}


/**@brief Function for initializing the Apple Notification Center Service.
 */
static void services_init(void)
{
	ancs_service_init();
	
#ifdef ENABLE_CTS
	cts_service_init();
#endif

#ifdef ENABLE_NUS
	nus_service_init();
#endif
}

/**@brief Function for initializing the advertising functionality.
 */
static void advertising_init(void)
{
    ret_code_t             ret;
    ble_advdata_t          advdata;
    ble_adv_modes_config_t options;

    static ble_uuid_t m_adv_uuids[1]; /**< Universally unique service identifiers. */

#ifdef ENABLE_CTS
	#ifdef ENABLE_NUS
	ble_advdata_t		   srdata;
	ble_uuid_t adv_uuids[] = {{BLE_UUID_CURRENT_TIME_SERVICE, BLE_UUID_TYPE_BLE},{BLE_UUID_NUS_SERVICE, m_nus.uuid_type}};
	#endif

#else
	#ifdef ENABLE_NUS	
	ble_advdata_t		   srdata;
	ble_uuid_t adv_uuids[] = {{BLE_UUID_NUS_SERVICE, m_nus.uuid_type}};
	#endif
#endif

    m_adv_uuids[0].uuid = ANCS_UUID_SERVICE;
    m_adv_uuids[0].type = m_ancs_c.service.service.uuid.type;

    // Build and set advertising data
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type                = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance       = false;
    advdata.flags                    = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;
    advdata.uuids_complete.uuid_cnt  = 0;
    advdata.uuids_complete.p_uuids   = NULL;
    advdata.uuids_solicited.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    advdata.uuids_solicited.p_uuids  = m_adv_uuids;

#if defined(ENABLE_NUS)|| defined(ENABLE_CTS)
	memset(&srdata, 0, sizeof(srdata));
    srdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    srdata.uuids_complete.p_uuids = adv_uuids;
#endif //ENABLE_NUS

    memset(&options, 0, sizeof(options));
    options.ble_adv_whitelist_enabled = true;
    options.ble_adv_fast_enabled      = true;
    options.ble_adv_fast_interval     = APP_ADV_FAST_INTERVAL;
    options.ble_adv_fast_timeout      = APP_ADV_FAST_TIMEOUT;
    options.ble_adv_slow_enabled      = true;
    options.ble_adv_slow_interval     = APP_ADV_SLOW_INTERVAL;
    options.ble_adv_slow_timeout      = APP_ADV_SLOW_TIMEOUT;

#if defined(ENABLE_NUS)|| defined(ENABLE_CTS)
    ret = ble_advertising_init(&advdata, &srdata, &options, on_adv_evt, NULL);
#else
    ret = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
#endif //ENABLE_NUS
    APP_ERROR_CHECK(ret);
}

/**@brief Function for initializing buttons and LEDs.
 *
 * @param[out] p_erase_bonds  True if the clear bonds button was pressed to wake the application up.
 */
//static void buttons_leds_init(bool * p_erase_bonds)
#if 0
static void buttons_leds_init( void )
{
    bsp_event_t startup_event;

    //uint32_t ret = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
	uint32_t ret = bsp_init(BSP_INIT_BUTTONS,
                            APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
                            bsp_event_handler);
    APP_ERROR_CHECK(ret);

    ret = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(ret);

    //*p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}
#endif
/**@brief Function for initializing the Event Scheduler.
 */
static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}


/**@brief Function for initializing the database discovery module.
 */
static void db_discovery_init(void)
{
    ret_code_t ret = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(ret);
}


/**@brief Function for initializing the nrf log module.
 */
#if NRF_LOG_ENABLED
static void log_init(void)
{
    ret_code_t ret = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(ret);
}
#endif

/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    ret_code_t ret = sd_app_evt_wait();
    APP_ERROR_CHECK(ret);
}

static void sgnl_struct_init(void)
{
	memset(&g_str_bit, 0, sizeof(bitfield_t));
	g_str_bit.b1_haptic_status = 1;
	g_str_bit.b1_por_status = 1;
	
	memset(&g_str_timer, 0, sizeof(timer_struct_t));
	
#ifdef ENABLE_BATT
	memset(&g_str_batt, 0, sizeof(batt_struct_t));
#endif
}

static void power_off_sequence( void )
{
#ifdef ENABLE_PATTERN
	call_power_off();
#endif
}
#ifdef ENABLE_WDT
void wdt_event_handler(void)
{
	force_off_pattern();
}
#endif

/**@brief Function for application main entry.
 */
int main(void)
{
    bool erase_bonds;

    // Initialize.
#if NRF_LOG_ENABLED
    log_init();
#endif
	
	timers_init();
	
#if defined(BOARD_SGNL)
	
	sgnl_struct_init();

	user_flash_init();
	
	//user_flash_all_erase();
	//user_flash_erase(USER_FLASH_PACKAGE_BLOCK);

	app_status_read();
	
	user_timers_init();
	
	if( g_str_app_status.ui8_ble_bonded )
	{
		erase_bonds = false;
	}
	else
	{
		erase_bonds = true;
		
		if( !g_str_app_status.ui8_app_noti_status )
		{
			g_str_app_status.ui8_app_noti_status = 1;

			g_str_app_status.ui16_longsit_start_time = 9<<8; // default 9 AM
			g_str_app_status.ui16_longsit_end_time = 18<<8; // default 6 PM
			//app_status_write();
		}
	}

	#ifdef ENABLE_GPIO
		gpio_init();
	#endif
	
	#ifdef ENABLE_ADC
		adc_init();
	#endif
		
	#ifdef ENABLE_TWI
		twi_init();
		
		#ifdef ENABLE_TOUCHKEY
		touchkey_init();
		#endif
		
		#ifdef ENABLE_SSD1306
		oled_init();
		#endif

		#ifdef ENABLE_PEDOMETER
		if( init_motion_sensor_driver() )
		{
			uint8_t data[3];
			uint8_t ui8_index = 0;
			data[++ui8_index] = REPORT_ERR_MSG;
			data[++ui8_index] = REP_ERR_PEDO_INIT;
			data[0] = ++ui8_index;
			
			nus_tx_push(data,data[0]);
			// pedo error
			g_str_bit.b1_pedo_err = 1;
		}
		
		pedo_flash_time_sync();

		#endif
		
		#ifdef ENABLE_FUEL_GAUGE
		fuel_gauge_init();
		#endif
	#endif
#endif

    ble_stack_init();
    peer_manager_init(erase_bonds);
    if (erase_bonds == true)
    {
        NRF_LOG_INFO("Bonds erased!\r\n");
    }
    db_discovery_init();
    scheduler_init();
    gap_params_init();
    services_init();
    advertising_init();
    conn_params_init();
	
#if defined(BOARD_SGNL)
	
	if(nrf_drv_gpiote_in_is_set(CHK_VBUS))
	{
		g_str_bit.b1_chg_status = 1;
		g_str_batt.ui16_batt_status = BATT_STATUS_IN_CHARGING;
		g_str_batt.ui8_low_level = BATT_LOW_NONE;
	}
	
	g_str_bit.b1_adv_status = 1;
	g_str_bit.b1_por_status = 0;
	m_nus.is_notification_enabled = false;
#endif

#ifdef ENABLE_IFA_DEMO
	while( !g_str_batt.ui16_batt_level && !g_str_batt.ui16_batt_status );

	while( g_str_batt.ui16_percent == 0 || g_str_batt.ui16_batt_status )
	{
		
#ifdef ENABLE_TOUCHKEY
		if( g_str_bit.b2_touch_sense )
		{
			touch_event();
		}

		if( g_str_bit.b1_touch_read_ready )
		{
			if( !g_str_bit.b1_twi_send_wait )
			{
				Host_Read_Status();
				g_str_bit.b1_touch_read_ready = 0;
			}
		}
#endif
		app_sched_execute();
		power_manage();
	}
	
#endif

#ifdef ENABLE_WDT
	//Configure WDT.
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
	config.reload_value = 5000;//5s
    uint32_t err_code = NRF_SUCCESS;
	 
	err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrf_drv_wdt_enable();
#endif
	
	// Start execution.
	NRF_LOG_INFO("BLE ANCS Started\r\n");
	advertising_start();

    // Enter main loop.
    for (;;)
    {
#ifdef ENABLE_ANCS_PARSER
		if( g_str_bit.b1_ancs_received )
		{
			parsing_ancs();
			g_str_bit.b1_ancs_received = 0;
		}
#endif //ENABLE_ANCS_PARSER

#ifdef ENABLE_NUS_QUEUE
		nus_rx_queue();
		
		if( g_str_bit.b1_nus_enable )
		{
			if( !g_str_bit.b1_ble_send_wait )
			{
				nus_tx_queue();
			}
		}
#endif

#ifdef ENABLE_PEDOMETER
		pedometer();
#endif
		
#ifdef ENABLE_TOUCHKEY
		if( g_str_bit.b2_touch_sense )
		{
			touch_event();
		}

		if( g_str_bit.b1_touch_read_ready )
		{
			if( !g_str_bit.b1_twi_send_wait )
			{
				Host_Read_Status();
				g_str_bit.b1_touch_read_ready = 0;
			}
		}
#endif

		
		if( g_str_bit.b1_power_off )
		{
			power_off_sequence();
			g_str_bit.b1_power_off = 0;
		}

		app_sched_execute();
#ifdef ENABLE_WDT
		nrf_drv_wdt_channel_feed(m_channel_id);
#endif
        power_manage();
    }

}


/**
 * @}
 */

