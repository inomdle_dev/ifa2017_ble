/*****************************************************************************
* File Name: CY8CMBR3xxx_HostFunctions.c
*
* Version 1.00
*
* Description:
*   This file contains the definitions of the low-level APIs. You may need to 
*   modify the content of these APIs to suit your host processor’s I2C 
*   implementation.
*
* Note:
*   These host-dependent Low Level APIs are provided as an example of
*   low level I2C read and write functions. This set of low level APIs are 
*   written for PSoC 4200/4100 devices and hence should be re-written
*   with equivalent host-dependent APIs from the respective IDEs, if the 
*   host design does not include PSoC 4200/4100 device.
* 
*   To use these APIs, the host should implement a working I2C communication
*   interface. This interface will be used by these APIs to communicate to the
*   CY8CMBR3xxx device.
*
*   For PSoC 4200/4100 devices, please ensure that you have created an instance 
*   of SCB component with I2C Master configuration. The component should be
*   named "SCB".
*
* Owner:
*   SRVS
*
* Related Document:
*   MBR3 Design Guide
*   MBR3 Device Datasheet
*
* Hardware Dependency:
*   PSoC 4200 (Update this as per the host used)
*
* Code Tested With:
*   PSoC Creator 3.0 CP7
*   CY3280-MBR3 Evaluation Kit
*   CY8CKIT-042 Pioneer Kit
*
******************************************************************************
* Copyright (2014), Cypress Semiconductor Corporation.
******************************************************************************
* This software is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and
* foreign), United States copyright laws and international treaty provisions.
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the
* Cypress Source Code and derivative works for the sole purpose of creating
* custom software in support of licensee product to be used only in conjunction
* with a Cypress integrated circuit as specified in the applicable agreement.
* Any reproduction, modification, translation, compilation, or representation of
* this software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH
* REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising out
* of the application or use of any product or circuit described herein. Cypress
* does not authorize its products for use as critical components in life-support
* systems where a malfunction or failure may reasonably be expected to result in
* significant injury to the user. The inclusion of Cypress' product in a life-
* support systems application implies that the manufacturer assumes all risk of
* such use and in doing so indemnifies Cypress against all charges. Use may be
* limited by and subject to the applicable Cypress software license agreement.
*****************************************************************************/

/*******************************************************************************
* Included headers
*******************************************************************************/
#include "peripheral_define.h"
//#include "MBR3_Configuration.h"
/*******************************************************************************
* API Constants
*******************************************************************************/
#define CY8CMBR3xxx_READ                    (1)
#define CY8CMBR3xxx_WRITE                   (0)
#define CY8CMBR3xxx_ACK                     (0)
#define CY8CMBR3xxx_NACK                    (1)
#define CY8CMBR3xxx_READ_BYTE_ERROR         (0x80000000)

/* The following macro defines the maximum number of times the low-level read
 * and write functions try to communicate with the CY8CMBR3xxx device, as
 * long as the I2C communication is unsuccessful.
 */
#define CY8CMBR3xxx_RETRY_TIMES             (10)    


/***************************************
* Project Constants
***************************************/
#define SLAVE_ADDRESS                   0x37
#define LED_ON                          0x00
#define LED_OFF                         0x01
#define GPO_OUTPUT_GPO0_MASK            0x01
#define GPO_LOW                         0x00
#define PROXIMITY_DETECT                0x01
#define PROXIMITY_TOUCH                 0x01
#define BUTTON_TOUCH                    0x78
#define GPO_OUTPUT_STATE_OFFSET         0x01
#define GPO_OUTPUT_STATE_WRITE_LENGTH   0x02
#define GPO_OUTPUT_STATE_READ_LENGTH    0x01
#define GPO_CFG_HOST_CONTROL_EN         0xFF
#define GPO_OUTPUT_STATE_ALL_OFF        0xFF
#define GPO_CFG_WRITE_LENGTH            0x02

/***************************************
* Delay Constants
***************************************/
#define PROX_LED_BLINK_DELAY            (50)
#define MBR3_BOOT_DELAY                 (1000)
 
#ifdef ENABLE_TOUCHKEY

const unsigned char CY8CMBR3108_configuration[128] = {
    0x0Bu, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x80u, 0x80u, 0x7Fu, 0x80u,
    0x7Fu, 0x7Fu, 0x7Fu, 0x7Fu, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x03u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x80u,
    0x05u, 0x00u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x1Eu, 0x1Eu, 0x00u,
    0x00u, 0x1Eu, 0x1Eu, 0x00u, 0x00u, 0x00u, 0x01u, 0x01u,
    0x00u, 0xFFu, 0xFFu, 0xFFu, 0xFFu, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x04u, 0x03u, 0x01u, 0x58u,
    0x00u, 0x37u, 0x06u, 0x00u, 0x00u, 0x0Au, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x15u, 0xB3u
};
/*******************************************************************************
*   Function Code
*******************************************************************************/

/*******************************************************************************
* Function Name: Host_LowLevelWrite
********************************************************************************
*
* Summary:
*  This API writes to the register map of the CY8CMBR3xxx device using the I2C 
*  communication protocol. The implementation is host processor dependent and 
*  you may need to update the API code to suit your host.
*
* Parameters:
*  uint8 slaveAddress:
*   The I2C address of the CY8CMBR3xxx device. Valid range: 8 - 119
*
*  uint8 *writeBuffer:
*   The buffer from which data is written to the device. 
*
*   The first element should always contain the location of the register 
*   of the device to write to. This value can be within 0 – 251.
*
*   Each successive element should contain the data to be written to that 
*   register and the successive registers. These elements can have a value 
*   between 0 – 255. The number of data bytes can be between 0 and 128.
*
*  uint8 numberOfBytes:
*   Number of bytes to be written, equal to the number of elements in the 
*   buffer (i.e. number of data bytes + 1)
*
* Pre:
*  The I2C interface should be enabled and working before this Low Level 
*  API can be called. Also make sure that the Global Interrupts are also
*  enabled (if required)
*
* Post:
*  N/A
*
* Return:
*  status
*    Value                Description
*    TRUE                 Write process was successful
*    FALSE                Write process was not successful
*
*******************************************************************************/
uint8 Host_LowLevelWrite(uint8 slaveAddress, uint8 *writeBuffer, uint8 numberOfBytes)
{
	
    /* Return the status */
    return touch_twi_write(slaveAddress, writeBuffer, numberOfBytes );
}

/*******************************************************************************
* Function Name: Host_LowLevelRead
********************************************************************************
*
* Summary:
*  This API reads from the register map of the CY8CMBR3xxx device using the 
*  I2C communication protocol. The implementation is host processor dependent 
*  and you may need to update the API code to suit your host.
*
* Parameters:
*  uint8 slaveAddress:
*   The I2C address of the CY8CMBR3xxx device. Valid range: 8 - 119
*
*  uint8 *readBuffer:
*   The buffer to be updated with the data read from the device.
*
*   The register location to read from should be set prior to calling 
*   this API. Each successive element to contain the data read from that 
*   register and the successive registers. These elements can have a value 
*   between 0 – 255.
*
*  uint8 numberOfBytes:
*   Number of data bytes to be read, equal to the number of elements in 
*   the buffer. Valid range: 1 – 252
*
* Pre:
*  The I2C interface should be enabled and working before this Low Level 
*  API can be called. Also make sure that the Global Interrupts are also
*  enabled (if required)
*
* Post:
*  N/A
*
* Return:
*  status
*    Value                Description
*    TRUE                 Read process was successful
*    FALSE                Read process was not successful
*
*******************************************************************************/
typedef enum key_status{
	TOUCHKEY_NONE = 0x00,
	TOUCHKEY_0 = 0x08,
	TOUCHKEY_1 = 0x01,
	TOUCHKEY_2 = 0x02,
}touch_status;

void touch_recog(uint16_t btn_status)
{
	static uint16_t last_status = TOUCHKEY_NONE;

	if( btn_status > 0 ) //pressed
	{
		//fist touch
		if( last_status == 0 )
		{
			//longpress timer on
			g_str_bit.b1_long_touch = 1;
		}
	}
	else //released
	{
		
		g_str_bit.b1_long_touch = 0;
		g_str_timer.ui16_reset_cnt = 0;
		
		if ( !g_str_bit.b1_reset_flag )
		{
			if( g_str_bit.b2_touch_sense == TOUCH_NONE)
			{
				g_str_bit.b2_touch_sense = TOUCH_CLICK;
			}
		}
	}
	
	last_status = btn_status;
	UNUSED_VARIABLE(last_status);
}

void Host_Read_Status(void)
{
	static uint16 prev_btn_status = 0x00;
	CY8CMBR3xxx_SENSORSTATUS sensorStatus;

	if (TRUE == CY8CMBR3xxx_ReadSensorStatus(SLAVE_ADDRESS, &sensorStatus))
	{
		/* Proceed if the Proximity Status changed for PS0 */
		if (prev_btn_status != sensorStatus.buttonStatus)
		{
			/* New status becomes the previous status */
			prev_btn_status = sensorStatus.buttonStatus;
			
			touch_recog(prev_btn_status);

			/* Turn ON LED on GPO0 (LED for Proximity Sensor PS0) */
			//writeGPOOutputState[GPO_OUTPUT_STATE_OFFSET] = ~GPO_OUTPUT_GPO0_MASK;
			//CY8CMBR3xxx_WriteData(SLAVE_ADDRESS, writeGPOOutputState, GPO_OUTPUT_STATE_WRITE_LENGTH);
			
			/* Wait for some time */
			//CyDelay(PROX_LED_BLINK_DELAY);
			
			/* Turn OFF LED on GPO0 (LED for Proximity Sensor PS0) */
			//writeGPOOutputState[GPO_OUTPUT_STATE_OFFSET] = GPO_OUTPUT_STATE_ALL_OFF;
			//CY8CMBR3xxx_WriteData(SLAVE_ADDRESS, writeGPOOutputState, GPO_OUTPUT_STATE_WRITE_LENGTH);
		}
		else /* No proximity status change */
		{
			/* Proceed if a Proximity Touch event or a Button Touch event was detected */
			/* Make corresponding LED on GPO glow as long as a touch is detected */
			/* and turn that LED off when the finger is lifted */
			//writeGPOOutputState[GPO_OUTPUT_STATE_OFFSET] = ~sensorStatus.buttonStatus;
			//CY8CMBR3xxx_WriteData(SLAVE_ADDRESS, writeGPOOutputState, GPO_OUTPUT_STATE_WRITE_LENGTH);
		}
	}
}


uint8 Host_LowLevelRead(uint8 slaveAddress, uint8 *readBuffer, uint8 numberOfBytes)
{    
    /* Return the status */
    return touch_twi_read(slaveAddress,readBuffer,numberOfBytes );
}

/*******************************************************************************
* Function Name: Host_LowLevelDelay
********************************************************************************
*
* Summary:
*  This API implements a time-delay function to be used by the High-level APIs. 
*  The delay period is in milliseconds. This delay is achieved by a 
*  code-execution block for the required amount of time.
*
*  The implementation is host processor dependent and you need to update the 
*  API code to suit your host.
*
* Parameters:
*  uint16 milliseconds:
*   The amount of time in milliseconds for which a wait is required. 
*   Valid range: 0 – 65535
*
* Return:
*  None
*
*******************************************************************************/
void Host_LowLevelDelay(uint16 milliseconds)
{
    // Call the host-specific delay implementation
    // Replace this with the correct host delay routine for introducing delays in milliseconds
    nrf_delay_ms(milliseconds);
}

void touchkey_init( void )
{
	//CY8CMBR3xxx_SENSORSTATUS sensorStatus;
    //uint8 prevProximityStatus = 0x00;
    uint8 writeGPOOutputState[2] = {CY8CMBR3xxx_GPO_OUTPUT_STATE, 0x00};
    uint8 writeGPOConfig[2] = {CY8CMBR3xxx_GPO_CFG, GPO_CFG_HOST_CONTROL_EN};
    
    /* Enable Global Interrupts to drive I2C communication */
    //CyGlobalIntEnable;
    
    /* Initialize and Enable I2C communication */
    //SCB_Start();
    
    /* Initialize and enable  */
    //hostInterruptIRQ_StartEx(HOST_INTERRUPT_ISR);
    //bHostInterruptFlag = 0;
    
    /* Turn OFF both the status LEDs */
    //PASS_LED_Write(LED_OFF);
   // FAIL_LED_Write(LED_OFF);
    
    /* Wait a second for the MBR3 device to bootup */
    nrf_delay_ms(MBR3_BOOT_DELAY);
    
    /* Configure the MBR3 device with configuration data from EzClick */
    if(TRUE == CY8CMBR3xxx_Configure(SLAVE_ADDRESS, &CY8CMBR3108_configuration[0]))
    {
		CY8CMBR3xxx_WriteData(SLAVE_ADDRESS, writeGPOConfig, GPO_CFG_WRITE_LENGTH);
		
		/* Turn OFF all LEDs */
		writeGPOOutputState[GPO_OUTPUT_STATE_OFFSET] = GPO_OUTPUT_STATE_ALL_OFF;
		CY8CMBR3xxx_WriteData(SLAVE_ADDRESS, writeGPOOutputState, GPO_OUTPUT_STATE_WRITE_LENGTH);
        /* Indicate a success using GREEN LED */
        //PASS_LED_Write(LED_ON);
        //FAIL_LED_Write(LED_OFF);
	}
	
	/* Make Prox LED Host Controlled */
	
}

/****************************End of File***************************************/
#endif //ENABLE_TOUCHKEY

