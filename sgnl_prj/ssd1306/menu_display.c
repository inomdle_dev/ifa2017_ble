#include "peripheral_define.h"
#ifdef ENABLE_SSD1306
void (*p_menu_func[MENU_COUNT])(void) = {
	menu_heath_display,
	menu_kcal_display,
	menu_batt_display,
	menu_app_noti,
	menu_bt_display
};

uint8_t p_menu_disp[MENU_COUNT][10] = {
	"health   ",
	"kcal     ",
	"battery  ",
	"noti     ",
	"bt_status"
};

uint8_t *arr_icon_health( void )
{
	return (uint8_t *)icon_run;
}

uint8_t *arr_icon_health_step( uint16_t step )
{
	static uint8_t buffer_t[SSD1306_LCDWIDTH * SSD1306_LCDHEIGHT / 8] = {0,};

	memset(buffer_t, 0, sizeof(buffer_t) );
	
	if( step > 9999 )
	{
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j] = innom_9x18_btmap[step/10000][(i-1)*9 + j];
			}
			
		}
	}
	
	if( step > 999 )
	{
		step %= 10000;
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 13] = innom_9x18_btmap[step/1000][(i-1)*9 + j];
			}
			
		}
	}
	
	if( step > 99 )
	{
		step %= 1000;
		
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 26] = innom_9x18_btmap[step/100][(i-1)*9 + j];
			}
			
		}
	}
	
	if( step > 9 )
	{
		step %= 100;
		
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 39] = innom_9x18_btmap[step/10][(i-1)*9 + j];
			}
			
		}
	}

	if( step )
	{
		step %= 10;
	}

	for (int i = 1; i < 4; i++)
	{
		for( int j = 0; j < 9; j++ )
		{
			buffer_t[SSD1306_LCDWIDTH * i + j + 52] = innom_9x18_btmap[step][(i-1)*9 + j];
		}
		
	}
	
	return (uint8_t *)buffer_t;
}

uint8_t *arr_icon_kcal( void )
{
	return (uint8_t *)icon_kcal;
}

uint8_t *arr_icon_kcal_num( uint16_t kcal )
{
	static uint8_t buffer_t[SSD1306_LCDWIDTH * SSD1306_LCDHEIGHT / 8] = {0,};

	memset(buffer_t, 0, sizeof(buffer_t) );
	
	kcal /= 12;
	
	if( kcal > 999 )
	{
		kcal %= 1000;
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 6] = innom_9x18_btmap[kcal/1000][(i-1)*9 + j];
			}
			
		}
	}
	
	if( kcal > 99 )
	{
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 20] = innom_9x18_btmap[kcal/100][(i-1)*9 + j];
			}
			
		}
	}
	
	if( kcal > 9 )
	{
		kcal %= 100;
		
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 32] = innom_9x18_btmap[kcal/10][(i-1)*9 + j];
			}
			
		}
	}

	if( kcal )
	{
		kcal %= 10;
	}

	for (int i = 1; i < 4; i++)
	{
		for( int j = 0; j < 9; j++ )
		{
			buffer_t[SSD1306_LCDWIDTH * i + j + 46] = innom_9x18_btmap[kcal][(i-1)*9 + j];
		}
		
	}
	
	return (uint8_t *)buffer_t;
}


uint8_t *arr_icon_batt( void )
{
	if( g_str_batt.ui16_batt_status == BATT_STATUS_IN_CHARGING )
	{
		return (uint8_t *)icon_batt_chg;
	}
	else
	{
		if( g_str_batt.ui16_level )
		{
			return (uint8_t *)icon_batt;
		}
		else
		{
			return (uint8_t *)icon_batt_low;
		}
	}
}

uint8_t *arr_icon_batt_per( uint16_t batt_per )
{
	static uint8_t buffer_t[SSD1306_LCDWIDTH * SSD1306_LCDHEIGHT / 8] = {0,};

	memset(buffer_t, 0, sizeof(buffer_t) );
	
	for( int i = 0; i < SSD1306_LCDWIDTH * SSD1306_LCDHEIGHT / 8; i++ )
	{
		buffer_t[i] = icon_batt_per[i];
	}
	
	if( batt_per > 99 )
	{
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 6] = innom_9x18_btmap[batt_per/100][(i-1)*9 + j];
			}
			
		}
	}
	
	if( batt_per > 9 )
	{
		batt_per %= 100;
		
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 20] = innom_9x18_btmap[batt_per/10][(i-1)*9 + j];
			}
			
		}
	}

	if( batt_per )
	{
		batt_per %= 10;
	}

	for (int i = 1; i < 4; i++)
	{
		for( int j = 0; j < 9; j++ )
		{
			buffer_t[SSD1306_LCDWIDTH * i + j + 32] = innom_9x18_btmap[batt_per][(i-1)*9 + j];
		}
		
	}
	
	return (uint8_t *)buffer_t;
}

uint8_t *arr_icon_noti( void )
{
	return (uint8_t *)icon_noti;
}

uint8_t *arr_icon_noti_cnt( uint16_t cnt )
{
	static uint8_t buffer_t[SSD1306_LCDWIDTH * SSD1306_LCDHEIGHT / 8] = {0,};

	memset(buffer_t, 0, sizeof(buffer_t) );
	
	if( cnt > 999 )
	{
		cnt %= 1000;
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 6] = innom_9x18_btmap[cnt/1000][(i-1)*9 + j];
			}
			
		}
	}
	
	if( cnt > 99 )
	{
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 20] = innom_9x18_btmap[cnt/100][(i-1)*9 + j];
			}
			
		}
	}
	
	if( cnt > 9 )
	{
		cnt %= 100;
		
		for (int i = 1; i < 4; i++)
		{
			for( int j = 0; j < 9; j++ )
			{
				buffer_t[SSD1306_LCDWIDTH * i + j + 32] = innom_9x18_btmap[cnt/10][(i-1)*9 + j];
			}
			
		}
	}

	if( cnt )
	{
		cnt %= 10;
	}

	for (int i = 1; i < 4; i++)
	{
		for( int j = 0; j < 9; j++ )
		{
			buffer_t[SSD1306_LCDWIDTH * i + j + 46] = innom_9x18_btmap[cnt][(i-1)*9 + j];
		}
		
	}
	
	return (uint8_t *)buffer_t;
}

uint8_t *arr_icon_bt( void )
{
	if( g_str_bit.b1_adv_status )
	{
		if( g_str_app_status.ui8_ble_bonded )
		{
			return (uint8_t *)icon_bt_disconn;
		}
		else
		{
			return (uint8_t *)icon_bt_ready;
		}
	}
	else
	{
		return (uint8_t *)icon_bt_conn;
	}
	
}

uint8_t *arr_icon_bt_con( uint16_t connect )
{
	return (uint8_t *)icon_bt_conn;
}

uint8_t *(*p_icon_func[MENU_COUNT])(void) = {
	arr_icon_health,
	arr_icon_kcal,
	arr_icon_batt,
	arr_icon_noti,
	arr_icon_bt
};

uint8_t *(*p_icon_sub[MENU_COUNT])(uint16_t) = 
{
	arr_icon_health_step,
	arr_icon_kcal_num,
	arr_icon_batt_per,
	arr_icon_noti_cnt,
	arr_icon_bt_con
};

enum {
	MENU_HEALTH,
	MENU_KCAL,
	MENU_BATT,
	MENU_NOTI,
	MENU_BT_STATUS,
};

void menu_icon(uint8_t menu_cnt)
{
	g_str_menu.b3_income = 0;
	force_off_pattern();
	
	if( g_str_bit.b1_chg_status )//in charging
	{
		ssd1306_fill_array((uint8_t *)icon_batt_chg);
		return;
	}

	switch(menu_cnt)
	{
#ifdef ENABLE_PEDOMETER
	case MENU_HEALTH:
		pedo_popup();
		break;
	case MENU_KCAL:
		ssd1306_auto_scroll(p_icon_func[menu_cnt](), p_icon_sub[menu_cnt](g_ui32_pedometer_times), 50);
		break;
#endif
	case MENU_BATT:
		batt_popup();
		break;
	case MENU_NOTI:
		app_notify();
		break;
	case MENU_BT_STATUS:
		bt_status_popup();
		break;
	default : break;
	}
}

void app_notify(void)
{
	if( g_str_app_status.ui8_host_device ) //ios
	{
		ssd1306_auto_scroll(p_icon_func[MENU_NOTI](), p_icon_sub[MENU_NOTI](g_str_ancs.ui8_category_cnt), 50);
	}
	else //Android
	{
#ifdef ENABLE_IFA_DEMO
		static uint8_t noti_cnt = 0;
#endif
		ssd1306_auto_scroll(p_icon_func[MENU_NOTI](), p_icon_sub[MENU_NOTI](++noti_cnt&0x07), 50);
	}
}

void batt_popup(void)
{
	ssd1306_auto_scroll(p_icon_func[MENU_BATT](), p_icon_sub[MENU_BATT](g_str_batt.ui16_percent), 50);
}

void incoming_call_popup(void)
{
	ssd1306_auto_scroll((uint8_t *)icon_call, (uint8_t *)icon_call_name1, 100);
	
	g_str_menu.b3_income = 1;
}

void bt_status_popup(void)
{
	ssd1306_fill_array(p_icon_func[MENU_BT_STATUS]());
}

#ifdef ENABLE_IFA_DEMO
void bt_status_conn(uint8_t status)
{
	switch( status )
	{
	case 0://disconn
		ssd1306_fill_array((uint8_t *)icon_bt_disconn);
		break;
	case 1://conn
		ssd1306_fill_array((uint8_t *)icon_bt_conn);
		break;
	default :
		ssd1306_fill_array((uint8_t *)icon_bt_ready);
		break;
	}
}
#endif

#ifdef ENABLE_PEDOMETER
void pedo_popup(void)
{
	static uint32_t prev_steps = 0;
	ssd1306_auto_scroll(p_icon_func[MENU_HEALTH](), p_icon_sub[MENU_HEALTH](g_ui32_pedometer_steps), 50);
	
	if( g_ui32_pedometer_steps )
	{
		if( g_ui32_pedometer_steps != prev_steps )
		{
			{
				g_str_bit.b1_live_update = 1;
			}
		}
		prev_steps = g_ui32_pedometer_steps;
	}
}
#endif // ENABLE_PEDOMETER

#endif
