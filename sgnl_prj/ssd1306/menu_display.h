#ifndef _MENU_DISPLAY_H_
#define _MENU_DISPLAY_H_

#define MENU_COUNT	5

void menu_app_noti( void );
void menu_heath_display( void );
void menu_kcal_display( void );
void menu_batt_display( void );
void menu_bt_display( void );

void batt_popup(void);
void app_notify(void);
void incoming_call_popup(void);
void bt_status_popup(void);
void pedo_popup(void);

#ifdef ENABLE_IFA_DEMO
void bt_status_conn(uint8_t);
#endif

void menu_icon(uint8_t menu_cnt);

#endif //_MENU_DISPLAY_H_
