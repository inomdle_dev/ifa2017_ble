#ifndef _ICON_H_
#define _ICON_H_

extern const uint8_t icon_run[];

extern const uint8_t icon_kcal[];
extern const uint8_t icon_kcal_num[];

extern const uint8_t icon_noti[];
extern const uint8_t icon_noti_cnt[];

extern const uint8_t icon_batt[];
extern const uint8_t icon_batt_chg[];
extern const uint8_t icon_batt_low[];
extern const uint8_t icon_batt_per[];

extern const uint8_t icon_bt_ready[];
extern const uint8_t icon_bt_conn[];
extern const uint8_t icon_bt_disconn[];

extern const uint8_t icon_call[];
extern const uint8_t icon_call_name1[];
extern const uint8_t icon_call_name2[];
extern const uint8_t icon_call_name3[];
extern const uint8_t icon_call_name4[];

extern const uint8_t icon_sgnl[];


#endif //_ICON_H_

