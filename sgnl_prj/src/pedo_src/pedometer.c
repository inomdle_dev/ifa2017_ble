//_PEDOMETER_C_
#include "peripheral_define.h"

#ifdef ENABLE_PEDOMETER

#define PEDOMETER_BACKUP_PERIOD			360	//15*24 15 days
#define PEDOMETER_BACKUP_SPARE_PERIOD	120	// 5*24  buffer 5 days

typedef enum BAC_Packet_e_
{
	BAC_DRIVE = (1 << 0),
	BAC_WALK = (1 << 1),
	BAC_RUN = (1 << 2),
	BAC_BIKE = (1 << 3),
	BAC_TILT = (1 << 4),
	BAC_STILL = (1 << 5),
}BAC_Packet_e;

pedometer_struct_t g_str_pedo;

uint16_t g_ui16_backup_still_time = 0;
uint32_t g_ui32_backup_run_step = 0;
uint16_t g_ui16_backup_run_time = 0;
uint32_t g_ui32_backup_walk_step = 0;
uint16_t g_ui16_backup_walk_time = 0;
uint8_t now_state = 0;
uint32_t g_ui32_pedometer_steps = 0;
uint32_t g_ui32_pedometer_times = 0;

void pedometer_data_update( void )
{
	uint32_t bac_ts = 0;
	uint32_t timestamp = 0;

	dmp_get_bac_ts((unsigned long *)&bac_ts); //10ms check
	timestamp = (bac_ts / 10);// unit : 100ms

	switch(now_state)
	{
		case BAC_STILL:
			g_str_pedo.ui16_total_still_time += (timestamp - g_ui16_backup_still_time);
			g_ui16_backup_still_time = timestamp;
			break;

		case BAC_WALK:
			if( g_ui32_pedometer_steps > g_ui32_backup_walk_step )
			{
				g_str_pedo.ui32_total_walk_step += (g_ui32_pedometer_steps - g_ui32_backup_walk_step);
			}
			//g_str_pedo.ui16_total_walk_time += (timestamp - g_ui16_backup_walk_time);
			g_ui32_backup_walk_step = g_ui32_pedometer_steps;
			g_ui16_backup_walk_time = timestamp;
			break;

		case BAC_RUN:
			if( g_ui32_pedometer_steps > g_ui32_backup_run_step )
			{
				g_str_pedo.ui32_total_run_step += (g_ui32_pedometer_steps - g_ui32_backup_run_step);
			}
			//g_str_pedo.ui16_total_run_time += (timestamp - g_ui16_backup_run_time);
			g_ui32_backup_run_step = g_ui32_pedometer_steps;
			g_ui16_backup_run_time = timestamp;
			break;
	}
}

void pedometer_data_transfer(void)
{
	if( g_str_bit.b1_pedo_transmit ) //live_data
	{
		pedometer_data_update();

		uint8_t byte[20] = "";
		uint8_t nus_index = 0;
		uint16_t walktime = g_str_pedo.ui16_total_walk_time;
		uint16_t runtime = g_str_pedo.ui16_total_run_time;

		uint32_t walk = g_str_pedo.ui32_total_walk_step;
		uint32_t run = g_str_pedo.ui32_total_run_step;

		byte[++nus_index] = REPORT_PEDOMETER;
		byte[++nus_index] = REP_PEDO_LIVE_DATA;
		byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff0000 ) >> 16 );
		byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff00 ) >> 8 );
		byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_date & 0xff );
		byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_hour );
		byte[++nus_index] = ( uint8_t )((walk&0xff000000)>>24);
		byte[++nus_index] = ( uint8_t )((walk&0x00ff0000)>>16);
		byte[++nus_index] = ( uint8_t )((walk&0x0000ff00)>>8);
		byte[++nus_index] = ( uint8_t )(walk&0x000000ff);
		byte[++nus_index] = ( uint8_t )((walktime&0xff00)>>8);
		byte[++nus_index] = ( uint8_t )(walktime&0x00ff);
		byte[++nus_index] = ( uint8_t )((run&0xff000000)>>24);
		byte[++nus_index] = ( uint8_t )((run&0x00ff0000)>>16);
		byte[++nus_index] = ( uint8_t )((run&0x0000ff00)>>8);
		byte[++nus_index] = ( uint8_t )(run&0x000000ff);
		byte[++nus_index] = ( uint8_t )((runtime&0xff00)>>8);
		byte[++nus_index] = ( uint8_t )(runtime&0x00ff);

		byte[0] = ++nus_index;
		nus_tx_push(byte, byte[0]);

		g_str_bit.b1_pedo_transmit = 0;
	}
}

void pedometer(void)
{
	if( g_str_bit.b1_pedo_transmit )
	{
		pedometer_data_transfer();
	}

	if( g_str_bit.b1_motion_chk )
	{
		pedometer_check();
	}
}

static uint8_t state_move = 0;
static uint8_t state_high = 0;
static uint8_t state_low = 0;
static uint8_t state_stop = 0;
static uint32_t data_left_in_fifo = 0;
static uint16_t bac_state = 0;
static uint32_t bac_ts = 0;
static uint32_t timestamp = 0;

static uint16_t header = 0;
static uint16_t header2 = 0;

/**
 * @brief processing pedometer data of mpu.
 * @par Parameters None
 * @retval void None
 * @par Required preconditions: None
 */
void pedometer_check(void)
{
	static uint16_t intr_status = 0;
	inv_identify_interrupt(&intr_status);
	if(intr_status & (BIT_MSG_DMP_INT | BIT_MSG_DMP_INT_0))
	{
		// data main loop for header packet from fifo
		do
		{
			// Read FIFO contents and parse it.
			dmp_process_fifo(&data_left_in_fifo, &header, &header2);

			// Activity recognition sample available from DMP FIFO
			if (header2 & ACT_RECOG_SET)
			{
				// Read activity type and associated timestamp out of DMP FIFO
				// activity type is a set of 2 bytes :
				// - high byte indicates activity start
				// - low byte indicates activity end 
				dmp_get_bac_state(&bac_state);
				dmp_get_bac_ts((unsigned long *)&bac_ts); //10ms check
				timestamp = (bac_ts / 10);//unit :100ms 

				// Tilt information is inside BAC, so extract it out 
				state_high = (bac_state >> 8);
				state_low = bac_state;

				if (state_high & BAC_STILL)
				{
					g_ui16_backup_still_time = timestamp;
					state_move = 0;
					state_stop = 1;
					now_state = BAC_STILL;
					
					if( g_str_bit.b2_pedo_mode )
					{
						pedo_timer_stop();
					}
					g_str_bit.b2_pedo_mode = 0;
					g_str_timer.ui16_stay_hour = 0;
					
				}
				else if ( (state_low & BAC_STILL) )
				{
					if(state_stop)
					{
						g_str_pedo.ui16_still_time = timestamp - g_ui16_backup_still_time;
						g_str_pedo.ui16_total_still_time += g_str_pedo.ui16_still_time;

						g_ui16_backup_still_time = timestamp;
						state_stop = 0;
						state_move = 1;
					}
				}

				if (state_high & BAC_WALK)
				{
					g_ui32_backup_walk_step = g_ui32_pedometer_steps;
					g_ui16_backup_walk_time = timestamp;
					state_move = 1;
					now_state = BAC_WALK;
					
					if( !g_str_bit.b2_pedo_mode )
					{
						pedo_timer_start();
					}
					
					g_str_bit.b2_pedo_mode = 1;
				}
				else if( (state_low & BAC_WALK) )
				{
					if ((g_ui32_backup_walk_step < g_ui32_pedometer_steps))
					{
						g_str_pedo.ui32_walk_step = g_ui32_pedometer_steps - g_ui32_backup_walk_step;
						g_str_pedo.ui16_walk_time = timestamp - g_ui16_backup_walk_time;
						g_str_pedo.ui32_total_walk_step += g_str_pedo.ui32_walk_step;
						//g_str_pedo.ui16_total_walk_time += g_str_pedo.ui16_walk_time;

						g_ui32_backup_walk_step = g_ui32_pedometer_steps;
						g_ui16_backup_walk_time = timestamp;

						state_move = 0;
					}
					else
					{
						g_ui32_backup_walk_step = g_ui32_pedometer_steps;
					}
				}

				if (state_high & BAC_RUN)
				{
					g_ui32_backup_run_step = g_ui32_pedometer_steps;
					g_ui16_backup_run_time = timestamp;
					state_move = 1;
					now_state = BAC_RUN;
					
					if( !g_str_bit.b2_pedo_mode )
					{
						pedo_timer_start();
					}
					
					g_str_bit.b2_pedo_mode = 2;
				}
				else if(state_low & BAC_RUN)
				{
					if (g_ui32_backup_run_step < g_ui32_pedometer_steps)
					{
						g_str_pedo.ui32_run_step = g_ui32_pedometer_steps - g_ui32_backup_run_step;
						g_str_pedo.ui16_run_time = timestamp - g_ui16_backup_run_time;
						g_str_pedo.ui32_total_run_step += g_str_pedo.ui32_run_step;
						//g_str_pedo.ui16_total_run_time += g_str_pedo.ui16_run_time;

						g_ui32_backup_run_step = g_ui32_pedometer_steps;
						g_ui16_backup_run_time = timestamp;
						state_move = 0;
					}
					else
					{
						g_ui32_backup_run_step = g_ui32_pedometer_steps;
					}
				}
			}

			// Step detector available from DMP FIFO and step counter sensor is enabled
			if (header & PED_STEPDET_SET)
			{
				if(state_move)
				{
					dmp_get_pedometer_num_of_steps(&g_ui32_pedometer_steps);
					
					if( g_str_app_status.ui8_step_status )
					{
						if( g_str_bit.b1_daily_step_noti )
						{
							if( g_ui32_pedometer_steps > ( g_str_app_status.ui16_target_steps * 1000 ) )
							{
#ifdef ENABLE_PATTERN
									call_activity_noti();
#endif
							}
							g_str_bit.b1_daily_step_noti = 0;
						}
					}

				}
			}
			if(!data_left_in_fifo)
				g_str_bit.b1_motion_chk = 0;


		}while (data_left_in_fifo);
	}
}

#endif
