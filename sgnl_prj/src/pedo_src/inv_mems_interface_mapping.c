/*
* ________________________________________________________________________________________________________
* Copyright ?2014-2015 InvenSense Inc. Portions Copyright ?2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software? is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/

#include "inv_defines.h"

int inv_mems_firmware_load(const unsigned char *data_start, unsigned short size_start, unsigned short load_addr)
{
	int write_size;
	int result;
	unsigned short memaddr;
	unsigned char *data;
	unsigned short size;
	unsigned char data_cmp[INV_MAX_SERIAL_READ];
	int flag = 0;
        
	// Write DMP memory
	data = (unsigned char *)data_start;
	size = size_start;
	memaddr = load_addr;
	while (size > 0) {
		write_size = min(size, INV_MAX_SERIAL_WRITE);
		if ((memaddr & 0xff) + write_size > 0x100)			// Moved across a bank
			write_size = (memaddr & 0xff) + write_size - 0x100;

		result = inv_write_mems(memaddr, write_size, data);
		if (result)
			return result;
                
		data += write_size;
		size -= write_size;
		memaddr += write_size;
	}

	// Verify DMP memory
	data = (unsigned char *)data_start;
	size = size_start;
	memaddr = load_addr;
	while (size > 0) {
		write_size = min(size, INV_MAX_SERIAL_READ);
		if ((memaddr & 0xff) + write_size > 0x100)      		// Moved across a bank
			write_size = (memaddr & 0xff) + write_size - 0x100;

		result = inv_read_mems(memaddr, write_size, data_cmp);
		if (result)
			flag++; // Error, DMP not written correctly
		if (memcmp(data_cmp, data, write_size))
                        return -1;
		data += write_size;
		size -= write_size;
		memaddr += write_size;
	}

	return INV_SUCCESS;
}

int dmp_get_pedometer_num_of_steps(uint32_t *steps)
{
	uint32_t lsteps = 0;
	int result;

	result = dmp_get_pedometer_num_of_steps_20648(&lsteps);
	*steps = lsteps;
	return result;
}

int reset_steps(void)
{
	int result;
	unsigned char big8[4];

	big8[0] = 0;
	big8[1] = 0;
	big8[2] = 0;
	big8[3] = 0;

	result = inv_write_mems((54 * 16), 4, big8);//PEDSTD_STEPCTR (54 * 16)
	if (result) 
		return result;


	return INV_SUCCESS;
}

unsigned char *inv_dmpdriver_int16_to_big8(short x, unsigned char *big8)
{
    big8[0] = (unsigned char)((x >> 8) & 0xff);
    big8[1] = (unsigned char)(x & 0xff);
    return big8;
}

unsigned char *inv_dmpdriver_int32_to_big8(long x, unsigned char *big8)
{
    big8[0] = (unsigned char)((x >> 24) & 0xff);
    big8[1] = (unsigned char)((x >> 16) & 0xff);
    big8[2] = (unsigned char)((x >> 8) & 0xff);
    big8[3] = (unsigned char)(x & 0xff);
    return big8;
}
