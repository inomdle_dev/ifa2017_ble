#include "peripheral_define.h"

#ifdef ENABLE_PEDOMETER

#define PEDOMETER_DAILY_BACKUP_PERIOD	14 //2 weeks
#define PEDOMETER_DAILY_BACKUP_BUFFER	7 //2 weeks
#define PEDOMETER_HOUR_BACKUP_MAX		30//24 // 24 hours + padding 6

pedo_daily_backup_sturct_t g_str_pedo_daily_back[PEDOMETER_DAILY_BACKUP_PERIOD];

uint32_t g_ui32_flash_hour_write_cnt = 0;
uint32_t g_ui32_flash_daily_write_cnt = 0;
uint32_t g_ui32_flash_daily_backup_cnt = 0;

void pedo_flash_daily_read( void )
{
	static uint32_t pedo_daily_data[2];

	uint32_t ui32_steps = 0;
	uint8_t byte[20] = "";
	uint8_t nus_index = 0;

	byte[++nus_index] = REPORT_PEDOMETER;
	byte[++nus_index] = REP_PEDO_PAST_DATE;
	byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff0000 ) >> 16 );
	byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff00 ) >> 8 );
	byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_date & 0x00ff );
	
	for(int cnt = 0; cnt < PEDOMETER_DAILY_BACKUP_PERIOD; cnt++ )//14 days - 2 weeks
	{
		user_flash_read( USER_FLASH_PEDOMETER_DAILY, pedo_daily_data, cnt, ( uint32_t )sizeof( pedo_daily_data ));

		if( pedo_daily_data[0] == 0xffffffff )
		{
			break;
		}
		else if( g_str_time.ui32_app_date == pedo_daily_data[0] >> 8 )
		{
			ui32_steps = pedo_daily_data[1];
			break;
		}
	}

	byte[++nus_index] = (uint8_t)((ui32_steps&0xff000000)>>24);
	byte[++nus_index] = (uint8_t)((ui32_steps&0xff0000)>>16);
	byte[++nus_index] = (uint8_t)((ui32_steps&0xff00)>>8);
	byte[++nus_index] = (uint8_t)(ui32_steps&0xff);
	
	byte[0] = ++nus_index;
	nus_tx_push(byte, byte[0]);
}

void pedo_flash_hour_read( void )
{
	static uint32_t pedometer_data[4];

	uint8_t byte[20] = "";
	uint8_t nus_index = 0;
	uint16_t walktime = 0, runtime = 0;
	uint32_t walk = 0, run = 0;
	
	byte[++nus_index] = REPORT_PEDOMETER;
	byte[++nus_index] = REP_PEDO_PAST_TIME;
	byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff0000 ) >> 16 );
	byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff00 ) >> 8 );
	byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_date & 0xff );
	byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_hour );

	for( int cnt = 0; cnt < PEDOMETER_HOUR_BACKUP_MAX; cnt++ )//24 - 24 hours 
	{
		user_flash_read( USER_FLASH_PEDOMETER_HOUR, pedometer_data, cnt, (uint32_t)sizeof(pedometer_data));

		if( pedometer_data[0] == 0xffffffff )
		{
			break;
		}
		else if( ( pedometer_data[0] & 0x00ff ) == (uint8_t)('L') )
		{
			//live_skip
		}
		else if( ( g_str_time.ui32_app_date == pedometer_data[0] >> 8 ) && ( g_str_time.ui32_app_hour == ( pedometer_data[0] & 0xff ) ) )
		{
			walktime += pedometer_data[3] >> 16;
			runtime += (pedometer_data[3] & 0xffff);
			walk += pedometer_data[1];
			run += pedometer_data[2];

			//break;
		}
		else
		{
			break;
		}
	}
	
	byte[++nus_index] = ( uint8_t )((walk&0xff000000)>>24);
	byte[++nus_index] = ( uint8_t )((walk&0x00ff0000)>>16);
	byte[++nus_index] = ( uint8_t )((walk&0x0000ff00)>>8);
	byte[++nus_index] = ( uint8_t )(walk&0x000000ff);
	byte[++nus_index] = ( uint8_t )((walktime&0xff00)>>8);
	byte[++nus_index] = ( uint8_t )(walktime&0x00ff);
	byte[++nus_index] = ( uint8_t )((run&0xff000000)>>24);
	byte[++nus_index] = ( uint8_t )((run&0x00ff0000)>>16);
	byte[++nus_index] = ( uint8_t )((run&0x0000ff00)>>8);
	byte[++nus_index] = ( uint8_t )(run&0x000000ff);
	byte[++nus_index] = ( uint8_t )((runtime&0xff00)>>8);
	byte[++nus_index] = ( uint8_t )(runtime&0x00ff);

	byte[0] = ++nus_index;
	nus_tx_push(byte, byte[0]);

}

void pedo_flash_daily_write( void )
{
	static uint32_t pedometer_data[4];
	static uint32_t pedo_daily_data[2];
	
	uint32_t walk = 0;
	uint32_t run = 0;
	
	for(int i = 0; i < 24; i++)
	{
		user_flash_read( USER_FLASH_PEDOMETER_HOUR, pedometer_data, i, (uint32_t)sizeof(pedometer_data));
		walk = pedometer_data[1];
		run = pedometer_data[2];

		if( pedometer_data[0] == 0xFFFFFFFF )
		{
			break;
		}
		else if( ( pedometer_data[0] & 0x00ff ) == (uint8_t)('L') )
		{
			//live_skip
		}
		else
		{
			pedo_daily_data[1] += ( walk + run );
		}
	}
	pedo_daily_data[0] = ( ( g_str_time.ui32_date << 8 ) & 0xFFFFFF00 ) | ( g_ui32_flash_daily_write_cnt & 0x00FF );
	user_flash_write( USER_FLASH_PEDOMETER_DAILY, pedo_daily_data, g_ui32_flash_daily_write_cnt++, (uint32_t)sizeof(pedo_daily_data));
	
	if( g_ui32_flash_daily_write_cnt >= PEDOMETER_DAILY_BACKUP_PERIOD )
	{
		g_ui32_flash_daily_backup_cnt = g_ui32_flash_daily_write_cnt - PEDOMETER_DAILY_BACKUP_PERIOD;

		if( g_ui32_flash_daily_write_cnt >= ( PEDOMETER_DAILY_BACKUP_PERIOD + PEDOMETER_DAILY_BACKUP_BUFFER ) )
		{
			uint16_t i = 0;
			for(i = 0; i < PEDOMETER_DAILY_BACKUP_PERIOD; i++)
			{
				user_flash_read( USER_FLASH_PEDOMETER_DAILY, pedo_daily_data, g_ui32_flash_daily_backup_cnt + i, (uint32_t)sizeof(pedo_daily_data));
				
				pedo_daily_data[0] = ( pedo_daily_data[0] & 0xFFFFFF00 ) | ( i & 0x00FF );
				g_str_pedo_daily_back[i].ui32_date_cnt = pedo_daily_data[0];
				g_str_pedo_daily_back[i].ui32_total_steps = pedo_daily_data[1];
			}
			user_flash_erase( USER_FLASH_PEDOMETER_DAILY );

			user_flash_write( USER_FLASH_PEDOMETER_DAILY, (uint32_t*)g_str_pedo_daily_back, 0, (uint32_t)sizeof(g_str_pedo_daily_back));
			
			g_ui32_flash_daily_write_cnt = PEDOMETER_DAILY_BACKUP_PERIOD;
		}
	}
	
	user_flash_erase( USER_FLASH_PEDOMETER_HOUR );
	g_ui32_flash_hour_write_cnt = 0;
	memset((pedometer_struct_t *)&g_str_pedo, 0x00, sizeof(g_str_pedo));
	
	g_ui32_pedometer_steps = 0;
	g_ui32_pedometer_times = 0;
			
	g_ui32_backup_walk_step = 0;
	g_ui32_backup_run_step = 0;
	
	reset_steps();
}

void pedo_flash_hour_write( void )
{
	static uint32_t pedometer_data[4] = {0,};
	
	pedometer_data_update();

	if( g_str_time.ui32_date )
	{
		pedometer_data[0]  = ( uint32_t )( ( g_str_time.ui32_date & 0x00FFFFFF ) << 8 );
		pedometer_data[0] |= ( uint32_t )( g_str_time.ui32_hour & 0x00ff );
		pedometer_data[1] = ( uint32_t )( g_str_pedo.ui32_total_walk_step );
		pedometer_data[2] = ( uint32_t )( g_str_pedo.ui32_total_run_step );
		pedometer_data[3] = ( uint32_t )( ( g_str_pedo.ui16_total_walk_time << 16 ) | g_str_pedo.ui16_total_run_time );

		user_flash_write( USER_FLASH_PEDOMETER_HOUR, pedometer_data, g_ui32_flash_hour_write_cnt , (uint32_t)sizeof(pedometer_data));

		++g_ui32_flash_hour_write_cnt ;

		memset((pedometer_struct_t *)&g_str_pedo, 0x00, sizeof(g_str_pedo));
	}
}

void pedo_flash_live_backup( void )
{
	static uint32_t pedometer_data[4] = {0,};
	
	//pedometer_data_update();

	//if( g_str_time.ui32_date )
	{
		sprintf((char*)&pedometer_data[0], "LB");
		pedometer_data[0] |= ( uint32_t )( ( g_str_time.ui8_minute << 16 ) & 0x00ff0000 );
		pedometer_data[0] |= ( uint32_t )( ( g_str_time.ui8_second << 24 ) & 0xff000000 );

		pedometer_data[1] = ( uint32_t )( g_ui32_pedometer_steps );
		pedometer_data[2] = ( uint32_t )( g_ui32_pedometer_times );
		
		pedometer_data[3]  = ( uint32_t )( ( g_str_time.ui32_date & 0x00FFFFFF ) << 8 );
		pedometer_data[3] |= ( uint32_t )( g_str_time.ui32_hour & 0x00ff );

		user_flash_write( USER_FLASH_PEDOMETER_HOUR, pedometer_data, g_ui32_flash_hour_write_cnt , (uint32_t)sizeof(pedometer_data));

		++g_ui32_flash_hour_write_cnt ;

	}
}

void pedo_flash_time_sync( void )
{
	static uint32_t pedometer_data[4] = {0,};
	static uint32_t pedo_daily_data[2] = {0,};

	for( int cnt = 0; cnt < PEDOMETER_HOUR_BACKUP_MAX; cnt++ )
	{
		user_flash_read( USER_FLASH_PEDOMETER_HOUR, pedometer_data, cnt, (uint32_t)sizeof(pedometer_data));

		if(pedometer_data[0] == 0xffffffff)// empty data
		{
			if(cnt)// update flash cnt
			{
				g_ui32_flash_hour_write_cnt = cnt;
			}
			else// empty flash
			{
				g_ui32_flash_hour_write_cnt = 0;
			}
			break;
		}
		else if( ( pedometer_data[0] & 0x00ff ) == (uint8_t)('L') )
		{
			//live_skip

			g_ui32_pedometer_steps = pedometer_data[1];
			g_ui32_pedometer_times = pedometer_data[2];
			
			//if( ( g_str_time.ui8_minute == 0 ) &&( g_str_time.ui8_second == 0 ) )
			{
				if( pedometer_data[3] )
				{
					g_str_time.ui32_date = pedometer_data[3]>>8;
					g_str_time.ui32_hour = pedometer_data[3]&0x00FF;
					g_str_time.ui8_minute = ( pedometer_data[0] >> 16 ) & 0x00FF;
					g_str_time.ui8_second = ( pedometer_data[0] >> 24 ) & 0x00FF;
				}
			}

			if( !g_str_bit.b1_pedo_err )
			{
				unsigned char big8[4];

				big8[0] = (g_ui32_pedometer_steps & 0xff000000)>>24;
				big8[1] = (g_ui32_pedometer_steps & 0xff0000)>>16;
				big8[2] = (g_ui32_pedometer_steps & 0xff00)>>8;
				big8[3] = (g_ui32_pedometer_steps & 0xff);

				inv_write_mems((54 * 16), 4, big8);//PEDSTD_STEPCTR (54 * 16)
			}
		}
		else// read date
		{
			if( pedometer_data[0] )
			{
				uint16_t walk_time = 0, run_time = 0;
	
				uint32_t walk = 0;
				uint32_t run = 0;
	
				g_str_time.ui32_date = pedometer_data[0]>>8;
				g_str_time.ui32_hour = pedometer_data[0]&0x00FF;
				
				if( pedometer_data[1] != 0xffffffff )
				{
					walk = pedometer_data[1];
				}

				if( pedometer_data[2] != 0xffffffff )
				{
					run = pedometer_data[2];
				}
				
				if( ( pedometer_data[3] >> 16 ) != 0xffff )
				{
					walk_time = ( pedometer_data[3] >> 16 );
				}
				
				if( ( pedometer_data[3] & 0xffff ) != 0xffff )
				{
					run_time = pedometer_data[3] & 0xffff;
				}

				g_ui32_pedometer_times += ( walk_time + run_time );

				UNUSED_VARIABLE(walk);
				UNUSED_VARIABLE(run);
			}
		}
	}
	
	for( int cnt = 0; cnt < PEDOMETER_DAILY_BACKUP_PERIOD + PEDOMETER_DAILY_BACKUP_BUFFER; cnt++ )
	{
		user_flash_read( USER_FLASH_PEDOMETER_DAILY, pedo_daily_data, cnt, (uint32_t)sizeof(pedo_daily_data));

		if(pedo_daily_data[0] == 0xffffffff)// empty data
		{
			if(cnt)// update flash cnt
			{
				g_ui32_flash_daily_write_cnt = cnt;
				
				if( !g_ui32_flash_hour_write_cnt )
				{
					g_str_time.ui32_hour = 23;
				}
			}
			else// empty flash
			{
				g_ui32_flash_daily_write_cnt = 0;
			}
			break;
		}
		else
		{
			if( !g_ui32_flash_hour_write_cnt )
			{
				g_str_time.ui32_date = pedo_daily_data[0]>>8;
			}
		}
	}

	g_ui32_backup_walk_step = g_ui32_pedometer_steps;
	g_ui32_backup_run_step = g_ui32_pedometer_steps;
	
	if( !g_str_bit.b1_pedo_err )
	{
		unsigned char big8[4];

		big8[0] = (g_ui32_pedometer_steps & 0xff000000)>>24;
		big8[1] = (g_ui32_pedometer_steps & 0xff0000)>>16;
		big8[2] = (g_ui32_pedometer_steps & 0xff00)>>8;
		big8[3] = (g_ui32_pedometer_steps & 0xff);

		inv_write_mems((54 * 16), 4, big8);//PEDSTD_STEPCTR (54 * 16)
	}
	
	if( g_str_time.ui32_date )
	{
		if( g_str_time.ui8_minute || g_str_time.ui8_second )
		{
			//live time update
		}
		else
		{
			time_update();
		}
		day_of_week();
	}

}
#endif//ENABLE_PEDOMETER
