/*
* ________________________________________________________________________________________________________
* Copyright �� 2015-2015 InvenSense Inc. Portions Copyright �� 2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively ��Software��) is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/
/*
* ________________________________________________________________________________________________________
*
* Main source codes for ICM20648
* by Eddy Kim
*
* Rev. 1.0
* Uniquest Corp.
* ________________________________________________________________________________________________________
*/

///// INCLUDES //////////////////////////////////////////////////////
#include "inv_defines.h"

/***************************************defines******************************************************************/
/*  @brief      ODR minimum for gyro and accelero based sensors in ms */
#define ODR_MIN                 5
/*  @brief      ODR minimum for magneto based sensors in ms */
#define ODR_MIN_MAGNETO         15
/*  @brief      ODR minimum for pressure based sensors is 16Hz, in ms */
#define ODR_MIN_PRESSURE        62
/*  @brief      ODR maximum for all sensors in ms */
#define ODR_MAX                 1000

///// FUNCTIONS /////////////////////////////////////////////////////
/**
* @brief Initialization of MEMS
* @details [long description]
*/
void inv_mems_init(void)
{

        // Force accelero fullscale to 4g to be Android-compliant
	//inv_set_accel_fullscale(MPU_FS_4G);
}
