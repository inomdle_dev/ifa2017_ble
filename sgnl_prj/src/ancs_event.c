/**
* File name : ancs_evnt.c
*
* This file contains the source code for a sample application using the Apple 
* Notification Center Service Client.
*/

#include "peripheral_define.h"

/* 20Byte Max */
#define ATTR_DATA_SIZE                 BLE_ANCS_ATTR_DATA_MAX                      /**< Allocated size for attribute data. */

/**@brief String literals for the iOS notification categories. used then printing to UART. */
static const char * lit_catid[BLE_ANCS_NB_OF_CATEGORY_ID] =
{
	"Other",
	"Incoming Call",
	"Missed Call",
	"Voice Mail",
	"Social",
	"Schedule",
	"Email",
	"News",
	"Health And Fitness",
	"Business And Finance",
	"Location",
	"Entertainment"
};

/**@brief String literals for the iOS notification event types. Used then printing to UART. */
static const char * lit_eventid[BLE_ANCS_NB_OF_EVT_ID] =
{
	"Added",
	"Modified",
	"Removed"
};

/**@brief String literals for the iOS notification attribute types. Used when printing to UART. */
static const char * lit_attrid[BLE_ANCS_NB_OF_NOTIF_ATTR] =
{
	"App Identifier",
	"Title",
	"Subtitle",
	"Message",
	"Message Size",
	"Date",
	"Positive Action Label",
	"Negative Action Label"
};


/**@brief String literals for the iOS notification attribute types. Used When printing to UART. */
static const char * lit_appid[BLE_ANCS_NB_OF_APP_ATTR] =
{
	"Display Name"
};

ble_ancs_c_t       m_ancs_c;                                     /**< Structure used to identify the Apple Notification Service Client. */

static ble_ancs_c_evt_notif_t m_notification_latest;                    /**< Local copy to keep track of the newest arriving notifications. */
static ble_ancs_c_attr_t      m_notif_attr_latest;                      /**< Local copy of the newest notification attribute. */

static uint8_t m_attr_appid[ATTR_DATA_SIZE];                            /**< Buffer to store attribute data. */
static uint8_t m_attr_title[ATTR_DATA_SIZE];                            /**< Buffer to store attribute data. */
static uint8_t m_attr_subtitle[ATTR_DATA_SIZE];                         /**< Buffer to store attribute data. */
static uint8_t m_attr_message[ATTR_DATA_SIZE];                          /**< Buffer to store attribute data. */
static uint8_t m_attr_message_size[ATTR_DATA_SIZE];                     /**< Buffer to store attribute data. */
static uint8_t m_attr_date[ATTR_DATA_SIZE];                             /**< Buffer to store attribute data. */
static uint8_t m_attr_posaction[ATTR_DATA_SIZE];                        /**< Buffer to store attribute data. */
static uint8_t m_attr_negaction[ATTR_DATA_SIZE];                        /**< Buffer to store attribute data. */

static uint8_t m_attr_disp_name[ATTR_DATA_SIZE];                        /**< Buffer to store attribute data. */

/**@brief Function for setting up GATTC notifications from the Notification Provider.
*
* @details This function is called when a successful connection has been established.
*/
static void apple_notification_setup(void)
{
	ret_code_t ret;
	
	if( !g_str_app_status.ui8_host_device )
	{
		g_str_app_status.ui8_host_device = HOST_DEVICE_IPHONE;
		app_status_write();
	}
	
	nrf_delay_ms(100); // Delay because we cannot add a CCCD to close to starting encryption. iOS specific.
	
	ret = ble_ancs_c_notif_source_notif_enable(&m_ancs_c);
	APP_ERROR_CHECK(ret);
	
	ret = ble_ancs_c_data_source_notif_enable(&m_ancs_c);
	APP_ERROR_CHECK(ret);
	
	NRF_LOG_DEBUG("Notifications Enabled.\r\n");
}


/**@brief Function for printing an iOS notification.
*
* @param[in] p_notif  Pointer to the iOS notification.
*/
static void notif_print(ble_ancs_c_evt_notif_t * p_notif)
{
	NRF_LOG_INFO("\r\nNotification\r\n");
	NRF_LOG_INFO("Event:       %s\r\n", (uint32_t)lit_eventid[p_notif->evt_id]);
	NRF_LOG_INFO("Category ID: %s\r\n", (uint32_t)lit_catid[p_notif->category_id]);
	NRF_LOG_INFO("Category Cnt:%u\r\n", (unsigned int) p_notif->category_count);
	NRF_LOG_INFO("UID:         %u\r\n", (unsigned int) p_notif->notif_uid);
	
	NRF_LOG_INFO("Flags:\r\n");
	
#ifdef ENABLE_ANCS_PARSER
	g_str_ancs.ui8_event = p_notif->evt_id;
	g_str_ancs.ui8_category_id = p_notif->category_id;
	//g_str_ancs.ui8_category_cnt = p_notif->category_count;
	
	if( g_str_ancs.ui8_event == 2 ) //removed
	{
		if( g_str_ancs.ui8_category_cnt )
		{
			g_str_ancs.ui8_category_cnt--;
		}
		if( g_str_ancs.ui8_category_id == BLE_ANCS_CATEGORY_ID_INCOMING_CALL )
		{
			g_str_call_bit.b4_call_status &= ~(CALL_STATUS_INCOMING);
			g_str_menu.b3_income = 0;
#ifdef ENABLE_PATTERN
			force_off_pattern();
#endif
		}
	}
	else if( g_str_ancs.ui8_event == 0 ) //added
	{
		g_str_ancs.ui8_category_cnt++;
	}
	
#endif //ENABLE_ANCS_PARSER	
	
	if(p_notif->evt_flags.silent == 1)
	{
		NRF_LOG_INFO(" Silent\r\n");
	}
	if(p_notif->evt_flags.important == 1)
	{
		NRF_LOG_INFO(" Important\r\n");
	}
	if(p_notif->evt_flags.pre_existing == 1)
	{
		NRF_LOG_INFO(" Pre-existing\r\n");
	}
	if(p_notif->evt_flags.positive_action == 1)
	{
		NRF_LOG_INFO(" Positive Action\r\n");
	}
	if(p_notif->evt_flags.negative_action == 1)
	{
		NRF_LOG_INFO(" Negative Action\r\n");
	}
}


/**@brief Function for printing iOS notification attribute data.
*
* @param[in] p_attr Pointer to an iOS notification attribute.
*/
static void notif_attr_print(ble_ancs_c_attr_t * p_attr)
{
	if (p_attr->attr_len != 0)
	{
		NRF_LOG_INFO("%s: %s\r\n", (uint32_t)lit_attrid[p_attr->attr_id], nrf_log_push((char *)p_attr->p_attr_data));
		
		switch( p_attr->attr_id )
		{
		case BLE_ANCS_NOTIF_ATTR_ID_APP_IDENTIFIER:
			{
				uint8_t prev_cnt = g_str_ancs.ui8_category_cnt;
				uint8_t prev_id = g_str_ancs.ui8_category_id;
				
				memset(&g_str_ancs, 0, sizeof(ancs_parse_struct_t));
				g_str_ancs.ui8_category_id = prev_id;
				g_str_ancs.ui8_category_cnt = prev_cnt;
				
				strncpy((char*)g_str_ancs.ui8_identifier, (char*)p_attr->p_attr_data, p_attr->attr_len);
				g_str_ancs.ui8_id_len = p_attr->attr_len;
			}
			break;
			
		case BLE_ANCS_NOTIF_ATTR_ID_TITLE:
			{
				if( g_str_ancs.ui8_category_id == BLE_ANCS_CATEGORY_ID_MISSED_CALL )
				{
					g_str_ancs.ui8_title_len = 11;
					strncpy((char*)g_str_ancs.ui8_title, (char*)"missed call", g_str_ancs.ui8_title_len);
				}
				else
				{
					char *ptr, *last;
					ptr = strtok((char *)g_str_ancs.ui8_identifier,".");
					
					while(ptr!= NULL)
					{
						last = ptr;
						ptr = strtok(NULL,".");
					}
					
					
					if( !strncmp(last, "MobileSMS", 9) )
					{
						g_str_ancs.ui8_title_len = 7;
						strncpy((char*)g_str_ancs.ui8_title, (char*)"Message", g_str_ancs.ui8_title_len);
						
						strncpy((char*)g_str_ancs.ui8_subtitle, (char*)p_attr->p_attr_data, p_attr->attr_len);
						g_str_ancs.ui8_subtitle_len = p_attr->attr_len;
					}
					else
					{
						for( int i = 0; i < p_attr->attr_len; i++ )
						{
							if(p_attr->p_attr_data[i] >= 0x80)
							{
								g_str_ancs.ui8_title_len = strlen(last);
								strncpy((char*)g_str_ancs.ui8_title, last, g_str_ancs.ui8_title_len);
								return;
							}
						}
						
						strncpy((char*)g_str_ancs.ui8_title, (char*)p_attr->p_attr_data, p_attr->attr_len);
						g_str_ancs.ui8_title_len = p_attr->attr_len;
					}
				}
			}
			break;
		case BLE_ANCS_NOTIF_ATTR_ID_SUBTITLE :
			{
				if( strncmp((char *)g_str_ancs.ui8_title, "Message", 7) )
				{
					for( int i = 0; i < p_attr->attr_len; i++ )
					{
						if(p_attr->p_attr_data[i] >= 0x80)
						{
							//g_str_ancs.ui8_subtitle_len = 19;
							//strncpy((char*)g_str_ancs.ui8_subtitle,"not       supported", g_str_ancs.ui8_subtitle_len);
							
							//return;
							p_attr->p_attr_data[i] = '\a';
							p_attr->p_attr_data[++i] = '\a';
						}
					}
					
					strncpy((char*)g_str_ancs.ui8_subtitle, (char*)p_attr->p_attr_data, p_attr->attr_len);
					g_str_ancs.ui8_subtitle_len = p_attr->attr_len;
				}
			}
			
			break;
		case BLE_ANCS_NOTIF_ATTR_ID_MESSAGE :
			for( int i = 0; i < p_attr->attr_len; i++ )
			{
				if(p_attr->p_attr_data[i] >= 0x80)
				{
					//g_str_ancs.ui8_subtitle_len = 19;
					//strncpy((char*)g_str_ancs.ui8_subtitle,"not       supported", g_str_ancs.ui8_subtitle_len);
					
					//return;
					p_attr->p_attr_data[i] = '\a';
					p_attr->p_attr_data[++i] = '\a';
				}
			}
			
			strncpy((char*)g_str_ancs.ui8_message, (char*)p_attr->p_attr_data, p_attr->attr_len);
			g_str_ancs.ui8_message_len = p_attr->attr_len;
			
			break;
			
		case BLE_ANCS_NOTIF_ATTR_ID_DATE:
			strncpy((char*)g_str_ancs.ui8_date, (char*)p_attr->p_attr_data, p_attr->attr_len);
			g_str_ancs.ui8_date_len = p_attr->attr_len;
			g_str_bit.b1_ancs_received = 1;
			break;
			
		default : break;
		
		}
	}
	else if (p_attr->attr_len == 0)
	{
		NRF_LOG_INFO("%s: (N/A)\r\n", (uint32_t)lit_attrid[p_attr->attr_id]);
	}
}

/**@brief Function for printing iOS notification attribute data.
*
* @param[in] p_attr Pointer to an iOS App attribute.
*/
static void app_attr_print(ble_ancs_c_attr_t * p_attr)
{
	if (p_attr->attr_len != 0)
	{
		NRF_LOG_INFO("%s: %s\r\n", (uint32_t)lit_appid[p_attr->attr_id], (uint32_t)p_attr->p_attr_data);
	}
	else if (p_attr->attr_len == 0)
	{
		NRF_LOG_INFO("%s: (N/A)\r\n", (uint32_t) lit_appid[p_attr->attr_id]);
	}
}


/**@brief Function for printing out errors that originated from the Notification Provider (iOS).
*
* @param[in] err_code_np Error code received from NP.
*/
static void err_code_print(uint16_t err_code_np)
{
	switch (err_code_np)
	{
	case BLE_ANCS_NP_UNKNOWN_COMMAND:
		NRF_LOG_INFO("Error: Command ID was not recognized by the Notification Provider. \r\n");
		break;
		
	case BLE_ANCS_NP_INVALID_COMMAND:
		NRF_LOG_INFO("Error: Command failed to be parsed on the Notification Provider. \r\n");
		break;
		
	case BLE_ANCS_NP_INVALID_PARAMETER:
		NRF_LOG_INFO("Error: Parameter does not refer to an existing object on the Notification Provider. \r\n");
		break;
		
	case BLE_ANCS_NP_ACTION_FAILED:
		NRF_LOG_INFO("Error: Perform Notification Action Failed on the Notification Provider. \r\n");
		break;
		
	default:
		break;
	}
}

/**@brief Function for handling the Apple Notification Service client errors.
*
* @param[in] nrf_error  Error code containing information about what went wrong.
*/
static void apple_notification_error_handler(uint32_t nrf_error)
{
	APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for handling the Apple Notification Service client.
*
* @details This function is called for all events in the Apple Notification client that
*          are passed to the application.
*
* @param[in] p_evt  Event received from the Apple Notification Service client.
*/
void on_ancs_c_evt(ble_ancs_c_evt_t * p_evt)
{
	ret_code_t ret = NRF_SUCCESS;
	
	switch (p_evt->evt_type)
	{
	case BLE_ANCS_C_EVT_DISCOVERY_COMPLETE:
		NRF_LOG_DEBUG("Apple Notification Center Service discovered on the server.\r\n");
		ret = nrf_ble_ancs_c_handles_assign(&m_ancs_c, p_evt->conn_handle, &p_evt->service);
		APP_ERROR_CHECK(ret);
		apple_notification_setup();
		break;
		
	case BLE_ANCS_C_EVT_NOTIF:
		m_notification_latest = p_evt->notif;
		
		notif_print(&m_notification_latest);
		ret = nrf_ble_ancs_c_request_attrs(&m_ancs_c, &m_notification_latest);
		APP_ERROR_CHECK(ret);
		break;
		
	case BLE_ANCS_C_EVT_NOTIF_ATTRIBUTE:
		m_notif_attr_latest = p_evt->attr;
		notif_attr_print(&m_notif_attr_latest);
		
		break;
	case BLE_ANCS_C_EVT_DISCOVERY_FAILED:
		NRF_LOG_DEBUG("Apple Notification Center Service not discovered on the server.\r\n");
		break;
		
	case BLE_ANCS_C_EVT_APP_ATTRIBUTE:
		app_attr_print(&p_evt->attr);
		break;
	case BLE_ANCS_C_EVT_NP_ERROR:
		err_code_print(p_evt->err_code_np);
		break;
	default:
		// No implementation needed.
		break;
	}
}

void ancs_service_init( void )
{
	ble_ancs_c_init_t ancs_init_obj;
	ret_code_t        ret;
	
	memset(&ancs_init_obj, 0, sizeof(ancs_init_obj));
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_APP_IDENTIFIER,
								  m_attr_appid,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_app_attr_add(&m_ancs_c,
									  BLE_ANCS_APP_ATTR_ID_DISPLAY_NAME,
									  m_attr_disp_name,
									  sizeof(m_attr_disp_name));
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_TITLE,
								  m_attr_title,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_SUBTITLE,
								  m_attr_subtitle,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_MESSAGE,
								  m_attr_message,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_MESSAGE_SIZE,
								  m_attr_message_size,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_DATE,
								  m_attr_date,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_POSITIVE_ACTION_LABEL,
								  m_attr_posaction,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
								  BLE_ANCS_NOTIF_ATTR_ID_NEGATIVE_ACTION_LABEL,
								  m_attr_negaction,
								  ATTR_DATA_SIZE);
	APP_ERROR_CHECK(ret);
	
	ancs_init_obj.evt_handler   = on_ancs_c_evt;
	ancs_init_obj.error_handler = apple_notification_error_handler;
	
	ret = ble_ancs_c_init(&m_ancs_c, &ancs_init_obj);
	APP_ERROR_CHECK(ret);
}

/* ancs negative anction func : using reject call */
void ancs_negative_action( void )
{
	ret_code_t        ret;
	if(m_notification_latest.evt_flags.negative_action == true)
	{
		NRF_LOG_INFO("Performing Negative Action.\r\n");
		ret = nrf_ancs_perform_notif_action(&m_ancs_c,
											m_notification_latest.notif_uid,
											ACTION_ID_NEGATIVE);
		APP_ERROR_CHECK(ret);
	}
}


/* ancs positive anction func */
void ancs_positive_action( void )
{
	ret_code_t        ret;
	if(m_notification_latest.evt_flags.positive_action == true)
	{
		NRF_LOG_INFO("Performing Positive Action.\r\n");
		ret = nrf_ancs_perform_notif_action(&m_ancs_c,
											m_notification_latest.notif_uid,
											ACTION_ID_POSITIVE);
		APP_ERROR_CHECK(ret);
	}
}

#ifdef ENABLE_ANCS_PARSER

void parsing_ancs( void )
{
	if( g_str_ancs.ui8_event == 0 ) //add
	{
		switch( g_str_ancs.ui8_category_id )
		{
		case BLE_ANCS_CATEGORY_ID_INCOMING_CALL :
			{
#ifdef ENABLE_PATTERN
				call_incoming_call();
#endif
			}
			
			g_str_call_bit.b4_call_status |= CALL_STATUS_INCOMING;
			break;
		case BLE_ANCS_CATEGORY_ID_MISSED_CALL :
			break;
			
		case BLE_ANCS_CATEGORY_ID_VOICE_MAIL :
			break;
			
		default :
			user_str_lower((uint8_t *)g_str_ancs.ui8_identifier, g_str_ancs.ui8_id_len);
			package_parser((uint8_t *)g_str_ancs.ui8_identifier, g_str_ancs.ui8_id_len);
			
			break;
		}
	}
	
}

#endif //ENABLE_ANCS_PARSER
