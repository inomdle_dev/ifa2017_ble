/**
 * File name : package_parser.c
 *
 * This file contains the source code for parsing ancs package name
 */

#include "peripheral_define.h"

#define PACKAGE_BACKUP_PERIOD	100

uint16_t find_package_name(uint8_t *package_name, uint8_t u8_data_len)
{
	static uint32_t flash_package_name32[8];
	int cnt = 0;

	while(cnt < PACKAGE_BACKUP_PERIOD)// flash read
	{
		user_flash_read( USER_FLASH_PACKAGE_BLOCK, flash_package_name32, cnt, (uint32_t)sizeof(flash_package_name32));

		if(flash_package_name32[0] == 0xffffffff)// no data
		{
			break;
		}
		else
		{
			uint8_t flash_package_name8[20] = "";
			sprintf((char *)flash_package_name8, "%s",(char *)(flash_package_name32));
									
			if( !strncmp( (char *)package_name, (char *)flash_package_name8, u8_data_len ) )// found
			{
				return  0;
			}
		}
		
		++cnt;
	}
	
	return ++cnt;
}

void write_package_name( uint8_t *package_name, uint8_t u8_data_len )
{
	static uint32_t flash_write_package_name32[8] = {0,};
	uint8_t cnt = find_package_name(package_name, u8_data_len) - 1;
	
	memset(&flash_write_package_name32, 0, sizeof(flash_write_package_name32));
	strncpy((char *)flash_write_package_name32, (char *)package_name, u8_data_len );
	user_flash_write(USER_FLASH_PACKAGE_BLOCK, (uint32_t *)flash_write_package_name32, cnt, (uint32_t)sizeof(flash_write_package_name32));
}

void erase_package_name( uint8_t *package_name, uint8_t u8_data_len )
{
	static uint32_t flash_package_backup[PACKAGE_BACKUP_PERIOD][8];
	static uint32_t flash_package_name32[8];
	int cnt = 0;
	int backup_cnt = 0;

	while(cnt < PACKAGE_BACKUP_PERIOD)// flash read
	{
		user_flash_read( USER_FLASH_PACKAGE_BLOCK, flash_package_name32, cnt, (uint32_t)sizeof(flash_package_name32));

		if(flash_package_name32[0] == 0xffffffff)// no data
		{
			break;
		}
		else
		{
			uint8_t flash_package_name8[20] = "";
			sprintf((char *)flash_package_name8, "%s",(char *)(flash_package_name32));
									
			if( strncmp( (char *)package_name, (char *)flash_package_name8, u8_data_len ) )// found
			{
				strncpy((char *)(flash_package_backup[backup_cnt++]), (char *)(flash_package_name32), 32);
			}
		}
		cnt++;
	}
	
	user_flash_erase( USER_FLASH_PACKAGE_BLOCK );		

	if( backup_cnt )
	{
		user_flash_write(USER_FLASH_PACKAGE_BLOCK, (uint32_t *)&flash_package_backup[0], 0, (uint32_t)sizeof(flash_package_backup[0]) * backup_cnt);
	}
}

void package_parser( uint8_t *package_name, uint8_t u8_data_len )
{
	uint16_t ret= 1;
	static uint8_t ui8_pkg_chk[20] = "";
	uint8_t byte[20] = "";
	
	memcpy(ui8_pkg_chk, package_name, u8_data_len);

	ret = find_package_name(package_name, u8_data_len);
	
	if( ret )// new data
	{
#ifdef ENABLE_PATTERN
		if( g_str_app_status.ui8_app_noti_status )
		{
			if( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING )
			{
				if( !g_str_app_status.ui8_dnd_status )
				{
					call_sub_app_noti();
				}
			}
			else
			{
				call_app_noti();
			}
		}
#endif

		if( u8_data_len <= 16 )
		{
			byte[0] = u8_data_len + 4;
			byte[1] = REPORT_ANCS_DATA;
			byte[2] = REP_ANCS_DATA_SEND;
			byte[3] = REP_ANCS_SEND_FIRST_ID;
			
			strncpy((char *)&byte[4], (char *)g_str_ancs.ui8_identifier, u8_data_len);
			nus_tx_push(byte, byte[0]);
			
		}
		else
		{
			byte[0] = 16 + 4;
			byte[1] = REPORT_ANCS_DATA;
			byte[2] = REP_ANCS_DATA_SEND;
			byte[3] = REP_ANCS_SEND_FIRST_ID;
			
			strncpy((char *)&byte[4], (char *)g_str_ancs.ui8_identifier, 16);
			nus_tx_push(byte, byte[0]);
			
			byte[0] = u8_data_len - 16 + 4;
			byte[1] = REPORT_ANCS_DATA;
			byte[2] = REP_ANCS_DATA_SEND;
			byte[3] = REP_ANCS_SEND_ADD_ID;
			
			strncpy((char *)&byte[4], (char *)&g_str_ancs.ui8_identifier[16], u8_data_len - 16);
			nus_tx_push(byte, byte[0]);

		}

		u8_data_len = g_str_ancs.ui8_title_len ;
		if( u8_data_len <= 16 )
		{
			byte[0] = u8_data_len + 4;
			byte[1] = REPORT_ANCS_DATA;
			byte[2] = REP_ANCS_DATA_SEND;
			byte[3] = REP_ANCS_SEND_FIRST_TITLE;
			
			strncpy((char *)&byte[4], (char *)g_str_ancs.ui8_title, u8_data_len);
			nus_tx_push(byte, byte[0]);
			
		}
		else
		{
			byte[0] = 16 + 4;
			byte[1] = REPORT_ANCS_DATA;
			byte[2] = REP_ANCS_DATA_SEND;
			byte[3] = REP_ANCS_SEND_FIRST_TITLE;
			
			strncpy((char *)&byte[4], (char *)g_str_ancs.ui8_title, 16);
			nus_tx_push(byte, byte[0]);
			
			byte[0] = u8_data_len - 16 + 4;
			byte[1] = REPORT_ANCS_DATA;
			byte[2] = REP_ANCS_DATA_SEND;
			byte[3] = REP_ANCS_SEND_ADD_TITLE;
			
			strncpy((char *)&byte[4], (char *)&g_str_ancs.ui8_title[16], u8_data_len - 16);
			nus_tx_push(byte, byte[0]);

		}

		byte[0] = 4;
		byte[1] = REPORT_ANCS_DATA;
		byte[2] = REP_ANCS_DATA_SEND;
		byte[3] = REP_ANCS_SEND_COMPLETE;
		
		nus_tx_push(byte, byte[0]);
	}
}

void ancs_data_parcer( uint8_t *u8_data, uint8_t u8_data_len )
{
	uint8_t u8_data_type = u8_data[0];
	static uint8_t u8_package_name[32] = "";
	static uint8_t u8_length = 0;

	switch( u8_data_type )
	{
	case REP_ANCS_SEND_FIRST_ID:
		strncpy( (char *)u8_package_name, (char *)(&u8_data[1]), u8_data_len);
		u8_length = u8_data_len;
		break;
	
	case REP_ANCS_SEND_ADD_ID:
		strncpy( (char *)&u8_package_name[u8_length], (char *)(&u8_data[1]), u8_data_len);
		u8_length += u8_data_len;
		break;

	case REP_ANCS_SEND_COMPLETE :
		if( u8_length )
		{
			user_str_lower( u8_package_name, u8_length );

			switch( u8_data[1] )
			{
			case REP_ANCS_FLAG_WRITE:
				write_package_name(u8_package_name, u8_length);
				break;

			case REP_ANCS_FLAG_ERASE:
				erase_package_name(u8_package_name, u8_length);
				break;
				
			default :
				break;
			}

			g_str_bit.b1_package_ready = 0;		
			u8_length = 0;
		}
		break;

	default :
		break;
	}

}
