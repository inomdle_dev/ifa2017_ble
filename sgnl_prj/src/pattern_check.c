/**
 * File name : pattern_check.c
 *
 * This file contains the source code for managing LED/haptic pattern call
 */

#include "peripheral_define.h"

#ifndef IAR_COMPILER
	#pragma anon_unions
#endif

union{
	uint32_t value;
	struct {
		uint32_t power_off:1;
		uint32_t power_on:1;

		uint32_t device_reset:1;

		uint32_t batt_gauge:1;
		uint32_t batt_low:1;
		uint32_t batt_charging:1;
		uint32_t batt_full:1;

		uint32_t unpaired:1;
		uint32_t unpair:1;
		uint32_t wait_pairing:1;
		uint32_t pairing_complete:1;
		uint32_t disconn_timeout:1;

		uint32_t dnd_on:1;
		uint32_t dnd_off:1;

		uint32_t favorite_select:1;
		uint32_t favorite_call:1;
		uint32_t incoming_favorite:1;
		
		uint32_t incoming_call:1;
		
		uint32_t activity_noti:1;
		uint32_t long_sit_noti:1;
		
		uint32_t app_noti:1;

	};
} pattern_check;

union{
	uint32_t value;
	struct {

		uint32_t activity_noti:1;
		uint32_t long_sit_noti:1;
		
		uint32_t app_noti:1;
		
	};
} sub_pattern_check;

#ifdef ENABLE_PATTERN
pattern_func p_pattern_func = NULL;
pattern_func p_sub_pattern_func = NULL;

void pattern_command_check(void)
{
	if( pattern_check.value )
	{
		pattern_timer_start();
		if( pattern_check.power_off )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_POWER_OFF_CNT;
				p_pattern_func = pattern_power_off;
				pattern_check.power_off = 0;
			}
		}
		else if( pattern_check.power_on )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_POWER_ON_CNT;
				p_pattern_func = pattern_power_on;
				pattern_check.power_on = 0;
			}
		}

		else if( pattern_check.device_reset )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DEVICE_RESET_CNT;
				p_pattern_func = pattern_device_reset;
				pattern_check.device_reset = 0;
			}
		}
#ifdef ENABLE_BATT
		else if( pattern_check.batt_gauge )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_GAUGE_CNT;
				p_pattern_func = pattern_batt_gauge;
				pattern_check.batt_gauge = 0;
			}
		}
		else if( pattern_check.batt_low )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_LOW_CNT;
				p_pattern_func = pattern_batt_low;
				pattern_check.batt_low = 0;
			}
		}
		else if( pattern_check.batt_charging )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
				p_pattern_func = pattern_batt_charging;
				pattern_check.batt_charging = 0;
			}
		}
		else if( pattern_check.batt_full )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_FULL_CNT;
				p_pattern_func = pattern_batt_full;
				pattern_check.batt_full = 0;
			}
		}
#endif
		else if( pattern_check.unpaired )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_UNPAIRED_CNT;
				p_pattern_func = pattern_unpaired;
				pattern_check.unpaired = 0;
			}
		}
		else if( pattern_check.unpair )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_UNPAIR_CNT;
				p_pattern_func = pattern_unpair;
				pattern_check.unpair = 0;
			}
		}
		else if( pattern_check.wait_pairing )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_WAIT_PAIRING_CNT;
				p_pattern_func = pattern_wait_pairing;
				pattern_check.wait_pairing = 0;
			}
		}
		else if( pattern_check.pairing_complete )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_PAIRING_COMPLETE_CNT;
				p_pattern_func = pattern_pairing_complete;
				pattern_check.pairing_complete = 0;
			}
		}
		else if( pattern_check.disconn_timeout )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DISCONN_TIMEOUT_CNT;
				p_pattern_func = pattern_disconn_timeout;
				pattern_check.disconn_timeout = 0;
			}
		}
#ifdef ENABLE_DND
		else if( pattern_check.dnd_on )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DND_ON_CNT;
				p_pattern_func = pattern_dnd_on;
				pattern_check.dnd_on = 0;
			}
		}
		else if( pattern_check.dnd_off )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DND_OFF_CNT;
				p_pattern_func = pattern_dnd_off;
				pattern_check.dnd_off = 0;
			}
		}
#endif //ENABLE_DND

		else if( pattern_check.incoming_call )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
				p_pattern_func = pattern_incoming_call;
				pattern_check.incoming_call = 0;
			}
		}
		else if( pattern_check.activity_noti )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_ACTIVITY_NOTI_CNT;
				p_pattern_func = pattern_activity_noti;
				pattern_check.activity_noti = 0;
			}
		}
		else if( pattern_check.long_sit_noti )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_LONG_SIT_NOTI_CNT;
				p_pattern_func = pattern_long_sit_noti;
				pattern_check.long_sit_noti = 0;
			}
		}
		else if( pattern_check.app_noti )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_APP_NOTI_CNT;
				p_pattern_func = pattern_app_noti;
				pattern_check.app_noti = 0;
			}
		}		
	}
	
}

void sub_pattern_command_check(void)
{	
	if( sub_pattern_check.value )
	{
		if( sub_pattern_check.activity_noti )
		{
			if( g_str_timer.ui16_sub_pattern_cnt == 0 )
			{
				g_str_timer.ui16_sub_pattern_cnt = PATTERN_ACTIVITY_NOTI_CNT;
				p_pattern_func = pattern_activity_noti;
				sub_pattern_check.activity_noti = 0;
			}
		}
		else if( sub_pattern_check.long_sit_noti )
		{
			if( g_str_timer.ui16_sub_pattern_cnt == 0 )
			{
				g_str_timer.ui16_sub_pattern_cnt = PATTERN_LONG_SIT_NOTI_CNT;
				p_pattern_func = pattern_long_sit_noti;
				sub_pattern_check.long_sit_noti = 0;
			}
		}
		else if( sub_pattern_check.app_noti )
		{
			if( g_str_timer.ui16_sub_pattern_cnt == 0 )
			{
				g_str_timer.ui16_sub_pattern_cnt = PATTERN_APP_NOTI_CNT;
				p_pattern_func = pattern_app_noti;
				sub_pattern_check.app_noti = 0;
			}
		}		
	}
}

void call_power_off( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_POWER_OFF_CNT;
		p_pattern_func = pattern_power_off;
		pattern_check.power_off = 0;
	}
	else
	{
		pattern_check.power_off = 1;
	}
}

void call_power_on( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_POWER_ON_CNT;
		p_pattern_func = pattern_power_on;
		pattern_check.power_on = 0;
	}
	else
	{
		pattern_check.power_on = 1;
	}
}

void call_device_reset( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DEVICE_RESET_CNT;
		p_pattern_func = pattern_device_reset;
		pattern_check.device_reset = 0;
	}
	else
	{
		pattern_check.device_reset = 1;
	}
}

#ifdef ENABLE_BATT
void call_batt_gauge( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_GAUGE_CNT;
		p_pattern_func = pattern_batt_gauge;
		pattern_check.batt_gauge = 0;
	}
	else
	{
		pattern_check.batt_gauge = 1;
	}
}

void call_batt_low( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_LOW_CNT;
		p_pattern_func = pattern_batt_low ;
		pattern_check.batt_low = 0;
	}
	else
	{
		pattern_check.batt_low = 1;
	}
}

void call_batt_charging( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
		p_pattern_func = pattern_batt_charging;
		pattern_check.batt_charging = 0;
	}
	else
	{
		pattern_check.batt_charging = 1;
	}
}

void call_batt_full( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_FULL_CNT;
		p_pattern_func = pattern_batt_full;
		pattern_check.batt_full = 0;
	}
	else
	{
		pattern_check.batt_full = 1;
	}
}
#endif //ENABLE_BATT
void call_unpaired( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_UNPAIRED_CNT;
		p_pattern_func = pattern_unpaired;
		pattern_check.unpaired = 0;
	}
	else
	{
		pattern_check.unpaired = 1;
	}
}

void call_unpair( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_UNPAIR_CNT;
		p_pattern_func = pattern_unpair;
		pattern_check.unpair = 0;
	}
	else
	{
		pattern_check.unpair = 1;
	}
}

void call_wait_pairing( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_WAIT_PAIRING_CNT;
		p_pattern_func = pattern_wait_pairing;
		pattern_check.wait_pairing = 0;
	}
	else
	{
		pattern_check.wait_pairing = 1;
	}
}

void call_pairing_complete( void )
{
	pattern_timer_start();
	force_off_pattern();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_PAIRING_COMPLETE_CNT;
		p_pattern_func = pattern_pairing_complete;
		pattern_check.pairing_complete = 0;
	}
	else
	{
		pattern_check.pairing_complete = 1;
	}
}

void call_disconn_timeout( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DISCONN_TIMEOUT_CNT;
		p_pattern_func = pattern_disconn_timeout;
		pattern_check.disconn_timeout = 0;
	}
	else
	{
		pattern_check.disconn_timeout = 1;
	}
}

#ifdef ENABLE_DND
void call_dnd_on( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DND_ON_CNT;
		p_pattern_func = pattern_dnd_on;
		pattern_check.dnd_on = 0;
	}
	else
	{
		pattern_check.dnd_on = 1;
	}
}

void call_dnd_off( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DND_OFF_CNT;
		p_pattern_func = pattern_dnd_off;
		pattern_check.dnd_off = 0;
	}
	else
	{
		pattern_check.dnd_off = 1;
	}
}
#endif //ENABLE_DND

void call_incoming_call( void )
{
	pattern_timer_start();
	force_off_pattern();

	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
		p_pattern_func = pattern_incoming_call;
		pattern_check.incoming_call = 0;
	}
	else
	{
		pattern_check.incoming_call = 1;
	}
}

void call_activity_noti( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_ACTIVITY_NOTI_CNT;
		p_pattern_func = pattern_activity_noti;
		pattern_check.activity_noti = 0;
	}
	else
	{
		pattern_check.activity_noti = 1;
	}
}

void call_long_sit_noti( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_LONG_SIT_NOTI_CNT;
		p_pattern_func = pattern_long_sit_noti;
		pattern_check.long_sit_noti = 0;
	}
	else
	{
		pattern_check.long_sit_noti = 1;
	}
}

void call_app_noti( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_APP_NOTI_CNT;
		p_pattern_func = pattern_app_noti;
		pattern_check.app_noti = 0;
	}
	else
	{
		pattern_check.app_noti = 1;
	}
}

void call_sub_activity_noti( void )
{
	sub_pattern_timer_start();
	if( g_str_timer.ui16_sub_pattern_cnt == 0 )
	{
		g_str_timer.ui16_sub_pattern_cnt = PATTERN_ACTIVITY_NOTI_CNT;
		p_sub_pattern_func = sub_pattern_activity_noti;
		sub_pattern_check.activity_noti = 0;
	}
	else
	{
		sub_pattern_check.activity_noti = 1;
	}
}

void call_sub_long_sit_noti( void )
{
	sub_pattern_timer_start();
	if( g_str_timer.ui16_sub_pattern_cnt == 0 )
	{
		g_str_timer.ui16_sub_pattern_cnt = PATTERN_LONG_SIT_NOTI_CNT;
		p_sub_pattern_func = sub_pattern_long_sit_noti;
		sub_pattern_check.long_sit_noti = 0;
	}
	else
	{
		sub_pattern_check.long_sit_noti = 1;
	}
}

void call_sub_app_noti( void )
{
	sub_pattern_timer_start();
	if( g_str_timer.ui16_sub_pattern_cnt == 0 )
	{
		g_str_timer.ui16_sub_pattern_cnt = PATTERN_APP_NOTI_CNT;
		p_sub_pattern_func = sub_pattern_app_noti;
		sub_pattern_check.app_noti = 0;
	}
	else
	{
		sub_pattern_check.app_noti = 1;
	}
}

void force_off_pattern( void )
{
	p_pattern_func = NULL;
	pattern_check.value = 0;
	g_str_timer.ui16_pattern_cnt = 0;
#ifdef ENABLE_SSD1306
	ssd1306_clear_display();
	ssd1306_display();
#endif
}

#endif //ENABLE_PATTERN
