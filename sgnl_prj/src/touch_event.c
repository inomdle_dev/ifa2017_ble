#include "peripheral_define.h"

#ifdef ENABLE_TOUCHKEY

void touch_event( void )
{
	switch( g_str_bit.b2_touch_sense )
	{
	case TOUCH_CLICK:
#ifdef ENABLE_SSD1306
		wait_oled_twi();
		oled_scroll_timer_stop();
		menu_icon(g_str_menu.b3_menu_cnt);

		if( g_str_menu.b3_menu_cnt < ( MENU_COUNT - 1 ) )
		{
			g_str_menu.b3_menu_cnt++;
		}
		else
		{
			g_str_menu.b3_menu_cnt = 0;
		}
		g_str_menu.b2_swipe_depth = 0;
		
		g_str_menu.b1_menu_flag = 1;
#endif
		break;
		
	default : break;
	}
	
	g_str_bit.b2_touch_sense = TOUCH_NONE;
}
#endif //ENABLE_TOUCHKEY
