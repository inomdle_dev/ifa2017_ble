/**
 * File name : timer_event.c
 *
 * This file contains the source code for managing TIMER
 */

#include "peripheral_define.h"

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */

#ifdef ENABLE_PATTERN
APP_TIMER_DEF(m_pattern_timer_id);                  /**< Connection parameters timer. */

APP_TIMER_DEF(m_sub_pattern_timer_id);                  /**< Connection parameters timer. */
#endif //ENABLE_PATTERN
APP_TIMER_DEF(m_rt_timer_id);                  /**< Connection parameters timer. */

#ifdef ENABLE_PEDOMETER
APP_TIMER_DEF(m_pedo_timer_id);                  /**< Connection parameters timer. */
#endif //ENABLE_PEDOMETER

#ifdef ENABLE_SSD1306
APP_TIMER_DEF(m_oled_timeout_timer_id);
APP_TIMER_DEF(m_oled_scroll_timer_id);
#endif //ENABLE_SSD1306

#ifdef ENABLE_PATTERN
/**
 * @brief Handler for timer events.
 */
static uint8_t pattern_check_flag = 0;

#ifdef ENABLE_IFA_DEMO

static void factory_reset( void )
{
	g_str_app_status.ui8_app_noti_status = 1;
	g_str_app_status.ui8_ble_bonded = 0;

	app_status_write();

#ifdef ENABLE_PATTERN
	call_device_reset();
#endif

}
#endif

void timer_pattern_event_handler(void* p_context)
{

	if( p_pattern_func != NULL )
	{
		p_pattern_func();

		if( g_str_timer.ui16_pattern_cnt == 0 )
		{
			p_pattern_func = NULL;
		}
		else
		{
			g_str_timer.ui16_pattern_cnt--;
		}
	}
	else
	{
		if( pattern_check_flag )
		{
			pattern_command_check();
		}
#ifdef ENABLE_DND
		if( g_str_app_status.ui8_dnd_status == DISABLE )
#endif //ENABLE_DND
		{
			g_str_bit.b1_haptic_status = 1;
		}
	}
}

void pattern_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_stop(m_pattern_timer_id);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_start(m_pattern_timer_id, APP_TIMER_TICKS(33, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}


void pattern_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_pattern_timer_id);
	APP_ERROR_CHECK(err_code);
}

void pattern_timer_init(void)
{
    uint32_t err_code = NRF_SUCCESS;

    /* Define a timer id used for 10Hz sample rate */
    err_code = app_timer_create(&m_pattern_timer_id, APP_TIMER_MODE_REPEATED, timer_pattern_event_handler);
    APP_ERROR_CHECK(err_code);
}


void timer_sub_pattern_event_handler(void* p_context)
{

	if( p_sub_pattern_func != NULL )
	{
		p_sub_pattern_func();

		if( g_str_timer.ui16_sub_pattern_cnt == 0 )
		{
			p_sub_pattern_func = NULL;
		}
		else
		{
			g_str_timer.ui16_sub_pattern_cnt--;
		}
	}
	else
	{
		sub_pattern_timer_stop();
		sub_pattern_command_check();
	}
}

void sub_pattern_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_stop(m_sub_pattern_timer_id);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_start(m_sub_pattern_timer_id, APP_TIMER_TICKS(33, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}


void sub_pattern_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_sub_pattern_timer_id);
	APP_ERROR_CHECK(err_code);
}

void sub_pattern_timer_init(void)
{
    uint32_t err_code = NRF_SUCCESS;

    /* Define a timer id used for 10Hz sample rate */
    err_code = app_timer_create(&m_sub_pattern_timer_id, APP_TIMER_MODE_REPEATED, timer_sub_pattern_event_handler);
    APP_ERROR_CHECK(err_code);
}
#endif //ENABLE_PATTERN

void timer_rt_event_handler(void* p_context)
{
#ifdef ENABLE_PEDOMETER

	if( g_str_time.ui32_date )
	{
		uint32_t cur_time = (g_str_time.ui32_hour&0xFF)<<8;
		cur_time |= (g_str_time.ui8_minute&0xFF);
		if( ( g_str_app_status.ui8_longsit_status ) &&
		   ( g_str_app_status.ui16_longsit_start_time > cur_time ) &&
		   ( g_str_app_status.ui16_longsit_end_time < cur_time ) )
		{
			if( ++g_str_timer.ui16_stay_hour == 3600 )
			{
				uint8_t byte[3];
				uint8_t nus_index = 0;

				byte[++nus_index] = REPORT_APP_CONFIG;
				byte[++nus_index] = REP_CONFIG_LONGSIT_STATUS;
				byte[0] = ++nus_index;
				nus_tx_push(byte, byte[0]);
				
				g_str_timer.ui16_stay_hour = 0;
		#ifdef ENABLE_PATTERN
				call_long_sit_noti();
		#endif
			}
		}

#if 1//def ENABLE_CTS
		if( ++g_str_time.ui8_second > 59 )
		{
			if( g_str_time.ui8_minute < 59 )
			{
				g_str_time.ui8_minute++;
			}
			else
			{
				pedo_flash_hour_write();
				g_str_time.ui8_minute = 0;
				time_update();
			}
			g_str_time.ui8_second = 0;
		}
#else
		if( ++g_str_timer.ui16_pedo_hour == 3600 )
		{
			pedo_flash_hour_write();
			
			g_str_timer.ui16_pedo_hour = 0;
			
			time_update();
		}

#endif
	}
#else //ifndef ENABLE_PEDOMETER
	if( g_str_time.ui32_date )
	{
		if( ++g_str_time.ui8_second == 60 )
		{
			if( g_str_time.ui8_minute < 59 )
			{
				g_str_time.ui8_minute++;
			}
		}
	}
#endif
	
#ifdef ENABLE_IFA_DEMO
	if( g_str_bit.b1_long_touch )
	{
		if( g_str_bit.b1_conn_status )
		{
			if( ++g_str_timer.ui16_reset_cnt == 10 )
			{
				g_str_bit.b1_reset_flag = 1;
			}
		}
		else
		{
			if( ++g_str_timer.ui16_reset_cnt == 5 )
			{
				g_str_bit.b1_reset_flag = 1;
			}
		}
		
		if( g_str_bit.b1_reset_flag )
		{
			factory_reset();
		}
	}
#endif

	if( ( ++g_str_timer.ui16_batt_cnt %60 ) == 2 )
	{
#ifdef ENABLE_ADC
		if( !g_str_bit.b1_chg_status )
		{
			adc_enable();
		}
#else
	#ifdef ENABLE_FUEL_GAUGE
		fuel_gauge_init();
	#endif
#endif //ENABLE_ADC
		g_str_timer.ui16_batt_cnt = 2;
	}
	
	if( g_str_bit.b1_ble_send_wait )
	{
		static int wait_cnt = 0;
		
		if( wait_cnt )
		{
			g_str_bit.b1_ble_send_wait = 0;
			wait_cnt = 0;
		}
		else
		{
			wait_cnt++;
		}
	}
	
	if( g_str_bit.b1_ble_conn_noti )
	{
		//disconn status
		if( ++g_str_timer.ui16_ble_con_cnt > 5 )
		{
			g_str_timer.ui16_ble_con_cnt = 0;
			g_str_bit.b1_ble_conn_noti = 0;
#ifdef ENABLE_SSD1306
			bt_status_popup();
#endif
		}
	}
	
#ifdef ENABLE_CTS	
	if( g_str_bit.b1_cts_ready )
	{
		if (m_cts.conn_handle != BLE_CONN_HANDLE_INVALID)
		{
			ret_code_t ret = ble_cts_c_current_time_read(&m_cts);
			if (ret == NRF_ERROR_NOT_FOUND)
			{
				NRF_LOG_INFO("Current Time Service is not discovered.\r\n");
			}
		}
	}
#endif
}

void monitor_timer_init( void )
{
	uint32_t err_code = NRF_SUCCESS;

	/* Define a timer id used for 10Hz sample rate */
	err_code = app_timer_create(&m_rt_timer_id, APP_TIMER_MODE_REPEATED, timer_rt_event_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_start(m_rt_timer_id, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}

void day_of_week( void )
{
	uint8_t year	= (uint8_t)(g_str_time.ui32_date>>16);
	uint8_t month	= (uint8_t)((g_str_time.ui32_date&0xff00)>>8);
	uint8_t days	= (uint8_t)(g_str_time.ui32_date & 0xff);

	int total = (year - 1) * 365 + (((year-1) / 4) - ((year-1) / 100) + ((year-1) / 400));
	int m[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};		// last day of month
	m[1] =  year % 4 == 0 && year % 100 != 0 || year % 400 == 0 ? 29 : 28;	// check leap year

	for(int i=1; i <month; i++)
	{
		total += m[i-1];

	}
	total += days;
	
	g_str_time.ui8_last_day_of_week = g_str_time.ui8_day_of_week;
	g_str_time.ui8_day_of_week = total % 7;
	
	if( !g_str_bit.b1_por_status )
	{
#ifdef ENABLE_PEDOMETER
		g_ui32_pedometer_times = 0;
		memset(&g_str_pedo, 0, sizeof(pedometer_struct_t));
		app_status_read();
		reset_steps();
#endif
	}
	
	g_str_bit.b1_daily_step_noti = 1;
	g_str_bit.b1_daily_active_noti = 1;
}

void time_update( void )
{
	uint32_t year = 0;
	uint32_t month = 0;
	uint32_t days = 0;

	if(++g_str_time.ui32_hour >= 24)// When it is 24 o'clock
	{       
		
		g_str_time.ui32_hour = g_str_time.ui32_hour % 24;
		year = (g_str_time.ui32_date&0xff0000)>>16;
		month = (g_str_time.ui32_date&0xff00)>>8;
		days = (g_str_time.ui32_date & 0xff);

		if( ( (month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12) ) && (days == 31) )
		{
			month++;
			days = 1;
			if(month == 13)
			{
				year++;
				month = 1;
			}
		}
		else if( ( (month == 4) || (month == 6) || (month == 9) || (month == 11) ) && (days == 30) )
		{
			month++;
			days = 1;
		}
		else if( (month == 2) && ( ( ( (year % 4) == 0 ) && (days == 29) ) || ( ( (year % 4) != 0 ) && (days == 28) ) ) )//leap year
		{
			month++;
			days = 1;
		}
		else
		{
			days++;
		}
#ifdef ENABLE_PEDOMETER
		if( g_ui32_flash_hour_write_cnt )
		{
			pedo_flash_daily_write();
		}
#endif				
		g_str_time.ui32_date = (year<<16) | (month<<8) | days;
		day_of_week();
	}
	
}

#ifdef ENABLE_PEDOMETER

void timer_pedo_event_handler(void* p_context)
{
	static uint32_t ui32_last_step = 0;
	static uint16_t ui16_err_cnt = 0;

	if( g_str_bit.b2_pedo_mode )
	{
		if( g_str_bit.b2_pedo_mode == 1 )
		{
			g_str_pedo.ui16_total_walk_time++;
		}
		else if ( g_str_bit.b2_pedo_mode == 2 )
		{
			g_str_pedo.ui16_total_run_time++;
		}

		g_ui32_pedometer_times++;
	}

	if( g_str_app_status.ui8_activity_status )
	{
		if( g_str_bit.b1_daily_active_noti )
		{
			if( g_ui32_pedometer_times > ( g_str_app_status.ui16_target_active_time * 60 ) )
			{
				// noti times
				if( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING )
				{
					if( !g_str_app_status.ui8_dnd_status )
					{
						call_sub_activity_noti();
					}
				}
				else
				{
					call_activity_noti();
				}
			}
			g_str_bit.b1_daily_active_noti = 0;
		}
	}	

	if( ui32_last_step == g_ui32_pedometer_steps )
	{
		ui16_err_cnt++;
	}
	else
	{
		ui16_err_cnt = 0;
	}
	
	if( ui16_err_cnt > 5 )
	{
		if( g_str_bit.b2_pedo_mode )
		{
			pedo_timer_stop();
		}
		g_str_bit.b2_pedo_mode = 0;
	}
	
	ui32_last_step = g_ui32_pedometer_steps;
}

void pedo_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(m_pedo_timer_id);
	APP_ERROR_CHECK(err_code);
}

void pedo_timer_start( void )
{
	uint32_t err_code = NRF_SUCCESS;

	err_code = app_timer_start(m_pedo_timer_id, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
}

void pedo_timer_init( void )
{
	uint32_t err_code = NRF_SUCCESS;

	/* Define a timer id used for 10Hz sample rate */
	err_code = app_timer_create(&m_pedo_timer_id, APP_TIMER_MODE_REPEATED, timer_pedo_event_handler);
	APP_ERROR_CHECK(err_code);

}

#endif //ENABLE_PEDOMETER

#ifdef ENABLE_SSD1306
static void oled_timeout_timer_handler(void * p_context)
{
	if( g_str_menu.b1_menu_flag )
	{
		g_str_menu.b1_menu_flag = 0;
		g_str_menu.b3_menu_cnt = 0;
	}
	oled_timeout_timer_stop();
#if 0
	if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
	{
		ssd1306_print_char((uint8_t *)g_str_ancs.ui8_title, g_str_ancs.ui8_title_len);//package name
		
	}
#endif
}

void oled_timeout_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;

	ssd1306_clear_display();
	ssd1306_display();

	err_code = app_timer_stop(m_oled_timeout_timer_id);
	APP_ERROR_CHECK(err_code);

	if( g_str_call_bit.b4_call_status == CALL_STATUS_INCOMING )
	{
		if( g_str_menu.b3_income == 5 )
		{
#ifdef ENABLE_IFA_DEMO
			static uint8_t repeat_cnt = 0;
			
			if( ++repeat_cnt > 2 )
			{
				g_str_call_bit.b4_call_status = CALL_STATUS_READY;
				g_str_menu.b3_income = 0;
				repeat_cnt = 0;
			}
			else
			{
				incoming_call_popup();
			}
#else			
			incoming_call_popup();
#endif
		}
		else
		{
			return;
		}
	}
	
	pattern_check_flag = 1;
	
	if( g_str_bit.b1_live_update )
	{
		pedo_flash_live_backup();
		g_str_bit.b1_live_update = 0;
	}

	if( !g_str_bit.b1_oled_scroll )
	{
		ssd1306_command(SSD1306_DISPLAYOFF); // 0xAE
		nrf_gpio_pin_clear(SSD1306_CONFIG_VDD_PIN); // vdd
	}
}

void oled_timeout_timer_start( uint8_t u8_sec )
{
	uint32_t err_code = NRF_SUCCESS;
	
	err_code = app_timer_stop(m_oled_timeout_timer_id);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_start(m_oled_timeout_timer_id, APP_TIMER_TICKS(u8_sec * 1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
	
	pattern_check_flag = 0;
}

void oled_timeout_init( void )
{
	ret_code_t ret;

	ret = app_timer_create(&m_oled_timeout_timer_id,
                           APP_TIMER_MODE_SINGLE_SHOT,
                           oled_timeout_timer_handler);

    APP_ERROR_CHECK(ret);
}

uint8_t * cur;
uint8_t * next;
uint16_t direct;
uint8_t byte[SSD1306_LCDWIDTH*SSD1306_LCDHEIGHT/8];

static void oled_scroll_timer_handler(void * p_context)
{
	static int k = 0;

	if( direct > 0 )
	{
		if( direct == 1 )
		{
			oled_timeout_timer_stop();
		}
		direct--;
		k = 1;
	}
	else
	{
	
		for( int l = 0; l < 4; l++ )
		{
			for (int i = 0; i < SSD1306_LCDWIDTH-k; i++)
			{
				byte[i+(SSD1306_LCDWIDTH * l)] = cur[(i+k)+(SSD1306_LCDWIDTH * l)];
			}
			
			for (int j = SSD1306_LCDWIDTH-k; j < SSD1306_LCDWIDTH; j++)
			{
				byte[j+(SSD1306_LCDWIDTH * l)] = next[(k+j-SSD1306_LCDWIDTH)+(SSD1306_LCDWIDTH * l)];
			}
			
			ssd1306_command(0xB0 + l);
			ssd1306_command(0x00);
			ssd1306_command(0x12);

			twi_mul_write(SSD1306_I2C_ADDRESS, &byte[SSD1306_LCDWIDTH * l], SSD1306_LCDWIDTH);
		}
		
		k += 2;
		if( k > 61 )
		{
			k = 0;
			oled_scroll_timer_stop();
			
			if( !g_str_menu.b3_income )
			{
				ssd1306_fill_array(next);

				g_str_bit.b1_oled_scroll = 0;
	
			}
		}
	}
}

void oled_scroll_timer_stop( void )
{
	uint32_t err_code = NRF_SUCCESS;

	if ( !g_str_menu.b3_income )
	{
		err_code = app_timer_stop(m_oled_scroll_timer_id);
		APP_ERROR_CHECK(err_code);
	}
	else
	{
		if( g_str_call_bit.b4_call_status == CALL_STATUS_INCOMING )
		{
			if( g_str_menu.b3_income == 1 )
			{
				ssd1306_auto_scroll((uint8_t *)icon_call_name1, (uint8_t *)icon_call_name2, 100);
				g_str_menu.b3_income++;
			}
			else if( g_str_menu.b3_income == 2 )
			{
				ssd1306_auto_scroll((uint8_t *)icon_call_name2, (uint8_t *)icon_call_name3, 100);
				g_str_menu.b3_income ++;
			}
			else if( g_str_menu.b3_income == 3 )
			{
				ssd1306_auto_scroll((uint8_t *)icon_call_name3, (uint8_t *)icon_call_name4, 100);
				g_str_menu.b3_income ++;
			}
			else if( g_str_menu.b3_income == 4 )
			{
				err_code = app_timer_stop(m_oled_scroll_timer_id);
				APP_ERROR_CHECK(err_code);

				ssd1306_fill_array((uint8_t *)icon_call_name4);
				g_str_menu.b3_income ++;
			}
		}
	}

	direct = 0;
}

void oled_scroll_timer_start( uint8_t *cur_icon, uint8_t *next_icon, uint16_t direction )
{
	uint32_t err_code = NRF_SUCCESS;
	
	err_code = app_timer_stop(m_oled_scroll_timer_id);
	APP_ERROR_CHECK(err_code);
	
	cur = cur_icon;
	next = next_icon;
	direct = direction;
	
	if( g_str_menu.b3_income )
	{
		err_code = app_timer_start(m_oled_scroll_timer_id, APP_TIMER_TICKS(20, APP_TIMER_PRESCALER), NULL);
	}
	else
	{
		err_code = app_timer_start(m_oled_scroll_timer_id, APP_TIMER_TICKS(10, APP_TIMER_PRESCALER), NULL);
	}
	
	APP_ERROR_CHECK(err_code);
}

void oled_scroll_timer_init( void )
{
	ret_code_t ret;

	ret = app_timer_create(&m_oled_scroll_timer_id,
                           APP_TIMER_MODE_REPEATED,
                           oled_scroll_timer_handler);

    APP_ERROR_CHECK(ret);
}
#endif

void user_timers_init( void )
{
	monitor_timer_init();
#ifdef ENABLE_PATTERN
	pattern_timer_init();
	sub_pattern_timer_init();
#endif
#ifdef ENABLE_PEDOMETER
	pedo_timer_init();
#endif
	
#ifdef ENABLE_SSD1306
	oled_timeout_init();
	oled_scroll_timer_init();
#endif
}
