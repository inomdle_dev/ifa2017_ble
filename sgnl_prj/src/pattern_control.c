/**
 * File name : pattern_control.c
 *
 * This file contains the source code for LED/haptic control
 */

#include "peripheral_define.h"

#ifdef ENABLE_PATTERN

void pattern_power_off( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_POWER_OFF_CNT )
#ifdef ENABLE_SSD1306		
	ssd1306_print_char("power_off", 9);
#endif
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 82 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 94 )
	{
		haptic_on();
	}
	else
	{
		uint32_t ret;
		
		haptic_off();
		g_str_timer.ui16_pattern_cnt = 0;
#ifdef ENABLE_IFA_DEMO
		//ret = sd_nvic_SystemReset();
		ret = sd_power_system_off();
#else
		ret = sd_power_system_off();
#endif
    	APP_ERROR_CHECK(ret);
	}
}

void pattern_power_on( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_POWER_ON_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("power_on",8);
#endif
	}
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 80 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 92 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_device_reset( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_DEVICE_RESET_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("device_reset",12);
#endif
	}
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DEVICE_RESET_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 6 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
		call_power_off();
	}
}

void pattern_batt_gauge( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_BATT_GAUGE_CNT )
	{
		uint8_t byte[10] = "";
		
		sprintf((char*)byte, "batt:%3d%%", g_str_batt.ui16_percent);
#ifdef ENABLE_SSD1306
		ssd1306_print_char(byte,sizeof(byte));
#endif
	}
}

void pattern_batt_low( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_BATT_LOW_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("batt low",8);
#endif
	}
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_LOW_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 6 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_batt_charging( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_BATT_CHARGING_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("batt charging:77%",17);
#endif
	}
}

void pattern_batt_full( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_BATT_FULL_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("batt full",9);
#endif
	}
}

void pattern_unpaired( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_UNPAIR_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("unpaired",8);
#endif
	}
}

void pattern_unpair( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_UNPAIR_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("unpair",6);
#endif
	}
}

void pattern_wait_pairing( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_WAIT_PAIRING_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("wait pairing",12);
#endif
	}

	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 7 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 13 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
		
}

void pattern_pairing_complete( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_PAIRING_COMPLETE_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("pairing complete",16);
#endif
	}
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 9 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 15 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_disconn_timeout( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_DISCONN_TIMEOUT_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("disconn timeout",15);
#endif
	}
	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 9 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 11 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 20 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 24 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 26 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 30 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 35 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

#ifdef ENABLE_DND
void pattern_dnd_on( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_DND_ON_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("DND On",6);
#endif
	}
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 7 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 13 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_dnd_off( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_DND_OFF_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("DND Off",7);
#endif
	}
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 7 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 13 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}
#endif //ENABLE_DND

void pattern_incoming_call( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_INCOMING_CALL_CNT )
	{
#ifdef ENABLE_SSD1306
		incoming_call_popup();
#endif
	}

	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 14 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 22 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 31 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 34 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 43 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 46 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 62 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 76 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
		g_str_timer.ui16_pattern_cnt = 0;
		
	}
}

void pattern_activity_noti( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_ACTIVITY_NOTI_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("complete active",15);
#endif
	}
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 17 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 31 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 56 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 60 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 66 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 80 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 86 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_long_sit_noti( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_LONG_SIT_NOTI_CNT )
	{
#ifdef ENABLE_SSD1306
		ssd1306_print_char("longsit move",12);
#endif
	}
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 5 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 25 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 35 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 55 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_app_noti( void )
{
	if( g_str_timer.ui16_pattern_cnt == PATTERN_APP_NOTI_CNT )
	{
#if 1
	#ifdef ENABLE_SSD1306
		app_notify();
	#endif
#else
		
	#ifdef ENABLE_SSD1306
		//ssd1306_print_char((uint8_t *)g_str_ancs.ui8_title, g_str_ancs.ui8_title_len);//package name
		if( !strncmp((char *)g_str_ancs.ui8_title, "Message", 7) )
		{
			ssd1306_fill_array((uint8_t *)icon_sms);
		}
		else
		{
			ssd1306_fill_array((uint8_t *)icon_noti);
		}
	#endif
#endif
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 8 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 14 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 24 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 30 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void sub_pattern_activity_noti( void )
{
	//haptic control
	if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 17 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 31 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 56 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 60 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 66 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 80 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_ACTIVITY_NOTI_CNT - 86 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void sub_pattern_long_sit_noti( void )
{
	//haptic control
	if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 5 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 25 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 35 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_LONG_SIT_NOTI_CNT - 55 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void sub_pattern_app_noti( void )
{
	//haptic control
	if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 8 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 14 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 24 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 30 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

#endif //ENABLE_PATTERN
