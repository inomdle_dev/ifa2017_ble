/** 
 * File name : adc_event.c
 *
 * This file contains the source code for a sampling battery ADC data
 */

#include "peripheral_define.h"

#define SAMPLES_IN_BUFFER 8

#ifdef ENABLE_ADC

static const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(4); /* ADC : use Timer 4 */
static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];
static nrf_ppi_channel_t     m_ppi_channel;

void timer_handler(nrf_timer_event_t event_type, void * p_context)
{
	/* do not anything */
}

void adc_data_send( uint16_t ui16_percent )
{
	uint8_t byte[5];
	uint8_t nus_index = 0;
	byte[++nus_index] = REPORT_APP_CONFIG;
	byte[++nus_index] = REP_CONFIG_BATT_STATUS;
	byte[++nus_index] = REP_BATT_PERCENTAGE;

	if( ui16_percent > 96 )
	{
		byte[++nus_index] = 100;
	}
	else if( ui16_percent > 2 )
	{
		byte[++nus_index] = ui16_percent;
	}
	else
	{
		byte[++nus_index] = 2;
	}
	
	byte[0] = ++nus_index;
	nus_tx_push(byte,byte[0]);
	
	g_str_batt.ui16_level = (g_str_batt.ui16_percent / 20);
		
	if( g_str_batt.ui8_charger_status == BATT_STATUS_NO_PLUG )
	{
		if( g_str_batt.ui8_low_level == BATT_LOW_NONE )
		{
			if( g_str_batt.ui16_percent < 20 )
			{
				g_str_batt.ui8_low_level = BATT_LOW_20;
	#ifdef ENABLE_PATTERN
		#ifdef ENABLE_SSD1306
				batt_popup();
		#endif
	#endif
			}
		}
		else if( g_str_batt.ui8_low_level == BATT_LOW_20 )
		{
			if( g_str_batt.ui16_percent < 10 )
			{
				g_str_batt.ui8_low_level = BATT_LOW_10;
	#ifdef ENABLE_PATTERN
		#ifdef ENABLE_SSD1306
				batt_popup();
		#endif
	#endif
			}
			else if( g_str_batt.ui16_percent > 20 )
			{
				g_str_batt.ui8_low_level = BATT_LOW_NONE;
			}
		}
		else if( g_str_batt.ui8_low_level == BATT_LOW_10 )
		{
			if( g_str_batt.ui16_percent < 5 )
			{
				g_str_batt.ui8_low_level = BATT_LOW_5;
	#ifdef ENABLE_PATTERN
		#ifdef ENABLE_SSD1306
				batt_popup();
		#endif
	#endif
			}
			else if( g_str_batt.ui16_percent > 20 )
			{
				g_str_batt.ui8_low_level = BATT_LOW_NONE;
			}
			else if( g_str_batt.ui16_percent > 10 )
			{
				g_str_batt.ui8_low_level = BATT_LOW_20;
			}
		}
	}
	
}

void saadc_sampling_event_init(void)
{
    ret_code_t err_code;

    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
    err_code = nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
    APP_ERROR_CHECK(err_code);

    /* setup m_timer for compare event every 400ms */
    uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, 400);
    nrf_drv_timer_extended_compare(&m_timer,
                                   NRF_TIMER_CC_CHANNEL0,
                                   ticks,
                                   NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
                                   false);
    nrf_drv_timer_enable(&m_timer);

    uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer,
                                                                                NRF_TIMER_CC_CHANNEL0);
    uint32_t saadc_sample_task_addr   = nrf_drv_saadc_sample_task_get();

    /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel,
                                          timer_compare_event_addr,
                                          saadc_sample_task_addr);
    APP_ERROR_CHECK(err_code);
}

void saadc_sampling_event_enable(void)
{
    ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel);

    APP_ERROR_CHECK(err_code);
}

void saadc_sampling_event_disable(void)
{
    ret_code_t err_code = nrf_drv_ppi_channel_disable(m_ppi_channel);

    APP_ERROR_CHECK(err_code);
}

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
#ifdef ENABLE_IFA_DEMO
	if( g_str_bit.b1_chg_status )
	{
		adc_disable();
		return;
	}
#endif
	uint16_t ui16_sampling_data = 0;
	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        //for (int i = 0; i < SAMPLES_IN_BUFFER; i++)
        {
			//ui16_sampling_data += p_event->data.done.p_buffer[i];
			ui16_sampling_data = p_event->data.done.p_buffer[SAMPLES_IN_BUFFER - 1];
        }
		
		uint16_t ui16_tmp_percent = 0;
		if(nrf_drv_gpiote_in_is_set(CHK_VBUS))
		{
			ui16_tmp_percent = 0;
		}
		else
		{
			ui16_tmp_percent = ( uint16_t )( ( ui16_sampling_data - 355 ) * 1.5 );
		}

		if( ui16_tmp_percent >= 65000 )
		{
			ui16_tmp_percent = 0;
		}
		if( ui16_tmp_percent >= 100 )//batt is higher than 100
		{
			ui16_tmp_percent = 100;
		}
		else if( ui16_tmp_percent <= 0 )//batt is lower than 0
		{
			ui16_tmp_percent = 0;
			call_power_off();
		}
#if 0
		if( !g_str_batt.ui16_previous_percent )
		{
		   g_str_batt.ui16_previous_percent = ui16_tmp_percent;
		}

		if( ui16_tmp_percent > g_str_batt.ui16_previous_percent )//now batt percentage > previous batt percentage
		{
			g_str_batt.ui16_percent = g_str_batt.ui16_previous_percent;
		}
		else
		{
			g_str_batt.ui16_percent = ui16_tmp_percent;
		}
		
		g_str_batt.ui16_previous_percent = g_str_batt.ui16_percent;
#else
		g_str_batt.ui16_percent = ui16_tmp_percent;
#endif
    }
	
	adc_disable();

	adc_data_send( g_str_batt.ui16_percent );
	
	if(ui16_sampling_data >= 430)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_2;
	else if(ui16_sampling_data >= 420)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_1;
	else if(ui16_sampling_data >= 410)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_0;
	else if(ui16_sampling_data >= 400)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_9;
	else if(ui16_sampling_data >= 390)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_8;
	else if(ui16_sampling_data >= 380)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_7;
	else if(ui16_sampling_data >= 370)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_6;
	else if(ui16_sampling_data >= 360)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_5;
	else if(ui16_sampling_data >= 350)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_4;
	else if(ui16_sampling_data >= 340)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_3;
	else if(ui16_sampling_data >= 330)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_2;
	else if(ui16_sampling_data >= 340)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_1;
	else if(ui16_sampling_data >= 310)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_0;
	
	ui16_sampling_data = 0;
}

void adc_enable( void )
{
	if(g_str_batt.ui16_batt_status == BATT_STATUS_IN_CHARGING)//while charging
	{
		g_str_batt.ui16_percent++;

		if( g_str_batt.ui16_percent > 100 )
		{
			g_str_batt.ui16_percent = 100;
			
			if( ++g_str_timer.ui16_charge_cnt > 10 )
			{
				g_str_batt.ui16_batt_status = BATT_STATUS_CHARGING_COMPLETE;
				
				uint8_t byte[5] = "";
				uint8_t nus_index = 0;
				
				byte[++nus_index] = REPORT_APP_CONFIG;
				byte[++nus_index] = REP_CONFIG_BATT_STATUS;
				byte[++nus_index] = REP_BATT_CHARGER;
				byte[++nus_index] = g_str_batt.ui16_batt_status;
				
				byte[0] = ++nus_index;
				nus_tx_push(byte, byte[0]);
				
				g_str_timer.ui16_charge_cnt = 0;
			}
		}
		else
		{
			if( g_str_timer.ui16_charge_cnt	)
			{
				g_str_timer.ui16_charge_cnt = 0;
			}
		}
		
		g_str_batt.ui16_previous_percent = g_str_batt.ui16_percent;

		adc_data_send( g_str_batt.ui16_percent );
	}
	else
	{
#ifndef ENABLE_ES2_BOARD
		nrf_drv_gpiote_out_set(BATT_CHK_ON);
#endif
		nrf_drv_timer_enable(&m_timer);
		saadc_sampling_event_enable();
	}
}

void adc_disable( void )
{
	saadc_sampling_event_disable();
	nrf_drv_timer_disable(&m_timer);
	nrf_drv_gpiote_out_clear(BATT_CHK_ON);
}

void adc_init( void )
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0); /* AIN0 P0.02 */

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
	
	saadc_sampling_event_init();
	saadc_sampling_event_enable();
}

#endif //ENABLE_ADC
