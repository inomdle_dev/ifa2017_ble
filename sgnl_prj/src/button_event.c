/**
* File name : button_event.c
*
* This file contains the source code for a processing button event
*/

#include "peripheral_define.h"

#define KEY_FUNC_RELEASE	BSP_EVENT_KEY_2
#define KEY_UP_RELEASE		BSP_EVENT_KEY_1
#define KEY_DOWN_RELEASE	BSP_EVENT_KEY_0

#define KEY_FUNC_LONG	BSP_EVENT_LONG_BTN_2
#define KEY_UP_LONG		BSP_EVENT_LONG_BTN_1
#define KEY_DOWN_LONG	BSP_EVENT_LONG_BTN_0

#define KEY_FUNC_PUSH	BSP_EVENT_PUSH_BTN_2
#define KEY_UP_PUSH		BSP_EVENT_PUSH_BTN_1
#define KEY_DOWN_PUSH	BSP_EVENT_PUSH_BTN_0

enum {
	KEY_FUNC_PUSHED = 1,
	KEY_UP_PUSHED = 2,
	KEY_DOWN_PUSHED = 4,
	
	KEY_FUNC_UP = 3,
	KEY_FUNC_DOWN = 5,
	KEY_UP_DOWN = 6,
	
	KEY_FUNC_RELEASED = ~(KEY_FUNC_PUSHED),
	KEY_UP_RELEASED = ~(KEY_UP_PUSHED),
	KEY_DOWN_RELEASED = ~(KEY_DOWN_PUSHED),
	
	KEY_FACTORY_RESET = 7,
};

#ifdef ENABLE_BUTTON
static uint16_t combi_key, long_key;
static uint16_t factory_reset_ready;
static uint16_t key_status;

static void factory_reset( void )
{
	user_flash_all_erase();
	memset(&g_str_timer, 0, sizeof(timer_struct_t));
	memset(&g_str_time, 0, sizeof(time_info_struct_t));
#ifdef ENABLE_PEDOMETER
	memset(&g_str_pedo, 0, sizeof(pedometer_struct_t));
#endif

	memset(&g_str_app_status, 0, sizeof(init_status_struct_t));
	g_str_app_status.ui8_app_noti_status = 1;
	
	g_str_app_status.ui16_longsit_start_time = 9<<8; // default 9 AM
	g_str_app_status.ui16_longsit_end_time = 18<<8; // default 6 PM

	app_status_write();

#ifdef ENABLE_PATTERN
	call_device_reset();
#endif

}

void button_event( bsp_event_t event )
{
	//ret_code_t ret;
	
	switch (event)
	{
		// long down button
	case KEY_DOWN_LONG :
		long_key = 1;
		if( factory_reset_ready )
		{
			//force reset
			factory_reset();
		}
		else
		{
#ifdef ENABLE_DND
			if( g_str_app_status.ui8_dnd_status )
			{
				g_str_app_status.ui8_dnd_status = DISABLE;
				app_status_write();
	#ifdef ENABLE_PATTERN
				call_dnd_off();
	#endif
			}
			else
			{
				g_str_app_status.ui8_dnd_status = ENABLE;
				app_status_write();
	#ifdef ENABLE_PATTERN
				call_dnd_on();
	#endif
			}
			
#endif //ENABLE_DND

		}
		break;
		
		// long up button
	case KEY_UP_LONG :
		long_key = 1;
		if( factory_reset_ready )
		{
			//force reset
			factory_reset();
		}
		else
		{
#ifdef ENABLE_DND
			if( g_str_app_status.ui8_dnd_status )
			{
				g_str_app_status.ui8_dnd_status = DISABLE;
				app_status_write();
	#ifdef ENABLE_PATTERN
				call_dnd_off();
	#endif
			}
			else
			{
				g_str_app_status.ui8_dnd_status = ENABLE;
				app_status_write();
	#ifdef ENABLE_PATTERN
				call_dnd_on();
	#endif
			}
			
#endif //ENABLE_DND
		}
		break;
		
		// long func button
	case KEY_FUNC_LONG :
		long_key = 1;
		if( factory_reset_ready )
		{
			//force reset
			factory_reset();
		}
		else
		{
			if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
			{
				if( g_str_app_status.ui8_host_device == HOST_DEVICE_IPHONE )
				{
					ancs_negative_action();
				}
				else
				{
					uint8_t byte[3] = "";
					uint8_t nus_index = 0;

					byte[++nus_index] = REPORT_CALL_STATUS;
					byte[++nus_index] = REP_CALL_END;
					byte[0] = ++nus_index;
					
					nus_tx_push(byte, byte[0]);
#ifdef ENABLE_PATTERN 
					force_off_pattern();
#endif
				}
			}
			else
			{
				g_str_bit.b1_power_off = 1;
			}
		}
		break;
		
		//func button
	case KEY_FUNC_RELEASE :
		key_status &= KEY_FUNC_RELEASED;
		if( combi_key )
		{
			break;
		}
		if( long_key )
		{
			long_key = 0;
			break;
		}

		{
			if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
			{
				//positive action
			}
			else
			{
#ifdef ENABLE_BATT
	#ifdef ENABLE_PATTERN
				call_batt_gauge();
	#endif
#endif
			}
		}
		break;
		
		// down button
	case KEY_DOWN_RELEASE:
		key_status &= KEY_DOWN_RELEASED;
		if( combi_key )
		{
			break;
		}

		if( long_key )
		{
			long_key = 0;
			break;
		}
		
		if( g_str_bit.b1_conn_status )
		{
			if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
			{
				g_str_bit.b1_haptic_status = 0;
			}
			else if( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING )
			{

			}
		}
		break;
		
		// up button
	case KEY_UP_RELEASE:
		key_status &= KEY_UP_RELEASED;
		if( combi_key )
		{
			break;
		}

		if( long_key )
		{
			long_key = 0;
			break;
		}
		
		if( g_str_bit.b1_conn_status )
		{
			if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
			{
				g_str_bit.b1_haptic_status = 0;
			}
			else if( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING )
			{
			}
		}
		break;

	case KEY_FUNC_PUSH :
		key_status |= KEY_FUNC_PUSHED;
		break;
	case KEY_UP_PUSH :
		key_status |= KEY_UP_PUSHED;
		break;
	case KEY_DOWN_PUSH :
		key_status |= KEY_DOWN_PUSHED;
		break;
	default:
		break;
	}

	if( key_status == KEY_FACTORY_RESET )
	{
		factory_reset_ready = 1;
	}
	else
	{
		if( ( key_status == KEY_FUNC_UP ) || ( key_status == KEY_FUNC_DOWN ) || ( key_status == KEY_UP_DOWN ) )
		{
			combi_key = 1;
		}
		else
		{
			if( key_status == 0 )
			{
				combi_key = 0;
			}
		}
		
		factory_reset_ready = 0;
	}
}
#endif //ENABLE_BUTTON
