/**
 * File name : twi_event.c
 *
 * This file contains the source code for managing TWI(I2C)
 */

#include "peripheral_define.h"

#define ICM20648_ADDR	0x68U//(0x68U >> 1)

#define INV_SUCCESS		(0)

#ifdef ENABLE_TWI

static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(0);
static volatile bool m_xfer_done = true;
static volatile bool m_rx_done = true;

static volatile bool m_pedo_xfer_done = true;
static volatile bool m_fuel_xfer_done = true;
static volatile bool m_fuel_rx_done = true;

static volatile bool m_oled_xfer_done = true;

static uint8_t tmp_data[256];
static uint8_t tmp_length;
static uint8_t tmp_addr;

void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{   
	if( m_pedo_xfer_done == false )
	{
		m_pedo_xfer_done = true;
	}
	
	m_oled_xfer_done = true;

	switch(p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if ((p_event->type == NRF_DRV_TWI_EVT_DONE) &&
                (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_TX))
            {
				m_xfer_done = true;
			}
            else if ((p_event->type == NRF_DRV_TWI_EVT_DONE) &&
                (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX))
            {
				m_rx_done = true;
            }
            break;
			
		case NRF_DRV_TWI_EVT_ADDRESS_NACK:

			if( !m_xfer_done )
			{
				nrf_drv_twi_tx(&m_twi, tmp_addr, tmp_data, tmp_length , false); 
			}
			if( !m_rx_done )
			{
				nrf_drv_twi_rx(&m_twi, tmp_addr, (uint8_t *)tmp_data, tmp_length);
			}
			break;
		case NRF_DRV_TWI_EVT_DATA_NACK:

			if( !m_xfer_done )
			{
				ret_code_t err_code = nrf_drv_twi_tx(&m_twi, tmp_addr, tmp_data, tmp_length , false);
				APP_ERROR_CHECK(err_code);
			}
			if( !m_rx_done )
			{
				ret_code_t err_code = nrf_drv_twi_rx(&m_twi, tmp_addr, (uint8_t *)tmp_data, tmp_length);
				APP_ERROR_CHECK(err_code);
			}
			break;
        default:
            break;        
    }   
}

/**
 * @brief UART initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

	nrf_gpio_cfg(
	SCL_PIN,
	NRF_GPIO_PIN_DIR_OUTPUT,
	NRF_GPIO_PIN_INPUT_DISCONNECT,
	NRF_GPIO_PIN_PULLDOWN,
	NRF_GPIO_PIN_H0H1, // NRF_GPIO_PIN_S0S1,
	NRF_GPIO_PIN_NOSENSE);
	
	nrf_gpio_cfg(
	SDA_PIN,
	NRF_GPIO_PIN_DIR_OUTPUT,
	NRF_GPIO_PIN_INPUT_DISCONNECT,
	NRF_GPIO_PIN_PULLDOWN,
	NRF_GPIO_PIN_H0H1, // NRF_GPIO_PIN_S0S1,
	NRF_GPIO_PIN_NOSENSE);


	nrf_gpio_pin_set(SCL_PIN); // vdd
	nrf_gpio_pin_set(SDA_PIN); // vdd
	
	nrf_delay_ms(10);
	
	nrf_gpio_pin_clear(SCL_PIN); // vdd
	nrf_gpio_pin_clear(SDA_PIN); // vdd
	
	nrf_delay_ms(10);
	
    const nrf_drv_twi_config_t twi_config = {
       .scl                = SCL_PIN,
       .sda                = SDA_PIN,
       .frequency          = NRF_TWI_FREQ_400K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
	
}

void wait_oled_twi(void)
{
	while(m_oled_xfer_done == false);
}

void twi_mul_write(uint8_t i2c_addr, const uint8_t *data, uint8_t length )
{
	static uint8_t control = 0x40;
	
	uint8_t reg_data[100];	
	
	g_str_bit.b1_twi_send_wait = 1;
	
	reg_data[0] = control;
	uint8_t i = 0;
	for(i=0; i < length; i = i + 1)
	{
		reg_data[i + 1] = data[i];
	}
	
	m_oled_xfer_done = false;
	//nrf_drv_twi_tx(&m_oled_twi, i2c_addr, reg_data, length+1, false);
	nrf_drv_twi_tx(&m_twi, i2c_addr, reg_data, length+1, false);
	
	while(m_oled_xfer_done == false);
	g_str_bit.b1_twi_send_wait = 0;
}

int twi_write(uint8_t i2c_addr, const uint8_t *data, uint32_t length)
{
	ret_code_t err_code;
	
	g_str_bit.b1_twi_send_wait = 1;
	m_oled_xfer_done = false;
	//err_code = nrf_drv_twi_tx(&m_oled_twi, i2c_addr, data, length , false);
	err_code = nrf_drv_twi_tx(&m_twi, i2c_addr, data, length , false);
	APP_ERROR_CHECK(err_code);

	while(m_oled_xfer_done == false);
	
	g_str_bit.b1_twi_send_wait = 0;

	return 0;
}
#ifdef ENABLE_TOUCHKEY

int touch_twi_write(uint8_t i2c_addr, const uint8_t *data, uint32_t length)
{
	ret_code_t err_code = 0;
	uint8_t retry_cnt = 10;

	m_xfer_done = false;
	err_code = nrf_drv_twi_tx(&m_twi, i2c_addr, data, length , false);
	APP_ERROR_CHECK(err_code);
	
	for( int i = 0; i < length; i++)
	{
		tmp_data[i] = data[i];
	}
	tmp_length = length;
	tmp_addr = i2c_addr;

	while( !m_xfer_done );

	UNUSED_VARIABLE(err_code);
	return (retry_cnt)? TRUE: FALSE;
}

int touch_twi_read(uint8_t i2c_addr, const uint8_t *data, uint32_t length)
{
	ret_code_t err_code = 0;
	
	uint8_t retry_cnt = 10;
		
	m_rx_done = false;
		
	err_code = nrf_drv_twi_rx(&m_twi, i2c_addr, (uint8_t *)data, length);
	APP_ERROR_CHECK(err_code);
	
	for( int i = 0; i < length; i++)
	{
		tmp_data[i] = data[i];
	}
	tmp_length = length;
	tmp_addr = i2c_addr;

	while(!m_rx_done );
	
	UNUSED_VARIABLE(err_code);

	return (retry_cnt)? TRUE: FALSE;
}
#endif //ENABLE_TOUCHKEY

int twi_check_connect( uint16_t i2c_addr )
{
	uint8_t sample_data;

	ret_code_t err_code = nrf_drv_twi_rx(&m_twi, i2c_addr, &sample_data, sizeof(sample_data));
	if (err_code == NRF_SUCCESS)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int inv_serial_interface_write_hook(uint16_t reg, uint32_t length, const uint8_t *data)
{
	ret_code_t err_code;
	
	uint8_t reg_data[20];	
	reg_data[0] = reg;
	uint8_t i = 0;
	for(i=0; i < length; i = i + 1)
	{
		reg_data[i + 1] = data[i];
	}
	
	m_pedo_xfer_done = false;
	err_code = nrf_drv_twi_tx(&m_twi, ICM20648_ADDR, reg_data, (length+1) , false);
	APP_ERROR_CHECK(err_code);

	while(m_pedo_xfer_done == false);

	return 0;
}

int inv_serial_interface_read_hook(uint16_t reg, uint32_t length, uint8_t *data)
{
	ret_code_t err_code;
	
	m_pedo_xfer_done = false;
	err_code = nrf_drv_twi_tx(&m_twi, ICM20648_ADDR, (uint8_t *)&reg, 1, true);
	APP_ERROR_CHECK(err_code);

	while(m_pedo_xfer_done == false);
	
	m_pedo_xfer_done = false;
	err_code = nrf_drv_twi_rx(&m_twi, ICM20648_ADDR, data, length);
	APP_ERROR_CHECK(err_code);

	while(m_pedo_xfer_done == false);

	return 0;
}

#ifdef ENABLE_FUEL_GAUGE
int inv_serial_interface_write_hook2(uint16_t reg, uint32_t length, const uint8_t *data)
{
	ret_code_t err_code;
	
	uint8_t reg_data[20];	
	reg_data[0] = reg;
	uint8_t i = 0;
	for(i=0; i < length; i = i + 1)
	{
		reg_data[i + 1] = data[i];
	}
	
	m_fuel_xfer_done = false;
	err_code = nrf_drv_twi_tx(&m_twi, FUEL_GAUGE_ADDRESS, reg_data, (length+1) , false);
	APP_ERROR_CHECK(err_code);

	while(m_fuel_xfer_done == false);

	return 0;
}

int inv_serial_interface_read_hook2(uint16_t reg, uint32_t length, uint8_t *data)
{
	ret_code_t err_code;
	
	m_fuel_xfer_done = false;
	err_code = nrf_drv_twi_tx(&m_twi, FUEL_GAUGE_ADDRESS, (uint8_t *)&reg, 1, true);
	APP_ERROR_CHECK(err_code);

	while(m_fuel_xfer_done == false);
	
	m_fuel_rx_done = false;
	err_code = nrf_drv_twi_rx(&m_twi, FUEL_GAUGE_ADDRESS, data, length);
	APP_ERROR_CHECK(err_code);

	while(m_fuel_rx_done == false);

	return 0;
}
#endif //ENABLE_FUEL_GAUGE

#endif

