#include "peripheral_define.h"

#ifdef ENABLE_FUEL_GAUGE

void fuel_gauge_condition(fuel_gauge_condition_e condition)
{
	uint8_t write_data[2] = {0,};
	
	write_data[0] = condition;
	
	inv_serial_interface_write_hook2(FUEL_GAUGE_CONDITION, 2, write_data);
	nrf_delay_us(5000);
}

void fuel_gauge_init(void)
{
	fuel_gauge_condition(ACTIVE_MODE);
	fuel_gauge_condition(OCV_CORRECTION);
	
	fuel_gauge_communication(VOLTAGE);
	fuel_gauge_communication(BATTERY_STATUS);
	fuel_gauge_communication(RELATIVE_STATE_CHARGE);
	
	fuel_gauge_condition(STANDBY_MODE);
}

void fuel_gauge_communication(fuel_gauge_command_e cmd)
{
	uint8_t read_data[8] = {0,};
	uint16_t data = 0;
	
	inv_serial_interface_read_hook2(cmd, 2, read_data);
	data = ((read_data[1] << 8) | read_data[0]);
	
	switch(cmd)
	{
	case TEMPERATURE:
		{
			uint16_t remainder = 0;
			
			if( (read_data[0] == 0xFF) && (read_data[1] == 0xFF) )
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			
			remainder = data % 10;//0.x℃ 값 받기
			if(remainder >= 5)//반올림
			{
				data += 10;
			}
			g_str_batt.i8_batt_temperature = (data - remainder)/10;
			break;
		}
		
	case VOLTAGE:
		{
			if(data > 4350)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			
			if(data >= 4200)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_2;
			else if(data >= 4100)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_1;
			else if(data >= 4000)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_0;
			else if(data >= 3900)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_9;
			else if(data >= 3800)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_8;
			else if(data >= 3700)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_7;
			else if(data >= 3600)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_6;
			else if(data >= 3500)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_5;
			else if(data >= 3400)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_4;
			else if(data >= 3300)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_3;
			else if(data >= 3200)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_2;
			else if(data >= 3100)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_1;
			else if(data >= 3000)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_0;
			break;
		}
		
	case USING_CURRENT:
		{
			if(data == 0xFFFF)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.i16_batt_using_current = data;
			break;
		}
		
	case AVERAGE_CURRENT:
		{
			if(data == 0xFFFF)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.i16_batt_avg_current = data;
			break;
		}
		
	case RELATIVE_STATE_CHARGE:
		{
			if(data == 0xFFFF)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			
			if( !data )
			{
				if( g_str_batt.ui16_batt_level == BATT_LEVEL_4_2 )
				{
					data = 25600;
				}
			}
			
			g_str_batt.f32_relative_state_charge = ((float)data/256.0);
			
			
			g_str_batt.ui16_percent = (uint16_t)(g_str_batt.f32_relative_state_charge);
			g_str_batt.ui16_level = (g_str_batt.ui16_percent / 20);
			
			uint8_t byte[5];
			uint8_t nus_index = 0;
			
			byte[++nus_index] = REPORT_APP_CONFIG;
			byte[++nus_index] = REP_CONFIG_BATT_STATUS;
			byte[++nus_index] = REP_BATT_PERCENTAGE;
			byte[++nus_index] = g_str_batt.ui16_percent;
			
			byte[0] = ++nus_index;
			nus_tx_push(byte,byte[0]);
			break;
		}
		
	case ABSOLUTE_STATE_CHARGE:
		{
			if(data == 0xFFFF)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.f32_absolute_state_charge = ((float)data/256.0);
			//g_str_batt.ui16_percent = (uint16_t)(g_str_batt.f32_absolute_state_charge);
			break;
		}
		
	case USABLE_CAPACITY:
		{
			if(data >= BATT_FULL_CAPACITY)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.ui16_batt_usable_capacity = data;
			break;
		}
		
	case REMAINING_CAPACITY:
		{
			if(data >= BATT_FULL_CAPACITY)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.ui16_batt_remain_capacity = data;
			break;
		}
		
	case FULL_CHARGE_CAPACITY:
		{
			if(data > BATT_FULL_CAPACITY)
			{
				inv_serial_interface_read_hook2(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.ui16_batt_full_charge_capacity = data;
			break;
		}
	case BATTERY_STATUS:
		{
			if(data == 0xFFFF)
			{
				memset(read_data, 0x00, sizeof(read_data));
				inv_serial_interface_read_hook2(cmd, 2, read_data);
			}
			g_str_fuel_gauge_batt.b1_battery_degradation_alert = (read_data[0] & 0x80) >> 7;
			g_str_fuel_gauge_batt.b1_usable_capacity_low_alarm = (read_data[0] & 0x40) >> 6;
			g_str_fuel_gauge_batt.b1_remaining_run_time_alarm = (read_data[0] & 0x20) >> 5;
			g_str_fuel_gauge_batt.b1_discharge = (read_data[0] & 0x10) >> 4;
			
			if(g_str_fuel_gauge.ui16_firmware_version >= 0x0703)
			{
				g_str_fuel_gauge_batt.b1_full_charge = (read_data[0] & 0x08) >> 3;
				g_str_fuel_gauge_batt.b1_full_discharge = (read_data[0] & 0x04) >> 2;
			}
			
			g_str_fuel_gauge_batt.b1_soc_high_detection = (read_data[0] & 0x02) >> 1;
			g_str_fuel_gauge_batt.b1_soc_low_detection = read_data[0] & 0x01;
			
			g_str_fuel_gauge_batt.b1_over_charge = (read_data[1] & 0x80) >> 7;
			g_str_fuel_gauge_batt.b1_over_discharge = (read_data[1] & 0x40) >> 6;
			g_str_fuel_gauge_batt.b1_charge_over_current = (read_data[1] & 0x20) >> 5;
			g_str_fuel_gauge_batt.b1_discharge_over_current = (read_data[1] & 0x10) >> 4;
			g_str_fuel_gauge_batt.b1_over_temperature = (read_data[1] & 0x08) >> 3;
			g_str_fuel_gauge_batt.b1_under_temperature = (read_data[1] & 0x04) >> 2;
			break;
		}
		
	case FUEL_GAUGE_STATUS:
		{
			g_str_fuel_gauge.b1_battery_alert = (read_data[0] & 0x80) >> 7;
			if(g_str_fuel_gauge.b1_battery_alert)
			{
				;//When abnormal condition or alert of Battery Status
				// command (0x16) (any bits of Byte0 or bit7 of Byte1 are set)
				// was detected, Battery Alert flag is set.
			}
			
			g_str_fuel_gauge.b2_system_failure_alarm = (read_data[0] & 0x60) >> 5;
			switch(g_str_fuel_gauge.b2_system_failure_alarm)
			{
			case 0://fuel gauge is normal operation
				break;
			case 1://fuel gauge is simple operation
				break;
			case 2://fuel gauge isn't operational
				break;
			}
			
			g_str_fuel_gauge.b1_alert_status = (read_data[0] & 0x10) >> 4;
			if(g_str_fuel_gauge.b1_alert_status)
			{
				;//This bit hold the state of the alert interrupt signal.
			}
			
			g_str_fuel_gauge.b4_command_response_status = (read_data[0] & 0x0f);
			
			g_str_fuel_gauge.b1_data_not_ready = (read_data[1] & 0x80) >> 7;
			g_str_fuel_gauge.b1_detect_stable_current = (read_data[1] & 0x10) >> 4;
			g_str_fuel_gauge.b1_update_battery_capacity = (read_data[1] & 0x04) >> 2;
			g_str_fuel_gauge.b1_correct_remaining_capacity = (read_data[1] & 0x02) >> 1;
			if(g_str_fuel_gauge.b1_data_not_ready)
			{
				;//FG data of SOC etc. cannot yet be obtained
			}
			break;
		}
		
	case IDENTIFY:
		{
			g_str_fuel_gauge.ui16_firmware_version = ((read_data[1] << 8) | read_data[0]);
			break;
		}
		
	case AVERAGE_TIME_EMPTY:
		break;
	case AVERAGE_TIME_FULL:
		break;
	case CYCLE_COUNT:
		break;
	case STATE_BATTERY_CONDITION:
		break;
	case INTERNAL_TEMPERATURE:
		break;
	case AT_RATE_TIME_EMPTY:
		break;
	case AT_RATE_TIME_FULL:
		break;
	case FUEL_GAUGE_PARAMETER_OFFSET:
		break;
	case FUEL_GAUGE_PARAMETER_DATA:
		break;
	case PRODUCT_INFORMATION:
		break;
	case ID_INFORMATION:
		break;
	case BATTERY_PACK_INFORMATION:
		break;
	case DESIGN_CAPACITY:
		break;
	case DESIGN_VOLTAGE:
		break;
	case NVM_PARAMTER_OFFSET:
		break;
	case NVM_PARAMTER_DATA:
		break;
	case MON_STATUS:
		break;
	case FUEL_GAUGE_RESET:
		break;
	case READ_NVM:
		break;
	case ERASE_NVM:
		break;
	case WRITE_NVM:
		break;
	default : break;
	}
	
	nrf_delay_us(1000);
}

#endif
