#include "peripheral_define.h"

#ifdef ENABLE_CTS

static const char * day_of_weeks[8] = {"Unknown",
                                      "Monday",
                                      "Tuesday",
                                      "Wednesday",
                                      "Thursday",
                                      "Friday",
                                      "Saturday",
                                      "Sunday"};

static const char * month_of_year[13] = {"Unknown",
                                         "January",
                                         "February",
                                         "March",
                                         "April",
                                         "May",
                                         "June",
                                         "July",
                                         "August",
                                         "September",
                                         "October",
                                         "November",
                                         "December"};


ble_cts_c_t		  m_cts;


/**@brief Function for handling the Current Time Service errors.
 *
 * @param[in] p_evt  Event received from the Current Time Service client.
 */
static void current_time_print(ble_cts_c_evt_t * p_evt)
{
    NRF_LOG_INFO("\r\nCurrent Time:\r\n");
    NRF_LOG_INFO("\r\nDate:\r\n");

    NRF_LOG_INFO("\tDay of week   %s\r\n", (uint32_t)day_of_weeks[p_evt->
                                                         params.
                                                         current_time.
                                                         exact_time_256.
                                                         day_date_time.
                                                         day_of_week]);

    if (p_evt->params.current_time.exact_time_256.day_date_time.date_time.day == 0)
    {
        NRF_LOG_INFO("\tDay of month  Unknown\r\n");
    }
    else
    {
        NRF_LOG_INFO("\tDay of month  %i\r\n",
                       p_evt->params.current_time.exact_time_256.day_date_time.date_time.day);
    }

    NRF_LOG_INFO("\tMonth of year %s\r\n",
    (uint32_t)month_of_year[p_evt->params.current_time.exact_time_256.day_date_time.date_time.month]);
    if (p_evt->params.current_time.exact_time_256.day_date_time.date_time.year == 0)
    {
        NRF_LOG_INFO("\tYear          Unknown\r\n");
    }
    else
    {
        NRF_LOG_INFO("\tYear          %i\r\n",
                       p_evt->params.current_time.exact_time_256.day_date_time.date_time.year);
    }
    NRF_LOG_INFO("\r\nTime:\r\n");
    NRF_LOG_INFO("\tHours     %i\r\n",
                   p_evt->params.current_time.exact_time_256.day_date_time.date_time.hours);
    NRF_LOG_INFO("\tMinutes   %i\r\n",
                   p_evt->params.current_time.exact_time_256.day_date_time.date_time.minutes);
    NRF_LOG_INFO("\tSeconds   %i\r\n",
                   p_evt->params.current_time.exact_time_256.day_date_time.date_time.seconds);
    NRF_LOG_INFO("\tFractions %i/256 of a second\r\n",
                   p_evt->params.current_time.exact_time_256.fractions256);

    NRF_LOG_INFO("\r\nAdjust reason:\r\r\n");
    NRF_LOG_INFO("\tDaylight savings %x\r\n",
                   p_evt->params.current_time.adjust_reason.change_of_daylight_savings_time);
    NRF_LOG_INFO("\tTime zone        %x\r\n",
                   p_evt->params.current_time.adjust_reason.change_of_time_zone);
    NRF_LOG_INFO("\tExternal update  %x\r\n",
                   p_evt->params.current_time.adjust_reason.external_reference_time_update);
    NRF_LOG_INFO("\tManual update    %x\r\n",
                   p_evt->params.current_time.adjust_reason.manual_time_update);
	
	uint8_t year  = p_evt->params.current_time.exact_time_256.day_date_time.date_time.year%100;
	uint8_t month = p_evt->params.current_time.exact_time_256.day_date_time.date_time.month;
	uint8_t day = p_evt->params.current_time.exact_time_256.day_date_time.date_time.day;
	uint8_t hour = p_evt->params.current_time.exact_time_256.day_date_time.date_time.hours;
	uint8_t minute = p_evt->params.current_time.exact_time_256.day_date_time.date_time.minutes;
	
	g_str_time.ui32_app_date = (year&0xFF)<<16;
	g_str_time.ui32_app_date |= (month&0xFF)<<8;
	g_str_time.ui32_app_date |= (day&0xFF);
	g_str_time.ui32_app_hour = hour;
	g_str_time.ui8_minute = minute;
	g_str_time.ui8_second = p_evt->params.current_time.exact_time_256.day_date_time.date_time.seconds;
	g_str_time.ui8_day_of_week = p_evt->params.current_time.exact_time_256.day_date_time.day_of_week%7;
	
	if( !( g_str_time.ui32_app_hour || g_str_time.ui8_minute || g_str_time.ui8_second ) )
	{
		g_str_bit.b1_cts_ready = 1;
		return;
	}
	else
	{
		g_str_bit.b1_cts_ready = 0;
	}
	
	if( g_str_time.ui32_date )
	{
		uint32_t ui32_cur_time  = ( uint32_t )( ( g_str_time.ui32_date & 0x00FFFFFF ) << 8 );
		uint32_t ui32_new_time  = ( uint32_t )( ( g_str_time.ui32_app_date & 0x00FFFFFF ) << 8 );
		
		ui32_cur_time |= ( uint32_t )( g_str_time.ui32_hour & 0x00ff );
		ui32_new_time |= ( uint32_t )( g_str_time.ui32_app_hour & 0x00ff );
		
		// check time
		if( ui32_new_time > ui32_cur_time )
		{
			pedo_flash_hour_write();
		}
	}
	
	g_str_time.ui32_date = g_str_time.ui32_app_date;
	g_str_time.ui32_hour = g_str_time.ui32_app_hour;
}

/**@brief Function for handling the Current Time Service client events.
 *
 * @details This function will be called for all events in the Current Time Service client that
 *          are passed to the application.
 *
 * @param[in] p_evt Event received from the Current Time Service client.
 */
void on_cts_c_evt(ble_cts_c_t * p_cts, ble_cts_c_evt_t * p_evt)
{
    ret_code_t err_code;

    switch (p_evt->evt_type)
    {
        case BLE_CTS_C_EVT_DISCOVERY_COMPLETE:
            NRF_LOG_INFO("Current Time Service discovered on server.\r\n");
            err_code = ble_cts_c_handles_assign(&m_cts,
                                                p_evt->conn_handle,
                                                &p_evt->params.char_handles);
            APP_ERROR_CHECK(err_code);
			
			g_str_bit.b1_cts_ready = 1;
			
            break;

        case BLE_CTS_C_EVT_DISCOVERY_FAILED:
            NRF_LOG_INFO("Current Time Service not found on server. \r\n");

            break;

        case BLE_CTS_C_EVT_DISCONN_COMPLETE:
            NRF_LOG_INFO("Disconnect Complete.\r\n");
            break;

        case BLE_CTS_C_EVT_CURRENT_TIME:
            NRF_LOG_INFO("Current Time received.\r\n");
            current_time_print(p_evt);
            break;

        case BLE_CTS_C_EVT_INVALID_TIME:
            NRF_LOG_INFO("Invalid Time received.\r\n");
            break;

        default:
            break;
    }
}

/**@brief Function for handling the Current Time Service errors.
 *
 * @param[in]  nrf_error  Error code containing information about what went wrong.
 */
static void current_time_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing services that will be used by the application.
 */
void cts_service_init(void)
{
    ret_code_t       err_code;
    ble_cts_c_init_t cts_init_obj;

    cts_init_obj.evt_handler   = on_cts_c_evt;
    cts_init_obj.error_handler = current_time_error_handler;
    err_code                   = ble_cts_c_init(&m_cts, &cts_init_obj);
    APP_ERROR_CHECK(err_code);
}

#endif //ENABLE_CTS

