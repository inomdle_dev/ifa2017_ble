#include "peripheral_define.h"
void app_status_read( void )
{
	user_flash_read( USER_FLASH_APP_CONFIG,  (uint32_t *)&g_str_app_status, 0, (uint32_t)sizeof(g_str_app_status));
	if( g_str_app_status.ui8_dnd_status == 0xFF )
	{
		memset(&g_str_app_status, 0, sizeof(init_status_struct_t));
	}
}

void app_status_write( void )
{
	user_flash_erase( USER_FLASH_APP_CONFIG );

	user_flash_write( USER_FLASH_APP_CONFIG,  (uint32_t *)&g_str_app_status, 0, (uint32_t)sizeof(g_str_app_status));
}
