/**
 * File name : gpio_event.c
 *
 * This file contains the source code for managing external interrupt / gpio output
 */

#include "peripheral_define.h"

#ifdef ENABLE_GPIO

#ifdef ENABLE_PEDOMETER
void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	switch(action)
	{
	case NRF_GPIOTE_POLARITY_TOGGLE:
		if(nrf_drv_gpiote_in_is_set(PEDO_INT))
		{
			g_str_bit.b1_motion_chk = 1;
		}
		break;
	default : break;
	}
}

#endif

#ifdef ENABLE_TOUCH_DETECT
void in_pin_handler2(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	if( g_str_bit.b1_por_status || !g_str_batt.ui16_percent )
	{
		if( !g_str_bit.b1_chg_status )
		{
			return;
		}
	}

	g_str_bit.b1_touch_read_ready = 1;

}
#endif

#ifdef ENABLE_CHARGER_DETECT
void in_pin_handler3(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	switch(action)
	{
	case NRF_GPIOTE_POLARITY_TOGGLE:
		if(nrf_drv_gpiote_in_is_set(CHK_VBUS))
		{
			g_str_bit.b1_chg_status = 1;
			g_str_batt.ui16_batt_status = BATT_STATUS_IN_CHARGING;
			g_str_batt.ui8_low_level = BATT_LOW_NONE;
#ifdef ENABLE_SSD1306	
			batt_popup();
#endif
			uint8_t byte[5] = "";
			uint8_t nus_index = 0;
			
			byte[++nus_index] = REPORT_APP_CONFIG;
			byte[++nus_index] = REP_CONFIG_BATT_STATUS;
			byte[++nus_index] = REP_BATT_CHARGER;
			byte[++nus_index] = g_str_batt.ui16_batt_status;
			
			byte[0] = ++nus_index;
			nus_tx_push(byte, byte[0]);
		}
		else
		{
			g_str_bit.b1_chg_status = 0;
			g_str_timer.ui16_batt_cnt = 59; //after 3s
			g_str_batt.ui16_batt_status = BATT_STATUS_NO_PLUG;
			
			uint8_t byte[5] = "";
			uint8_t nus_index = 0;
			
			byte[++nus_index] = REPORT_APP_CONFIG;
			byte[++nus_index] = REP_CONFIG_BATT_STATUS;
			byte[++nus_index] = REP_BATT_CHARGER;
			byte[++nus_index] = g_str_batt.ui16_batt_status;
			
			byte[0] = ++nus_index;
			nus_tx_push(byte, byte[0]);
		}
		break;
	default : break;
	}
}

#endif

void gpio_init(void)
{
    ret_code_t err_code;
	
	nrf_drv_gpiote_uninit();

	if (!nrf_drv_gpiote_is_init())
	{
    	err_code = nrf_drv_gpiote_init();
   		APP_ERROR_CHECK(err_code);
	}
		
#ifdef ENABLE_PEDOMETER
	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    in_config.pull = NRF_GPIO_PIN_NOPULL;


	err_code = nrf_drv_gpiote_in_init(PEDO_INT, &in_config, in_pin_handler);
	APP_ERROR_CHECK(err_code);

	nrf_drv_gpiote_in_event_enable(PEDO_INT, true);
#endif
	
#ifdef ENABLE_TOUCH_DETECT
	nrf_drv_gpiote_in_config_t in_config2 = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);
	in_config2.pull = NRF_GPIO_PIN_PULLUP;

	err_code = nrf_drv_gpiote_in_init(TOUCH_INT, &in_config2, in_pin_handler2);
	APP_ERROR_CHECK(err_code);

	nrf_drv_gpiote_in_event_enable(TOUCH_INT, true);
#endif
	
#ifdef ENABLE_CHARGER_DETECT
	nrf_drv_gpiote_in_config_t in_config3 = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    in_config3.pull = NRF_GPIO_PIN_NOPULL;

	err_code = nrf_drv_gpiote_in_init(CHK_VBUS, &in_config3, in_pin_handler3);
	APP_ERROR_CHECK(err_code);

	nrf_drv_gpiote_in_event_enable(CHK_VBUS, true);
#endif

	nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE(false);

    err_code = nrf_drv_gpiote_out_init(HAPTIC_OUT, &out_config);
    APP_ERROR_CHECK(err_code);
	
	nrf_drv_gpiote_out_clear(HAPTIC_OUT);
	
#ifndef ENABLE_ES2_BOARD
	err_code = nrf_drv_gpiote_out_init(BATT_CHK_ON, &out_config);
	APP_ERROR_CHECK(err_code);
	
	nrf_drv_gpiote_out_clear(BATT_CHK_ON);
#endif
}

void haptic_on( void )
{
	if( g_str_bit.b1_haptic_status)
	{
		nrf_drv_gpiote_out_set(HAPTIC_OUT);
	}
}

void haptic_off( void )
{
	nrf_drv_gpiote_out_clear(HAPTIC_OUT);
}

#endif //ENABLE_GPIO
