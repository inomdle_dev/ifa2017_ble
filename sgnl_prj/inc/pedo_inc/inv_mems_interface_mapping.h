/*
* ________________________________________________________________________________________________________
* Copyright � 2014-2015 InvenSense Inc. Portions Copyright � 2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software�) is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/
/** @defgroup inv_mems_interface_mapping inv_interface_mapping
	@ingroup Mems_dmp3
	@{
*/	
#ifndef INV_MEMS_INTERFACE_MAPPING_H__DSFJSD__
#define INV_MEMS_INTERFACE_MAPPING_H__DSFJSD__

//#include "mltypes.h"

int inv_mems_firmware_load(const unsigned char *data_start, unsigned short size_start, unsigned short load_addr);
int dmp_get_pedometer_num_of_steps(uint32_t *steps);
int reset_steps(void);
int inv_dmpdriver_mems_firmware_load(const unsigned char *data_start, unsigned short size_start, unsigned short load_addr);
unsigned char *inv_dmpdriver_int16_to_big8(short x, unsigned char *big8);
unsigned char *inv_dmpdriver_int32_to_big8(long x, unsigned char *big8);

#endif // INV_INTERFACE_MAPPING_H__DSFJSD__

/** @} */
