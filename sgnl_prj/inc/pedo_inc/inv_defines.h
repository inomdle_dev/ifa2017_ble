/*
* ________________________________________________________________________________________________________
* Copyright ?2014-2015 InvenSense Inc. Portions Copyright ?2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software? is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/

#ifndef _INV_DEFINES_H_
#define _INV_DEFINES_H_

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "dmp3Default_20648.h"

#include "inv_mems_hw_config.h"
#include "pedometer.h"

#include "invn_types.h"
#include "inv_mems.h"
#include "inv_mems_base_control.h"
#include "inv_mems_base_driver.h"
#include "inv_mems_defines.h"
#include "inv_mems_interface_mapping.h"
#include "inv_mems_mpu_fifo_control.h"
#include "inv_mems_transport.h"

#include "peripheral_define.h"

#endif
