/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#ifndef PIN_SGNL_H
#define PIN_SGNL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"
	
#ifdef ENABLE_ES2_BOARD

#define BUTTONS_NUMBER 2

#define BUTTON_1       3
#define BUTTON_2       5

#define BUTTON_PULL    NRF_GPIO_PIN_NOPULL
#define BUTTONS_ACTIVE_STATE 0
	
#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2
	
#define TOUCH_INT		22
	
#define SCL_PIN			13
#define SDA_PIN			14
	
#define OLED_SCL_PIN	28
#define OLED_SDA_PIN	29

#define PEDO_INT		15
#define HAPTIC_OUT		12
	
#define SSD1306_CONFIG_RST_PIN      26
#define SSD1306_CONFIG_VDD_PIN      30
	
#define POWER_HOLD		4
	
#define ADC_A0_PIN		2     // Analog channel 0

#else // IFA SAMPLE
	
	#define BUTTONS_NUMBER 2

	#define BUTTON_1       3
	#define BUTTON_2       4
	//#define BUTTON_3       6

	#define BUTTON_PULL    NRF_GPIO_PIN_NOPULL
	#define BUTTONS_ACTIVE_STATE 0
		
	#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

	#define BSP_BUTTON_0   BUTTON_1
	#define BSP_BUTTON_1   BUTTON_2

	#define ADC_A0_PIN		2     // Analog channel 0

	#define TOUCH_INT		6
		
	#define SCL_PIN			7
	#define SDA_PIN			8

	#define SSD1306_CONFIG_VDD_PIN      9
	#define SSD1306_CONFIG_RST_PIN      10

	#define CHK_VBUS	    12
	#define HAPTIC_OUT		14

	#define BATT_CHK_ON     17
	#define PEDO_INT		20
#endif

// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_RC,            \
                                 .rc_ctiv       = 10,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_250_PPM}

#ifdef __cplusplus
}
#endif

#endif // PIN_SGNL_H
