#ifndef _PERIPHERAL_DEFINE_H_
#define _PERIPHERAL_DEFINE_H_

#define ENABLE_IFA_DEMO

#if defined(BOARD_SGNL)

//#define ENABLE_BUTTON

//#define ENABLE_CTS

#define ENABLE_NUS
#define ENABLE_GPIO
#define ENABLE_TWI
#define ENABLE_PATTERN
#define ENABLE_FLASH
#define ENABLE_ADC
#define ENABLE_BATT
#define ENABLE_ANCS

#define ENABLE_TOUCH_DETECT
#define ENABLE_CHARGER_DETECT

#define ENABLE_SSD1306
#define ENABLE_TOUCHKEY

#define ENABLE_DND

#define ENABLE_WDT

#ifdef ENABLE_NUS
	#define ENABLE_NUS_QUEUE
#endif

#ifdef ENABLE_TWI
	#define ENABLE_PEDOMETER
	//#define ENABLE_FUEL_GAUGE
#endif

#ifdef ENABLE_ANCS
	#define ENABLE_ANCS_PARSER
#endif

#ifdef ENABLE_FUEL_GAUGE
	#ifdef ENABLE_ADC
		#undef ENABLE_ADC
	#endif
#endif

#endif //if defined(BOARD_SGNL)

#include "nordic_common_header.h"

#include "sgnl_struct.h"

#include "ancs_event.h"

#include "timer_event.h"


#ifdef ENABLE_BUTTON
#include "button_event.h"
#endif

#ifdef ENABLE_GPIO
#include "nrf_drv_gpiote.h"
#include "gpio_event.h"
#endif

#ifdef ENABLE_NUS
#include "ble_nus.h"
#include "nus_event.h"
#include "ble_protocol_group.h"
#endif //ENABLE_NUS

#ifdef ENABLE_ADC
#include "nrf_drv_saadc.h"
#include "app_pwm.h"
#include "app_timer.h"
#include "adc_event.h"
#endif //ENABLE_ADC

#ifdef ENABLE_TWI
#include "nrf_drv_twi.h"
#include "twi_event.h"

#ifdef ENABLE_PEDOMETER
#include "inv_defines.h"
#include "pedometer.h"
#include "timer_event.h"
#include "pedo_flash_manager.h"
#endif //ENABLE_PEDOMETER

#ifdef ENABLE_FUEL_GAUGE
	#include "fuel_gauge.h"
#endif

#endif //ENABLE_TWI

#ifdef ENABLE_SPI
#include "nrf_drv_spi.h"
#include "spi_event.h"
#endif //ENABLE_SPI

#ifdef ENABLE_TOUCHKEY
#include "CY8CMBR3xxx_APIs.h"
#include "CY8CMBR3xxx_Registers.h"
#include "CY8CMBR3xxx_HostFunctions.h"

#include "touch_event.h"
#endif

#ifdef ENABLE_SSD1306
#include "binary.h"
#include "ssd1306.h"
#include "icon.h"
#include "innom_num_font.h"
#include "menu_display.h"
#endif //ENABLE_SSD1306

#ifdef ENABLE_PATTERN

#include "app_timer.h"
#include "pattern_control.h"
#include "pattern_check.h"

#endif

#ifdef ENABLE_FLASH

#include "nrf_soc.h"
#include "flash_event.h"

#include "app_status_manager.h"
#endif

#ifdef ENABLE_ANCS
#include "package_parser.h"
#endif

#ifdef ENABLE_WDT
#include "nrf_drv_wdt.h"
#endif
#ifdef ENABLE_CTS
#include "cts_service.h"
#endif

#endif //_PERIPHERAL_DEFINE_H_
