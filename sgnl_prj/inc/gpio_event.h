#ifndef _GPIO_EVENT_H_
#define _GPIO_EVENT_H_

extern void gpio_init( void );
extern void exti_init( void );
extern void haptic_on( void );
extern void haptic_off( void );

#endif
