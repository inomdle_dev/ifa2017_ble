#ifndef _ADC_EVENT_H_
#define _ADC_EVENT_H_

extern void adc_enable( void );
extern void adc_disable( void );

extern void adc_init( void );

#endif //_ADC_EVENT_H_
