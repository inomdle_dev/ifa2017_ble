#ifndef _TIMER_EVENT_H_
#define _TIMER_EVENT_H_

extern void pattern_timer_start(void);
extern void pattern_timer_stop(void);

extern void sub_pattern_timer_start(void);
extern void sub_pattern_timer_stop(void);

extern void day_of_week( void );
extern void time_update(void);
extern void pedo_timer_start(void);
extern void pedo_timer_stop(void);

extern void favo_timeout_timer_start(void);
extern void favo_timeout_timer_stop(void);

extern void favo_log_timer_start( void );
extern void favo_log_timer_stop( void );

extern void mcu_timeout_timer_start(void);
extern void mcu_timeout_timer_stop(void);

extern void oled_timeout_timer_start(uint8_t u8_sec);
extern void oled_timeout_timer_stop(void);

extern void oled_scroll_timer_start( uint8_t *cur_icon, uint8_t *next_icon, uint16_t direction );
extern void oled_scroll_timer_stop(void);

extern void user_timers_init(void);

#endif //_TIMER_EVENT_H_
