#ifndef _ANCS_EVENT_H_
#define _ANCS_EVENT_H_

extern void on_ancs_c_evt(ble_ancs_c_evt_t * p_evt);
extern void ancs_service_init( void );
extern void ancs_negative_action( void );
extern void ancs_positive_action( void );

extern void parsing_ancs( void );

extern ble_ancs_c_t m_ancs_c;

#endif //_ANCS_EVENT_H_
