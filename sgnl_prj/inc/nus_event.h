#ifndef _NUS_EVENT_H_
#define _NUS_EVENT_H_

extern void ble_data_send( uint8_t* ui8_data, uint8_t length );
extern void nus_service_init(void);

extern void nus_rx_queue( void );
extern void nus_tx_queue( void );

extern void nus_tx_push( uint8_t* ui8_byte, uint16_t ui16_length );

extern ble_nus_t m_nus;

#endif//_NUS_EVENT_H_
