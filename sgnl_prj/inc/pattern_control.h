#ifndef _PATTERN_CONTROL_H_
#define _PATTERN_CONTROL_H_

#define PATTERN_POWER_OFF_CNT			110
#define PATTERN_POWER_ON_CNT			110

#define PATTERN_DEVICE_RESET_CNT		100

#define PATTERN_BATT_GAUGE_CNT			120
#define PATTERN_BATT_LOW_CNT			110
#define PATTERN_BATT_CHARGING_CNT		110
#define PATTERN_BATT_FULL_CNT			110

#define PATTERN_UNPAIRED_CNT			180
#define PATTERN_UNPAIR_CNT				110
#define PATTERN_WAIT_PAIRING_CNT		110
#define PATTERN_PAIRING_COMPLETE_CNT	110
#define PATTERN_DISCONN_TIMEOUT_CNT		110

#define PATTERN_DND_ON_CNT				110
#define PATTERN_DND_OFF_CNT				110

#define PATTERN_FAVORITE_SELECT_CNT		110
#define PATTERN_FAVORITE_CALL_CNT		110

#define PATTERN_INCOMING_CALL_CNT		110
#define PATTERN_ACTIVITY_NOTI_CNT		110
#define PATTERN_LONG_SIT_NOTI_CNT		60

#define PATTERN_APP_NOTI_CNT			60

void pattern_power_off( void );
void pattern_power_on( void );

void pattern_device_reset( void );

void pattern_batt_gauge( void );
void pattern_batt_low( void );
void pattern_batt_charging( void );
void pattern_batt_full( void );

void pattern_unpaired( void );
void pattern_unpair( void );
void pattern_wait_pairing( void );
void pattern_pairing_complete( void );
void pattern_disconn_timeout( void );

void pattern_dnd_on( void );
void pattern_dnd_off( void );

void pattern_favorite_select( void );
void pattern_favorite_call( void );
void pattern_incoming_favorite( void );

void pattern_incoming_call( void );
void pattern_activity_noti( void );
void pattern_long_sit_noti( void );

void pattern_app_noti( void );

void sub_pattern_activity_noti( void );
void sub_pattern_long_sit_noti( void );

void sub_pattern_app_noti( void );

#endif //_PATTERN_CONTROL_H_
