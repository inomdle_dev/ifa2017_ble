#ifndef _PATTERN_CHECK_H_
#define _PATTERN_CHECK_H_

#define PATTERN_POWER_OFF_CNT			110
#define PATTERN_POWER_ON_CNT			110

#define PATTERN_DEVICE_RESET_CNT		100

#define PATTERN_BATT_GAUGE_CNT			120
#define PATTERN_BATT_LOW_CNT			110
#define PATTERN_BATT_CHARGING_CNT		110
#define PATTERN_BATT_FULL_CNT			110

#define PATTERN_UNPAIRED_CNT			180
#define PATTERN_UNPAIR_CNT				110
#define PATTERN_WAIT_PAIRING_CNT		110
#define PATTERN_PAIRING_COMPLETE_CNT	110
#define PATTERN_DISCONN_TIMEOUT_CNT		110

#define PATTERN_DND_ON_CNT				110
#define PATTERN_DND_OFF_CNT				110

#define PATTERN_FAVORITE_SELECT_CNT		110
#define PATTERN_FAVORITE_CALL_CNT		110

#define PATTERN_INCOMING_CALL_CNT		110
#define PATTERN_ACTIVITY_NOTI_CNT		110
#define PATTERN_LONG_SIT_NOTI_CNT		60

#define PATTERN_APP_NOTI_CNT			60

void pattern_command_check(void);

void call_power_off( void );
void call_power_on( void );
void call_device_reset( void );
void call_batt_gauge( void );
void call_batt_low( void );
void call_batt_charging( void );
void call_batt_full( void );
void call_unpaired( void );
void call_unpair( void );
void call_wait_pairing( void );
void call_pairing_complete( void );
void call_disconn_timeout( void );
void call_dnd_on( void );
void call_dnd_off( void );
void call_favorite_select( void );
void call_favorite_call( void );
void call_incoming_favorite( void );

void call_incoming_call( void );
void call_activity_noti( void );
void call_long_sit_noti( void );

void call_app_noti( void );

void sub_pattern_command_check(void);

void call_sub_activity_noti( void );
void call_sub_long_sit_noti( void );

void call_sub_app_noti( void );

void force_off_pattern( void );

typedef void ( *pattern_func )( void );

extern pattern_func p_pattern_func;
extern pattern_func p_sub_pattern_func;

#endif //_PATTERN_CHECK_H_
