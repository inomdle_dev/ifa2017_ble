#ifndef _TWI_EVENT_H_
#define _TWI_EVENT_H_

#define TWI_SCL_PIN	27
#define TWI_SDA_PIN	26

void twi_init(void);
void oled_twi_init(void);

void wait_oled_twi(void);

extern void twi_mul_write(uint8_t i2c_addr, const uint8_t *data, uint8_t length );
extern int twi_write(uint8_t i2c_addr, const uint8_t *data, uint32_t length);

extern int touch_twi_write(uint8_t i2c_addr, const uint8_t *data, uint32_t length);
extern int touch_twi_read(uint8_t i2c_addr, const uint8_t *data, uint32_t length);

extern int twi_check_connect( uint16_t i2c_addr );

extern int inv_serial_interface_read_hook2(uint16_t reg, uint32_t length, uint8_t *data);
extern int inv_serial_interface_write_hook2(uint16_t reg, uint32_t length, const uint8_t *data);

extern const nrf_drv_twi_t m_twi;

#endif //_TWI_EVENT_H_
